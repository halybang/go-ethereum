package ethapi

import (
	"bytes"
	"context"
	"crypto/ecdsa"
	//"encoding/hex"
	"errors"
	//"fmt"
	"math/big"
	"strings"
	//"time"

	//"github.com/davecgh/go-spew/spew"
	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/accounts/keystore"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	//"github.com/ethereum/go-ethereum/common/math"
	//"github.com/ethereum/go-ethereum/consensus/ethash"
	//"github.com/ethereum/go-ethereum/core"
	//"github.com/ethereum/go-ethereum/core/rawdb"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/core/vm"
	"github.com/ethereum/go-ethereum/crypto"
	//"github.com/ethereum/go-ethereum/log"
	//"github.com/ethereum/go-ethereum/p2p"
	"github.com/ethereum/go-ethereum/params"
	//"github.com/ethereum/go-ethereum/rlp"
	"github.com/ethereum/go-ethereum/rpc"
	//"github.com/syndtr/goleveldb/leveldb"
	//"github.com/syndtr/goleveldb/leveldb/util"
)

var (
	ErrInvalidWAddress                  = errors.New("Invalid Waddress, try again")
	ErrFailToGeneratePKPairFromWAddress = errors.New("Fail to generate publickey pair from WAddress")
	ErrFailToGeneratePKPairSlice        = errors.New("Fail to generate publickey pair hex slice")
	ErrInvalidPrivateKey                = errors.New("Invalid private key")
	ErrInvalidOTAMixSet                 = errors.New("Invalid OTA mix set")
	ErrInvalidOTAAddr                   = errors.New("Invalid OTA address")
	ErrReqTooManyOTAMix                 = errors.New("Require too many OTA mix address")
	ErrInvalidOTAMixNum                 = errors.New("Invalid required OTA mix address number")
	ErrInvalidInput                     = errors.New("Invalid input")
)

// RingSignedData represents a ring-signed digital signature
type RingSignedData struct {
	PublicKeys []*ecdsa.PublicKey
	KeyImage   *ecdsa.PublicKey
	Ws         []*big.Int
	Qs         []*big.Int
}

func (s *PrivateAccountAPI) UpdateAccount(addr common.Address, oldPassword string, newPassword string) error {
	keystore := fetchKeystore(s.am)
	if keystore == nil {
		return errors.New("invalid keystore!")
	}

	account := accounts.Account{Address: addr}
	return keystore.Update(account, oldPassword, newPassword, newPassword)
}

// SendPrivacyCxtTransaction will create a transaction from the given arguments and
// tries to sign it with the OTA key associated with args.To.
func (s *PrivateAccountAPI) SendPrivacyCxtTransaction(ctx context.Context, args SendTxArgs, sPrivateKey string) (common.Hash, error) {

	if !hexutil.Has0xPrefix(sPrivateKey) {
		return common.Hash{}, ErrInvalidPrivateKey
	}

	// Set some sanity defaults and terminate on failure
	if err := args.setDefaults(ctx, s.b); err != nil {
		return common.Hash{}, err
	}

	if args.To == nil || len(*args.Data) == 0 {
		return common.Hash{}, ErrInvalidInput
	}

	// Assemble the transaction and sign with the wallet
	tx := args.toOTATransaction()

	var chainID *big.Int
	//if config := s.b.ChainConfig(); config.IsEIP155(s.b.CurrentBlock().Number()) {
	if config := s.b.ChainConfig(); config != nil {
		chainID = config.ChainID
	}

	privateKey, err := crypto.HexToECDSA(sPrivateKey[2:])
	if err != nil {
		return common.Hash{}, err
	}

	//for fixing bug send privacy tx with a different private key
	fromAddr := crypto.PubkeyToAddress(privateKey.PublicKey)
	if !bytes.Equal(fromAddr[:], args.From[:]) {
		return common.Hash{}, errors.New("from address mismatch private key")
	}

	var signed *types.Transaction
	signed, err = types.SignTx(tx, types.NewEIP155Signer(chainID), privateKey)
	if err != nil {
		return common.Hash{}, err
	}

	return submitTransaction(ctx, s.b, signed)
}

// GenRingSignData generate ring sign data
func (s *PrivateAccountAPI) GenRingSignData(ctx context.Context, hashMsg string, privateKey string, mixWanAdresses string) (string, error) {
	if !hexutil.Has0xPrefix(privateKey) {
		return "", ErrInvalidPrivateKey
	}

	hmsg, err := hexutil.Decode(hashMsg)
	if err != nil {
		return "", err
	}

	ecdsaPrivateKey, err := crypto.HexToECDSA(privateKey[2:])
	if err != nil {
		return "", err
	}

	privKey, err := hexutil.Decode(privateKey)
	if err != nil {
		return "", err
	}

	if privKey == nil {
		return "", ErrInvalidPrivateKey
	}

	wanAddresses := strings.Split(mixWanAdresses, "+")
	if len(wanAddresses) == 0 {
		return "", ErrInvalidOTAMixSet
	}

	return genRingSignData(hmsg, privKey, &ecdsaPrivateKey.PublicKey, wanAddresses)
}

func genRingSignData(hashMsg []byte, privateKey []byte, actualPub *ecdsa.PublicKey, mixWanAdress []string) (string, error) {
	otaPrivD := new(big.Int).SetBytes(privateKey)

	publicKeys := make([]*ecdsa.PublicKey, 0)
	publicKeys = append(publicKeys, actualPub)

	for _, strWanAddr := range mixWanAdress {
		pubBytes, err := hexutil.Decode(strWanAddr)
		if err != nil {
			return "", errors.New("fail to decode wan address!")
		}

		if len(pubBytes) != common.WAddressLength {
			return "", ErrInvalidWAddress
		}

		publicKeyA, _, err := keystore.GeneratePKPairFromWAddress(pubBytes)
		if err != nil {

			return "", errors.New("Fail to generate public key from wan address!")

		}

		publicKeys = append(publicKeys, publicKeyA)
	}

	retPublicKeys, keyImage, w_random, q_random, err := crypto.RingSign(hashMsg, otaPrivD, publicKeys)
	if err != nil {
		return "", err
	}

	return encodeRingSignOut(retPublicKeys, keyImage, w_random, q_random)
}

//  encode all ring sign out data to a string
func encodeRingSignOut(publicKeys []*ecdsa.PublicKey, keyimage *ecdsa.PublicKey, Ws []*big.Int, Qs []*big.Int) (string, error) {
	tmp := make([]string, 0)
	for _, pk := range publicKeys {
		tmp = append(tmp, common.ToHex(crypto.FromECDSAPub(pk)))
	}

	pkStr := strings.Join(tmp, "&")
	k := common.ToHex(crypto.FromECDSAPub(keyimage))
	wa := make([]string, 0)
	for _, wi := range Ws {
		wa = append(wa, hexutil.EncodeBig(wi))
	}

	wStr := strings.Join(wa, "&")
	qa := make([]string, 0)
	for _, qi := range Qs {
		qa = append(qa, hexutil.EncodeBig(qi))
	}
	qStr := strings.Join(qa, "&")
	outs := strings.Join([]string{pkStr, k, wStr, qStr}, "+")
	return outs, nil
}

////////////////////added for privacy tx ////////////////////////////////////////
// GetWanAddress returns corresponding WAddress of an ordinary account
func (s *PublicTransactionPoolAPI) GetWanAddress(ctx context.Context, a common.Address) (string, error) {
	account := accounts.Account{Address: a}
	// first fetch the wallet/keystore, and then retrieve the wanaddress
	wallet, err := s.b.AccountManager().Find(account)
	if err != nil {
		return "", err
	}
	wanAddr, err := wallet.GetWanAddress(account)
	if err != nil {
		return "", err
	}

	return hexutil.Encode(wanAddr[:]), nil
}

// GenerateOneTimeAddress returns corresponding One-Time-Address for a given WanAddress
func (s *PublicTransactionPoolAPI) GenerateOneTimeAddress(ctx context.Context, wAddr string) (string, error) {
	strlen := len(wAddr)
	if strlen != (common.WAddressLength<<1)+2 {
		return "", ErrInvalidWAddress
	}

	PKBytesSlice, err := hexutil.Decode(wAddr)
	if err != nil {
		return "", err
	}

	PK1, PK2, err := keystore.GeneratePKPairFromWAddress(PKBytesSlice)
	if err != nil {
		return "", ErrFailToGeneratePKPairFromWAddress
	}

	PKPairSlice := hexutil.PKPair2HexSlice(PK1, PK2)

	SKOTA, err := crypto.GenerateOneTimeKey(PKPairSlice[0], PKPairSlice[1], PKPairSlice[2], PKPairSlice[3])
	if err != nil {
		return "", err
	}

	otaStr := strings.Replace(strings.Join(SKOTA, ""), "0x", "", -1)
	raw, err := hexutil.Decode("0x" + otaStr)
	if err != nil {
		return "", err
	}

	rawWanAddr, err := keystore.WaddrFromUncompressedRawBytes(raw)
	if err != nil || rawWanAddr == nil {
		return "", err
	}

	return hexutil.Encode(rawWanAddr[:]), nil
}

func (args *SendTxArgs) toOTATransaction() *types.Transaction {
	return types.NewOTATransaction(uint64(*args.Nonce), *args.To, (*big.Int)(args.Value), (uint64)(*args.Gas), (*big.Int)(args.GasPrice), *args.Data)
}

func (s *PrivateAccountAPI) GetOTABalance(ctx context.Context, blockNr rpc.BlockNumber) (*big.Int, error) {
	state, _, err := s.b.StateAndHeaderByNumber(ctx, blockNr)
	if state == nil || err != nil {
		return nil, err
	}

	otaB, err := vm.GetUnspendOTATotalBalance(state)
	if err != nil {
		return common.Big0, err
	}

	return otaB, state.Error()

}

// GetOTABalance returns OTA balance
func (s *PublicBlockChainAPI) GetOTABalance(ctx context.Context, otaWAddr string, blockNr rpc.BlockNumber) (*big.Int, error) {
	if !hexutil.Has0xPrefix(otaWAddr) {
		return nil, ErrInvalidOTAAddr
	}

	state, _, err := s.b.StateAndHeaderByNumber(ctx, blockNr)
	if state == nil || err != nil {
		return nil, err
	}

	var otaAX []byte
	otaWAddrByte := common.FromHex(otaWAddr)
	switch len(otaWAddrByte) {
	case common.HashLength:
		otaAX = otaWAddrByte
	case common.WAddressLength:
		otaAX, _ = vm.GetAXFromWanAddr(otaWAddrByte)
	default:
		return nil, ErrInvalidOTAAddr
	}

	return vm.GetOtaBalanceFromAX(state, otaAX)
}

func (s *PublicBlockChainAPI) GetSupportWanCoinOTABalances(ctx context.Context) []*big.Int {
	return vm.GetSupportWanCoinOTABalances()
}

func (s *PublicBlockChainAPI) GetSupportStampOTABalances(ctx context.Context) []*big.Int {
	return vm.GetSupportStampOTABalances()
}

func (s *PublicTransactionPoolAPI) GetOTAMixSet(ctx context.Context, otaAddr string, setLen int) ([]string, error) {
	if setLen <= 0 {
		return []string{}, ErrInvalidOTAMixNum
	}

	if uint64(setLen) > params.GetOTAMixSetMaxSize {
		return []string{}, ErrReqTooManyOTAMix
	}

	if !hexutil.Has0xPrefix(otaAddr) {
		return []string{}, ErrInvalidOTAAddr
	}

	orgOtaAddr := common.FromHex(otaAddr)
	if len(orgOtaAddr) < common.HashLength {
		return []string{}, ErrInvalidOTAAddr
	}

	state, _, err := s.b.StateAndHeaderByNumber(ctx, rpc.BlockNumber(-1))
	if state == nil || err != nil {
		return nil, err
	}

	otaAX := orgOtaAddr[:common.HashLength]
	if len(orgOtaAddr) == common.WAddressLength {
		otaAX, _ = vm.GetAXFromWanAddr(orgOtaAddr)
	}

	otaByteSet, _, err := vm.GetOTASet(state, otaAX, setLen)
	if err != nil {
		return nil, err
	}

	ret := make([]string, 0, setLen)
	for _, otaByte := range otaByteSet {
		ret = append(ret, common.ToHex(otaByte))

	}

	return ret, nil
}

// ComputeOTAPPKeys compute ota private key, public key and short address
// from account address and ota full address.
func (s *PublicTransactionPoolAPI) ComputeOTAPPKeys(ctx context.Context, address common.Address, inOtaAddr string) (string, error) {
	account := accounts.Account{Address: address}
	wallet, err := s.b.AccountManager().Find(account)
	if err != nil {
		return "", err
	}

	wanBytes, err := hexutil.Decode(inOtaAddr)
	if err != nil {
		return "", err
	}

	otaBytes, err := keystore.WaddrToUncompressedRawBytes(wanBytes)
	if err != nil {
		return "", err
	}

	otaAddr := hexutil.Encode(otaBytes)

	//AX string, AY string, BX string, BY string
	otaAddr = strings.Replace(otaAddr, "0x", "", -1)
	AX := "0x" + otaAddr[0:64]
	AY := "0x" + otaAddr[64:128]

	BX := "0x" + otaAddr[128:192]
	BY := "0x" + otaAddr[192:256]

	sS, err := wallet.ComputeOTAPPKeys(account, AX, AY, BX, BY)
	if err != nil {
		return "", err
	}

	otaPub := sS[0] + sS[1][2:]
	otaPriv := sS[2]

	privateKey, err := crypto.HexToECDSA(otaPriv[2:])
	if err != nil {
		return "", err
	}

	var addr common.Address
	pubkey := crypto.FromECDSAPub(&privateKey.PublicKey)
	//caculate the address for replaced pub
	copy(addr[:], crypto.Keccak256(pubkey[1:])[12:])

	return otaPriv + "+" + otaPub + "+" + hexutil.Encode(addr[:]), nil

}

//func (s *PublicTransactionPoolAPI) GetOTAMixSet(ctx context.Context, otaAddr string, setLen int) ([]string, error) {
//	if setLen <= 0 {
//		return []string{}, ErrInvalidOTAMixNum
//	}

//	if uint64(setLen) > params.GetOTAMixSetMaxSize {
//		return []string{}, ErrReqTooManyOTAMix
//	}

//	if !hexutil.Has0xPrefix(otaAddr) {
//		return []string{}, ErrInvalidOTAAddr
//	}

//	orgOtaAddr := common.FromHex(otaAddr)
//	if len(orgOtaAddr) < common.HashLength {
//		return []string{}, ErrInvalidOTAAddr
//	}

//	state, _, err := s.b.StateAndHeaderByNumber(ctx, rpc.BlockNumber(-1))
//	if state == nil || err != nil {
//		return nil, err
//	}

//	otaAX := orgOtaAddr[:common.HashLength]
//	if len(orgOtaAddr) == common.WAddressLength {
//		otaAX, _ = vm.GetAXFromWanAddr(orgOtaAddr)
//	}

//	otaByteSet, _, err := vm.GetOTASet(state, otaAX, setLen)
//	if err != nil {
//		return nil, err
//	}

//	ret := make([]string, 0, setLen)
//	for _, otaByte := range otaByteSet {
//		ret = append(ret, common.ToHex(otaByte))

//	}

//	return ret, nil
//}

//// ComputeOTAPPKeys compute ota private key, public key and short address
//// from account address and ota full address.
//func (s *PublicTransactionPoolAPI) ComputeOTAPPKeys(ctx context.Context, address common.Address, inOtaAddr string) (string, error) {
//	account := accounts.Account{Address: address}
//	wallet, err := s.b.AccountManager().Find(account)
//	if err != nil {
//		return "", err
//	}

//	wanBytes, err := hexutil.Decode(inOtaAddr)
//	if err != nil {
//		return "", err
//	}

//	otaBytes, err := keystore.WaddrToUncompressedRawBytes(wanBytes)
//	if err != nil {
//		return "", err
//	}

//	otaAddr := hexutil.Encode(otaBytes)

//	//AX string, AY string, BX string, BY string
//	otaAddr = strings.Replace(otaAddr, "0x", "", -1)
//	AX := "0x" + otaAddr[0:64]
//	AY := "0x" + otaAddr[64:128]

//	BX := "0x" + otaAddr[128:192]
//	BY := "0x" + otaAddr[192:256]

//	sS, err := wallet.ComputeOTAPPKeys(account, AX, AY, BX, BY)
//	if err != nil {
//		return "", err
//	}

//	otaPub := sS[0] + sS[1][2:]
//	otaPriv := sS[2]

//	privateKey, err := crypto.HexToECDSA(otaPriv[2:])
//	if err != nil {
//		return "", err
//	}

//	var addr common.Address
//	pubkey := crypto.FromECDSAPub(&privateKey.PublicKey)
//	//caculate the address for replaced pub
//	copy(addr[:], crypto.Keccak256(pubkey[1:])[12:])

//	return otaPriv + "+" + otaPub + "+" + hexutil.Encode(addr[:]), nil

//}
