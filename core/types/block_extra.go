// Copyright 2014 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

// Package types contains data types related to Ethereum consensus.
package types

import (
	//"encoding/binary"
	"io"
	"math/big"
	//"sort"
	//"sync/atomic"
	//"time"
	//	"unsafe"

	"github.com/ethereum/go-ethereum/common"
	//	"github.com/ethereum/go-ethereum/common/hexutil"
	//"github.com/ethereum/go-ethereum/core/types/devotedb"
	//	"golang.org/x/crypto/sha3"
	"github.com/ethereum/go-ethereum/rlp"
)

// Light Body is a simple (mutable, non-safe) data container for storing and moving
// a block's data contents (transactions and uncles) together.
type BodyLight struct {
	TxHeader []*TxHeader `json:"Transactions"`
	Uncles   []*Header
}

// [deprecated by eth/63]
// StorageBlock defines the RLP encoding of a Block stored in the
// state database. The StorageBlock encoding contains fields that
// would otherwise need to be recomputed.
type StorageBlockLight Block

// "external" block encoding. used for eth protocol, etc.
type extblocklight struct {
	Header *Header
	Uncles []*Header
	Txs    []*TxHeader
}

// [deprecated by eth/63]
// "storage" block encoding. used for database.
type storageblocklight struct {
	Header *Header
	Uncles []*Header
	Txs    []*TxHeader
	TD     *big.Int
}

func NewBlockLight(header *Header, txs []*TxHeader, uncles []*Header, receipts []*Receipt) *Block {
	b := &Block{header: CopyHeader(header), td: new(big.Int)}

	// TODO: panic if len(txs) != len(receipts)
	if len(txs) == 0 {
		b.header.TxHash = EmptyRootHash
	} else {
		b.header.TxHash = DeriveSha(TxHeaders(txs))
		//b.txs = make(TxHeaders, len(txs))
		//copy(b.txs, txs)
	}

	if len(receipts) == 0 {
		b.header.ReceiptHash = EmptyRootHash
	} else {
		b.header.ReceiptHash = DeriveSha(Receipts(receipts))
		b.header.Bloom = CreateBloom(receipts)
	}

	if len(uncles) == 0 {
		b.header.UncleHash = EmptyUncleHash
	} else {
		b.header.UncleHash = CalcUncleHash(uncles)
		b.uncles = make([]*Header, len(uncles))
		for i := range uncles {
			b.uncles[i] = CopyHeader(uncles[i])
		}
	}

	return b
}

// DecodeRLP decodes the Ethereum
func (b *Block) DecodeRLPLight(s *rlp.Stream) error {
	var eb extblocklight
	_, size, _ := s.Kind()
	if err := s.Decode(&eb); err != nil {
		return err
	}
	b.header, b.uncles = eb.Header, eb.Uncles
	//b.txs = eb.Txs // Store TxHeaders
	b.size.Store(common.StorageSize(rlp.ListSize(size)))
	return nil
}

// EncodeRLP serializes b into the Ethereum RLP block format.
func (b *Block) EncodeRLPLight(w io.Writer) error {
	return rlp.Encode(w, extblocklight{
		Header: b.header,
		//Txs:    b.txs,
		Uncles: b.uncles,
	})
}

// [deprecated by eth/63]
func (b *StorageBlockLight) DecodeRLP(s *rlp.Stream) error {
	var sb storageblocklight
	if err := s.Decode(&sb); err != nil {
		return err
	}
	b.header, b.uncles, b.td = sb.Header, sb.Uncles, sb.TD
	//b.txs = sb.Txs
	return nil
}

/*
func (b *Block) TransactionLights() TxHeaders { return b.txs }

func (b *Block) TransactionLight(hash common.Hash) *TxHeader {
	for _, transaction := range b.txs {
		if transaction.Hash == hash {
			return transaction
		}
	}
	return nil
}

// Body returns the non-header content of the block.
func (b *Block) BodyLight() *BodyLight { return &BodyLight{b.txs, b.uncles} }

// Size returns the true RLP encoded storage size of the block, either by encoding
// and returning it, or returning a previsouly cached value.
func (b *BlockLight) Size() common.StorageSize {
	if size := b.size.Load(); size != nil {
		return size.(common.StorageSize)
	}
	c := writeCounter(0)
	rlp.Encode(&c, b)
	b.size.Store(common.StorageSize(c))
	return common.StorageSize(c)
}
*/
// WithBody returns a new block with the given transaction and uncle contents.
func (b *Block) WithLightBody(transactions []*TxHeader, uncles []*Header) *Block {
	block := &Block{
		header: CopyHeader(b.header),
		//txs:    make([]*TxHeader, len(transactions)),
		uncles: make([]*Header, len(uncles)),
	}
	//copy(block.txs, transactions)
	for i := range uncles {
		block.uncles[i] = CopyHeader(uncles[i])
	}
	return block
}
