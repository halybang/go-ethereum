package rawdb

import (
	//"bytes"
	"crypto/rand"
	"encoding/json"
	"math/big"
	"time"

	"testing"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/core/types/devotedb"
	"github.com/ethereum/go-ethereum/crypto/ed25519"
	//"golang.org/x/crypto/sha3"
	"github.com/ethereum/go-ethereum/ethdb"
	//"github.com/ethereum/go-ethereum/rlp"
)

// 188f2e02-6504-4c7b-97c3-4bc8477ddadd
func masternodeAddress() []common.Address {
	addresses := []common.Address{
		common.HexToAddress("0x010096Ac7fB8316Abc710225f055a33196D5563e146b47E56C9BE2AA4A2e9e36"),
		common.HexToAddress("0x0101F89111822aa693a27A9E94ceD0B0fc9745f90Ea8da8e8263C165f60E55dE"),
		common.HexToAddress("0x0102a1A7BAe2246963F7879Fa81628b15c5502cd65993bcaCEBCc690E51843c7"),
		common.HexToAddress("0x01034F9e6dd953A3F48ea6278b86eeD00AF26246936e9591cA71fb1aa71F2f3A"),
		common.HexToAddress("0x0104Fe4C89023E346D59CEF5058f7C88DEc4a445df07fC5223ff70031b15ab38"),
		common.HexToAddress("0x010574FEA5DAE1F75e8cA940bC74f1601C7bF250147721917e62FB9E793E7151"),
		common.HexToAddress("0x0106270C466544F02e76337cA141032A896B17E45278893D590D0BB941e26b0E"),
		common.HexToAddress("0x0107c102136fEB4f32F283f215cb1d4e23536BCFd47e3E2CAde222Aa6d001F50"),
		common.HexToAddress("0x0108121419E8042b5d49b702EC51B62c76c31699CDfCc2688fB96CD5928333dD"),
		common.HexToAddress("0x01097359174CA12279300275225b9169AC914B7fD47A4790c9f57106918d7495"),
		common.HexToAddress("0x010A8a4b89001514F36eA5Be20a7CDf1A2D4Dfaa542214B123fceD89F0395FaF"),
		common.HexToAddress("0x010Be244C932C880eF33E7850cF8f1F5BbB81DDb3Da73D13666F5b62c1EAb7F8"),
		common.HexToAddress("0x010C088D4de6887E6453163b0f139941efe81B75131E5a7691c28Ae328bf8461"),
		common.HexToAddress("0x010d221d88e3d5E4254B135B2410E81D8A74fa1AF9d398122708d94B3f937F02"),
		common.HexToAddress("0x010Ea9140b6d0Fc212E335A9CFC7ee0A05cc2e080b646A1ee32087e4f2D56c42"),
		common.HexToAddress("0x010F42F9Fa97015773594c1DB170817Cd7bAfA65274f6F99684D621c7668D6a3"),
		common.HexToAddress("0x011089363Ebb82e181710f45046803aFB35594071966F30BBA19293C8fDE6035"),
		common.HexToAddress("0x011142ca046cd3b331CFD61c3F35657CC103ddeA72b4a4db7092d28C9387D01A"),
		common.HexToAddress("0x01121ADD1F1bd0fd4036b872c11F28E04dBA9C672e7c3508648c8A2976d5aCfb"),
		common.HexToAddress("0x01134418C6C8be6eB6dB13d6F87737730596Ed7df4116E42535354e211680629"),
		common.HexToAddress("0x0114C2138c6deD444CEfad13FdF1F92A9c129503A1A66Ff4735b4969A401076F"),
		common.HexToAddress("0x0115C96863e67998E530B22F2EA2C3397D5094252daaf91F4d12b4dE1Ee39caa"),
		common.HexToAddress("0x011632c2a14157059134707Df1F8c3e2CdebFAc0C564ba8eEe09A9E647ec232C"),
		common.HexToAddress("0x011761e4BDFdB627b7F9f2F8dB2C3AA0f46F62030a98358aFEf03BcCcE453b85"),
		common.HexToAddress("0x011888597C05E7A5417A7FAFe740927e7e55CBBBCB0cBdfef86646ce666a79aA"),
		common.HexToAddress("0x0119096DD6EbCCfCE964a0ac8B668106B3A61bf1376066aD9Ae50700543004D7"),
		common.HexToAddress("0x011a5a2f714EE60a9793Da3022643Bd14cf09fcb0Aae92b9448DFe2BdD6ECdA0"),
		common.HexToAddress("0x011b2C602085aB2e8Dc163c1A2b7764841eA4D687F944609a3ba870FA7fBbC6a"),
		common.HexToAddress("0x011C62629D9480c0c1E3f574aa722DA589f7D1A1E9A6092c9b3aB4cE86184E24"),
		common.HexToAddress("0x011d53b45046Fc5003e2019a1d43691f02DdDbD415e7ccAC08f1886F37fC4137"),
		common.HexToAddress("0x011e285Ab5E001F09a828E22425883D2dfcAC1034dB230A9213335C158333CBE"),
		common.HexToAddress("0x011fda79e014E6fBf4d13197a44760825eE9152B61c0e610F3EE8a22B389bf70"),
		common.HexToAddress("0x0120AAd9b706453bDd09d52a86d65A59e7C6774dB0bbcDADf55F9b800E34Cb99"),
		common.HexToAddress("0x01218a329a0B2315Cc698BaeaC89034d4cf62632bb11F38EB6b33C8f80491A72"),
		common.HexToAddress("0x0122f46EB515CE34C71b95539E2052d0A8AE88a456a70CE1a06f30AB73EAb382"),
		common.HexToAddress("0x0123fF16694B4f965e55473EeF61a540CD675CacD41fe3bd992283f689891933"),
		common.HexToAddress("0x01244343cb4773430365B8c726391e9f45580a3b83C6DAe755AB7a8F5597C590"),
		common.HexToAddress("0x01257Ed054cAAFc36Fb1De72AB9C03f62FE3EFe046ca00Db35598Aa724766676"),
		common.HexToAddress("0x0126a395A6183Fa88a60b618aaE3D072FC16B26D6848f5c8da487B2812CC532A"),
		common.HexToAddress("0x01275C484d85ff2E8309cC098f3F8f3a284297327f988Af5828588Ab07704929"),
		common.HexToAddress("0x012873fDf62Bcb510BE40c74575ca6977e078D5910061a7E61CC43eCE0BF0c01"),
		common.HexToAddress("0x01297708208dc14c3aAd60c036a63B37871b69c49971a6400Db37D43a75D6C3b"),
		common.HexToAddress("0x012A7130C1b4d8E6FaC2b383BFe5F71b4d4b85fF6Ab2Ac10476893eEb112D895"),
		common.HexToAddress("0x012B5ac906EF886Cd0e73105fF8ad6F6712AEd8Ca7A0aa6d3Cc64f455180DdE0"),
		common.HexToAddress("0x012C58268eF85c0d9c8C28f9465b90Bb9298CeA83c92261Caf44C3322A65BD39"),
		common.HexToAddress("0x012d7CD9D8eae9E57bd2aC4121230b4ad4C0431433FB67B505c5588c39C7521A"),
		common.HexToAddress("0x012E5Cf21044C9F6F44C6c4E6828680b97583b6af2D1Fe64ad9295865c86Ccd8"),
		common.HexToAddress("0x012f671a63cA50df43155C976a7B8e21a28eaC7B48D2e396418285aF3922c28e"),
		common.HexToAddress("0x0130fD0fda69a2Bdb1325312b724d5C6068A94043f9c08E41FeC3B7343e8fa42"),
		common.HexToAddress("0x01315BeB3729805B7675Aa31065702BD6deEab8f7FEd3c57EeE73483A04d31bF"),
		common.HexToAddress("0x0132215218866658D07616f0e321ad65A5Eee422A9a584C4443DDe74C24a29B4"),
		common.HexToAddress("0x01338B8B3A84CA7B9f5715CaE06064EDcc5C605C94b2Eb5663d9605Fd7B5423f"),
		common.HexToAddress("0x013498D631E26dFB3F6A18d263381274b9fb9980293E1e1207bEc880378d7838"),
		common.HexToAddress("0x01352631014cf39d32CF88Fd69242805a9178308C785fA170C2cbf4658BCeccB"),
		common.HexToAddress("0x013635F3f415eCb3D522F202E193951c6F400102AF791fA91284975D3e16525b"),
		common.HexToAddress("0x01372cA7FC537884aa89ff703ab87E68f4fA2f91122cf0fBA1ED600CE39B5E2B"),
		common.HexToAddress("0x01385B0515de8Df1319843147E44E9141817a4a026c4D65fE0D6cF6A3A3DEeCB"),
		common.HexToAddress("0x0139883149dC7b578bbe40Ba8da218cE16e104FA3720ecD8EE7d14EF0F7b2790"),
		common.HexToAddress("0x013A6E8B19Fad26B606c56d73e03881b7DACf696E8A4f58880C775CCDaD40061"),
		common.HexToAddress("0x013bB12BE579c3a67B1087dC977B8902E2E3cBE8E4d05f6B8E7F77c803db134A"),
		common.HexToAddress("0x013cAee53b22Acc49ed385a95dDA02103785873f9087691055Bf48FA6975A2Ea"),
		common.HexToAddress("0x013D222c31007a4d90Ae6e0D38942A80E1ba8AF645b3fF3abF972f8D5570cf1F"),
		common.HexToAddress("0x013eC4965CC7240401ED7d06Da94Ae0c7b330b991cE91666110Ef1574BE85ED5"),
		common.HexToAddress("0x013F24BFEcBA5A008De1607b857E5Ab181bcB9eA55D74FeD566e32CF12d40aC5"),
		common.HexToAddress("0x0140317beD5bff465cB60b405e2412017fA60413294c5473e095f9366BDE8aBC"),
		common.HexToAddress("0x0141cC9bcC2Ee5F3526A3724A07B7f819A61d19d7f66F13541621Ed86CB471ae"),
		common.HexToAddress("0x014298bDA1A7F712657B845EA9D50fF7549842DCB92138aFFD15e16a3dF1d1B9"),
		common.HexToAddress("0x01430C7F6E52E5929a31c6b070cC638eFE0a363F47F70652B133f910f5AdFb5d"),
		common.HexToAddress("0x01448F203E26E56D37Daf10b86Aa2D349276bBce58b69727e4EcFa4Bcef27046"),
		common.HexToAddress("0x0145b86d2293ce232A6017BcfbE57d0F870444A1385E48C445Cc9b2Ac277271a"),
		common.HexToAddress("0x01467e2cA96D17449FFE6f192D84Ad6E05C49E804Bb638787492E236817D3771"),
		common.HexToAddress("0x0147d3f42DdAdC42019cb543B5a8d7EE9A62f76d31D86d7755754ec8696E7596"),
		common.HexToAddress("0x014857d3db40C6D9C25AA19A57ffb99705457BfE19565fB9403E2061f892e61d"),
		common.HexToAddress("0x0149Ae5dAD7485D450B7511C19D2aD0003B0695c354b74D0A4c93AB11d5f7E76"),
		common.HexToAddress("0x014aF1c446fE0B7c3B6a583AEE220677a4701B96bCAC3aaFb81b38b641a1624F"),
		common.HexToAddress("0x014b86ca176f0340c6d3825273591da5adb4323B1f58EBa362970Ba9182c582d"),
		common.HexToAddress("0x014c3A0fFF6C3e49887dA6Bca0332ADFFbAAf5e30A0beeE77950D313AB2E0D76"),
		common.HexToAddress("0x014Dbc5EcF6d173AE43F9A4c683e20a8E469e0CBF70a23d3E37FF098Fc3a54e3"),
		common.HexToAddress("0x014Ead2589fB133C46b440cbbfCF070E935Ec30Cd7d66882111CFA6B61419684"),
		common.HexToAddress("0x014F765adB646B7d64CE0Ee00B01FA3EC3B667Cf04D48c40A5e07D5C665F6736"),
		common.HexToAddress("0x0150CB9D22616C7B541A08F788fD91696Ff1754568Dd3107a3C2AD3c597F8Bfd"),
		common.HexToAddress("0x0151EF23D99F4eA25674dA8B6626cD4F5d86Baf71669178e3D7b48D402839dB9"),
		common.HexToAddress("0x0152A403b043C5EbA482809E7Dc50E6134FDf7Cc4d8Ba1402CD8355652de57AD"),
		common.HexToAddress("0x0153b548f695f971ff098E2Bf7d1053EC3d7A73e0A001797948a1147a056753E"),
		common.HexToAddress("0x01541529b16BF844bc2Af396B1aC9984Fec7cC579e272704279d7D6a09FD4717"),
		common.HexToAddress("0x0155B1be239d31f184550f3F99c3A7e510a0ae3F15712a758f5a6d74d058847F"),
		common.HexToAddress("0x0156EB64b9BC5B0Ba2E7f26fABB3dbA21BD450928F9e756bA0A18FF67Af47834"),
		common.HexToAddress("0x01577cd28cdD8Da24F797aF360539D07F65A96AAbFDcBB60c385A9e00d3a0bdE"),
		common.HexToAddress("0x015852C53700213EFC77606FCBD752429f560Ab8b162c0CDbF1862c838f5dEa8"),
		common.HexToAddress("0x01597309b398Dfc1542E13b94FDBa38ba70119Eb072162434C9044BdDaA523F0"),
		common.HexToAddress("0x015a28ab7b82eF28fc47685012776f477D55E5Bd583671CeD2cbA51B0F9E3A3E"),
		common.HexToAddress("0x015b41f2122048F524A5eBD6d569601264F59Bfe79711A3b19AAd4FD0f05784c"),
		common.HexToAddress("0x015c201c805088829185616D9770F83179703690CF3CFcf481391EB04B005479"),
		common.HexToAddress("0x015dac3B6900Cf22E8e12F46d0Fe2E1cB10133f3B572C408356D9dd355c44D41"),
		common.HexToAddress("0x015Ed33fCab9dcBE14C7AAe77fd3Aca56A1A30c5557A5BC6F4c12819aAE17687"),
		common.HexToAddress("0x015fE937766676041D408E8B7C506869478625D4887945b8fD0595356363220C"),
		common.HexToAddress("0x0160B404B07257CFb7B254ADE63FF7030B5bb91EfF9ddA908E85757766D09408"),
		common.HexToAddress("0x016115A5c330556DD7E6E48C12a947F0947D2698af751cED286d4D88F04d06f0"),
		common.HexToAddress("0x01627526CeFb76B31c38993A2FFE3E38922632B48428F76f8562022FA234EA63"),
		common.HexToAddress("0x0163b1B5805db50EA508616F9bA80d28dad2Fb3b2b320542E9Ce4D0FC1B4b7a8"),
		common.HexToAddress("0x01648120D9835CF295522D28e058876C067E1E8C5DbbdA5ff4765897ad9Ef4a4"),
		common.HexToAddress("0x0165a4DcC0215f3695F3cA3D8A99Febf2d7251c3A109F05d70451acCB71c045F"),
		common.HexToAddress("0x016608f08fA6fD4cDCA03B119e8191ea9621f3f9c0621Cae501B5CBBdCEd6e5a"),
		common.HexToAddress("0x0167E73327B03417af9EdaCD89491da933052dEb040b2b690d2269a2554E3A49"),
		common.HexToAddress("0x01681eE254a22c552b52A3fA5c389C0fa84B80563a4F97EE899d1FC6A20c48Ae"),
		common.HexToAddress("0x01696AAC516BAD303EaB95c7e9c7A4d08B918cd42B6fcd514b76AD07FefeDf92"),
		common.HexToAddress("0x016A69CE4ec49E0e7848Fd3E180f3fE48B4e7A38945407EC1349d070389d40b8"),
		common.HexToAddress("0x016B723E75270Ef32069b1222eee3b116ee7C65904F7A52F9A2eb9BA327d9337"),
		common.HexToAddress("0x016C6B2080dc518caa2749c17093534793c9896c5dc7F1197BE6e689BD8eA58f"),
		common.HexToAddress("0x016db9456C08Fd8e172aBC32c6b43E86906e4Ed81e2a58aE994153746EFE3386"),
		common.HexToAddress("0x016E991DD62f1e6B6AD0ceff80A9827a0594Aa86e3C2df096FA474Ad4BF4fdb5"),
		common.HexToAddress("0x016Fa191833163C792f3FCd73b1caeB1E0294D700778AC6b25E1C6c70BD39a5f"),
		common.HexToAddress("0x0170bEA3D9096344627931E14F6257Eb996629B8427055d602683EE688d6955D"),
		common.HexToAddress("0x0171068aF28B59969a70D41414F4621bAa1102E70cF775269e64b6Ed961bD01D"),
		common.HexToAddress("0x01722147358A39504B57B3913F28113Bb42419aAB2e158a41b10C8238a0c6817"),
		common.HexToAddress("0x0173F2bC3e512F25a8354E60b358C0E4f0f8078FD360C978942544a759f6Aa23"),
		common.HexToAddress("0x0174281290EAC8BCd0CDf138cF450738Ae54E7f79952b7bBc9bB9eC117011e65"),
		common.HexToAddress("0x01759ef814A6903fD92997bf46CCacfEeec73d34006108d4825F0b0Ba490985c"),
		common.HexToAddress("0x01760a02836d1EB2aE7980109bd38EA49778065Eb7c79955D284bC66d272eaD9"),
		common.HexToAddress("0x0177f1F49E2Cfe7aF65c84e0157e550679053B6964c7003C33D6B34948248958"),
		common.HexToAddress("0x01783dfa8a6870fFbE522541066F37152A24dfca0cE998151F8784E005Bf2035"),
		common.HexToAddress("0x0179987DF3167468F3439a246bd0306D491b5AE8B4a50bc2C5d6eB028d0AdFb5"),
		common.HexToAddress("0x017Af56583B4780724063F84BBb166288cFB8dd3876A4cDaeac97210B3684F71"),
		common.HexToAddress("0x017B9f65d83aEC023D52D575651DAD9D11Aa4761DA4ec3b3B26b7fcDeD6A8754"),
		common.HexToAddress("0x017C6422373D699EbDb6479617CeFC1C14103fE825c29D8bAa10121ad2941790"),
		common.HexToAddress("0x017De5dA2a2a800BFf69730Da70Fc71747586263f687C9B81aCf03d373cca5e7"),
		common.HexToAddress("0x017EE8EeBf2D7F06f7d155787E86443ceb7c6d82A636c241949330871768B508"),
		common.HexToAddress("0x017f2770A1eF459e117cDbBe4365b7b8995ddFf99C7572fD6AcdCF4D071A87B4"),
		common.HexToAddress("0x0180D80D8c22138dF6Bb031d1fB8171a4f6A09dEC965c0a2A1b556E25a7Aa0fb"),
		common.HexToAddress("0x0181C85f2d8f23e1Aa5Eb340eEee131E179bef3C8c8D07beFB3f3Fa19eAA9738"),
		common.HexToAddress("0x01820152f4a2d603B578C8e77b5392bA8d6505Ec6c73295FF322815f3d93C3a5"),
		common.HexToAddress("0x0183877C53418259e68eB857f35C79d566F670C04B44141EB3034f7d210cc28B"),
		common.HexToAddress("0x0184dBA88177563C6858cAb205e7C05E221c745E595DA2109a9D49Bb14e92e87"),
		common.HexToAddress("0x01857C5911866F07B8C5f8f6E4fbAa808140843286cB0575b19526b5AB0C5Ef0"),
		common.HexToAddress("0x01862A6aBA2C06157743C9340d71a861b93FF1FF0D05788DDDE4f6ca06F11F6b"),
		common.HexToAddress("0x0187e90807da2354889976E3487b5017Ec6556437C8Ed33713F6F4C0f707DF93"),
		common.HexToAddress("0x0188c698dCeBE3d77F5E75497e4f82164fE3f8f924F4e745E7Fe0FF0aEe4aF96"),
		common.HexToAddress("0x0189C4dC74d3b38e16C9FB4C9A44cc6304690924866787eB99cBAc31bDe26Ddd"),
		common.HexToAddress("0x018aaa910D911c6E96aFEE0B4D2367356B03fb693D2444B2313C6E4Ec7B3D5f8"),
		common.HexToAddress("0x018b1b6457a554De172Ec660a43ac9028FE7313A7F6C8aaA8bCb7DD23Ab6f814"),
		common.HexToAddress("0x018C31d9e97bcBF009B9E8E34d96225F641f5D13317B3f905EBA07da81C58FC0"),
		common.HexToAddress("0x018D43f9a4370ae5c591DB53e335Ef24fe043480501F6c9AF64e12ba63555488"),
		common.HexToAddress("0x018ed91bABe0F44F8Ef19DF45B6adFE2940df628479D31F634f7827BAF357D37"),
		common.HexToAddress("0x018F546efab048D81e1bF479b22BC7f447D107246eCCAba0E3d93e6FCE86a283"),
		common.HexToAddress("0x0190571eAf3539305f7c64ca03364e1131ea8A741Acfc7e0D568E9a21DB62105"),
		common.HexToAddress("0x01916bf195Af6A706Af6D1c8DEFb6F6AAE8bc56b8ce01a32375be45b7F2dc284"),
		common.HexToAddress("0x0192da2051861dDF45a0Da4B87361f7DD208032556AA06A44eaBe0dcE342c9d2"),
		common.HexToAddress("0x019384e85B9B22A1Ac6De847D2D4C8137E0bdad91ecc9223D6ffDB1be2F20e0A"),
		common.HexToAddress("0x0194A6A69b90EF3e3D1B75b13A6451f4D45dDC932A51409FB3DaB5149444468a"),
		common.HexToAddress("0x01958C2f30db9E57c1a436aca5fe63d165D6219d687a1F39a2E1598962726851"),
		common.HexToAddress("0x019617C8B7357da9F582D8417ec7552c279c0E77FeFF63A6822E82E464B796E2"),
		common.HexToAddress("0x01977c59d592ef6Bc7C89791edA198cB14145aA9314d786eD806Fca1562F0f3f"),
		common.HexToAddress("0x0198b0e9E5D8dd26503BEb1a6ebb67000ae6eF4AE541A015413Bb23c5fEDD761"),
		common.HexToAddress("0x0199588B9426BAccb86454E77e1EC21c51B202bF46d0DE02CbC2BEf896dC7Da3"),
		common.HexToAddress("0x019A0eC772Dd6EE968e637602a05ea1B386032dc884Aa8Ea9663b1F8AE39d1E7"),
		common.HexToAddress("0x019B58db19f876bbFE55c00DaAa43BaFAfcC7eFca572328741129E143b96DC91"),
		common.HexToAddress("0x019C0422E124BCE25124b4772F8b77afcAAC382BEDbbCcbd1B65066FA0A1a2e3"),
		common.HexToAddress("0x019dd93840E0f1C952Af553ed44DA2898E32eF75c7bF69de7Ee24Ce0bc803dA5"),
		common.HexToAddress("0x019ebd1D0DE63b700269C9D64727DdBE9eC5d7B2153Cf9AE8097884a68EEf3a0"),
		common.HexToAddress("0x019FE3D22e9C767CBf58f4EceB7d8844C411CE7925dB3A8A78FeCa40F14bF03c"),
		common.HexToAddress("0x01a0237CB0202f5407fd2c79B194Be0D05112A28A40D576fe55534819fb29642"),
		common.HexToAddress("0x01a1b8f01c3417Dc3dc0eE8905da8De359755cE711D5B816c1204f4399C32847"),
		common.HexToAddress("0x01a21029626CBAc55480CF2B481cD1862924470F17a2d4B8d2eb3f80c7D6Df93"),
		common.HexToAddress("0x01a327c4d846eFA4bbB48981cA64298F6948F279f35b131a175ea4b72e1E0d7c"),
		common.HexToAddress("0x01A49F4BCdCb48b9CB2bd133E8623eeeE5F17f7583B7aED59C6827A20d039aDd"),
		common.HexToAddress("0x01A5b870DdD8812bDce95AbF547f113f9d32F58BB8989173a2232f13653cBA7b"),
		common.HexToAddress("0x01a6A86c3b2358713CA210e294958A102Dc2563ab5821dCaaeAAe31Da546B447"),
		common.HexToAddress("0x01A79132920DbBFDec25B50488C4D47EeBd9b20F5BD30FD8eC21532041eF648F"),
		common.HexToAddress("0x01A8E072d849d5389B597bbE5FBfee3Fd18Fcf440FcB134F25aeb7Fdaf73E3eb"),
		common.HexToAddress("0x01a9d6571288eD4FCF1adAc96f5362B1FBf0763ebb5a08f163Ec1430C5c89A7c"),
		common.HexToAddress("0x01Aa0A90C302140B244cc345C34eFfb28BCf258Ca835e673B1D95049fb566404"),
		common.HexToAddress("0x01ab6c48E041436Cae4ff449C4f2CBEf55997F9a322C5833DE13B8f72Ab2Fb17"),
		common.HexToAddress("0x01Ac23e2e6055bCe4c6288837eb92391A3A129b3Dac297Ef5408331EB344E591"),
		common.HexToAddress("0x01aDBdEBcc43F3Af21a52Df5e7F8CF186ccd113fBd488cF99759aB5919d90ADA"),
		common.HexToAddress("0x01AEe5EF8D7b7BCF140A70658db8A3fc5D8D781A61310eBFdfFd64fa75e7fa72"),
		common.HexToAddress("0x01AF21Cefff5e89a041028f63839C6bba783D7E21DCCD8499bE81Cd48f01bfF2"),
		common.HexToAddress("0x01B019C44594F1B1645dE578EeA3BbCe9e584Fd395f68E8Fd70F73f5Ff019758"),
		common.HexToAddress("0x01B12496830890ebbd904F440622393C96AddDAE541a3818B96906CC29962cF2"),
		common.HexToAddress("0x01b23C3AFDCfb7114739339C169D7bDaC92878E4eB5d44b156d20bDDA82b1853"),
		common.HexToAddress("0x01B36225D9C40A6121d06471335679C38218dDD7881Ce3f4cDbEFF72CC4496b8"),
		common.HexToAddress("0x01B47320ba5D1D667635cAb7F62Ebc540aaa8656b3B04AD39A9E7eAaAFF2a034"),
		common.HexToAddress("0x01B566B19025ee1aF1dCc2DE2e90BD606840a6f005fF83bab78bDd9d786F5c97"),
		common.HexToAddress("0x01b600d232d374C89B26C672975B901ECFa8BA4F574aE42c6Ca0A32F930c67e3"),
		common.HexToAddress("0x01B7295fE7dc5C6159FcAab255094092f2b4C335e1E941F865e2D9ac1610519a"),
		common.HexToAddress("0x01B8D8F5bE7bC2C0E5D192303b77B85Eb2D9E0236797F653ff042f9226ae60Df"),
		common.HexToAddress("0x01b934E8fa66d69c87087F7fc8B325f03Ed4bf32baaAf7fA15A5Fd71FED2e9EC"),
		common.HexToAddress("0x01bA552E710A74483c2392afED3a1Cbc981234E55a9F415c8c16a448Cf5B0dAc"),
		common.HexToAddress("0x01BBa24a37a859D0e3E1Dcb3797b66dE176781e0Ce714761bB2350e057545552"),
		common.HexToAddress("0x01bC2B3af816b4C200906eC41bB1C81A3c91afDac6dC4959375C1dfD35225f22"),
		common.HexToAddress("0x01BDc4a7089B97B35D70F3727BDB5656506B5CCCd66e512912BC7eE985eC80D7"),
		common.HexToAddress("0x01BeEF2248e88fb96E4Fb81D80352A560434d6Ac06f942c00EC62c024ed3FF0B"),
		common.HexToAddress("0x01BF55c70D72ef37214233C684D708666E35637D06fa13c7311d61607a84C473"),
		common.HexToAddress("0x01C0dC6541BF80611cF07e5360588BB24b4156199EEe0C02CcebdF769DeB53D6"),
		common.HexToAddress("0x01c1A26aBe32aFa1349da476D599a8aA749C24F5A4Cd311279469E70140ECe70"),
		common.HexToAddress("0x01c266d91D7EFf1BA71a682989EfBEAa8434114624Aa273A4A80c551B233Ca3B"),
		common.HexToAddress("0x01c3cFC4376821c198eb3279b76AFa206Dc93f5A69AB0931A6BAd26B0058C058"),
		common.HexToAddress("0x01c472233AF751b499ffF154CC1CC41336DE58990158a6b622432Fa9FB8c84BB"),
		common.HexToAddress("0x01c5Bcc1bda6825ba3Eb6F26927C32096732b3a9790556bBbD5fab8Ff32923aB"),
		common.HexToAddress("0x01c6287CC48B27d37A3C0F53C5F789400f74D4DF0845F4b74e8a9Ed8796E6Fcf"),
		common.HexToAddress("0x01c72358D555C199a969e0c16895081425ef91b09C676bf0D1F5Be6dA92CCf69"),
		common.HexToAddress("0x01c8D3B672B018Ab444506C20E35DAFF89Db16A27b4440938Acc687e0FB876f7"),
		common.HexToAddress("0x01c926C313033C6a9a9ff05bdA4B101Ff2C2e1891Ab8cee4BCF12f137B94f4dB"),
		common.HexToAddress("0x01cA998cCB7f1193E68ac7FB849E5a33FfF871B8D0B7a85E69EC737ED79bDD89"),
		common.HexToAddress("0x01cB08537Bd2f09FDa2313Afe321E58b460460e00b00D8C6410B327bfB46A43A"),
		common.HexToAddress("0x01cC8412Ea89D147B053b92485002f3744f5BF8Ea5AF12E3e1E19Ca7fC989B2F"),
		common.HexToAddress("0x01CdA92A9AE74c235A859D539ccC0bF50854983E3F6717c0eDA8DDbE54fc1F63"),
		common.HexToAddress("0x01CE8c633af586fA8CC1704A0B5545D8F69C315D81B2F45cf9002B1122dc9FE6"),
		common.HexToAddress("0x01cfF21be4d40755Ff857F290e2a44450b28f4F98274A4fAE0453Ff57B478eEF"),
		common.HexToAddress("0x01d0fc8ABc78e5f704317372d6Caf25800eb8977da6f37ca5fB1abD0e87f99eF"),
		common.HexToAddress("0x01D14C5fc931Bd6f5f3fd8eBCd64E353613F7709017238cFD31e686152eB8Fa7"),
		common.HexToAddress("0x01D2e3dA7D0a72c871E5fCAabD1A8F877386be3803D5a814E14665e1a7E772Ac"),
		common.HexToAddress("0x01d34981165986317Ec4C95F9A2C775A7F3c2594879DCcCB741012f42bAD37D3"),
		common.HexToAddress("0x01d44ba391c20e5Ac705c12e21b83943578a8a800DF5254e9F156001F08d8aB0"),
		common.HexToAddress("0x01D5B939d7a9E2Da66Ed5C6F9c5Cf500D1253a56B2cebd208B85B69082004297"),
		common.HexToAddress("0x01D6208D32a037D92aF8A67ffc481a0CEdfd15400B7A38f75f0421872B45A678"),
		common.HexToAddress("0x01D7AbD3E6f0E08Bd2956B0dCbCF10642485392ae63D6830d11412714897688a"),
		common.HexToAddress("0x01D8b4Df9E27884875BBd569353975E5d673D4aF8baAD2FaEBF16505426A0329"),
		common.HexToAddress("0x01d917d5E5a85C285eE00E14c5EaD51a0F7fa2b01A3f40Fe32F4EaD75A286B7d"),
		common.HexToAddress("0x01Da07f2BBD985bd55ECC868b378f83F4AE8139B912B88BB7c706d0fE5598D62"),
		common.HexToAddress("0x01dBB3AB16b0c6C0E9d6E3aa65489F805f87B0112Fe5d4a0F4fb3da2ba983C95"),
		common.HexToAddress("0x01Dca23288267748c7CF46B20205d05deaC53e72301Bc05ac371De9FA3763548"),
		common.HexToAddress("0x01dD165555155f8c78f3679DDc59A5905e5Bbf27e205fd917fF43Aee389e4E94"),
		common.HexToAddress("0x01dE48422baA31405D0b190bbD8E9290A450C0a8187ccDdb2D2a300BA41e3b4E"),
		common.HexToAddress("0x01DFD146fc7F604937dcA5F6Ae330D697acE671B28c76fb560b7a12F05a2B906"),
		common.HexToAddress("0x01e021eAebF5cA50e69B396640414670d0eC26a4A2A24fa877c0C87678d2FCfd"),
		common.HexToAddress("0x01E1a6CD59db6071AA35B9aabF068D7298F3AE3D8A71Fd646F422eE2226d0eeA"),
		common.HexToAddress("0x01e27EA4526B527798a4D6b8f719eCE3F94933182142ec5C84BaE5B0816B7030"),
		common.HexToAddress("0x01e3d2B767887a70ef8397c492021CA698BE7477940c246429257C9fF4f5128C"),
		common.HexToAddress("0x01E4ED99C6C947547AFEF483c790Ae70868a1a329416f7d644a44e169503709F"),
		common.HexToAddress("0x01E536505D8EA4693f77f055d955170BB56F084501aE67cFAA897Cc1BEBba51b"),
		common.HexToAddress("0x01E6707282428f5c5fCC15BFCf35D53eE8BfD5194C697cD19E11DB27d2e611d5"),
		common.HexToAddress("0x01e7BdD4d9378D2fdE7F5448CAC8BfC8904669FE16dA4354A1c76bCcBaC0aA51"),
		common.HexToAddress("0x01e8C83D5B8619316EE052fE3FC5Aa1DF4D4fa80E72F7b43c882A1733CBb2E0e"),
		common.HexToAddress("0x01e9bC77e7fd37Fe78C23b54D869b6B6Dc24DB027ba04942042c9dF6d150cB32"),
		common.HexToAddress("0x01EAbB1A81853Bb7fa1A97862ab7Ca64b67698d8Bd5aeD3c96BE554Da8921F6b"),
		common.HexToAddress("0x01Eb97D4afe1C2Fc613a3F2f43208e931A01D4F77ceCF512D0bbb991a8004771"),
		common.HexToAddress("0x01Ec6A7055933f9c10622e4ea3190E142a360e8074bD7D9437f4290Ad843Aa6a"),
		common.HexToAddress("0x01eDB0349Da68517898E7BeB744aEeFaC6f2204c207C6D49F6f66bB65be9cED4"),
		common.HexToAddress("0x01ee9A389C4dCF1473a18be922D27CEb06E57b143f1607E8005B9BE869F47071"),
		common.HexToAddress("0x01eF907e418a9C4f2074d9ea00ABfd97206f1b95843de96b550946803FA75F80"),
		common.HexToAddress("0x01F01Becf015e988363c7aC2f0Cd0309569e1339e3e3603E980FB95a2c3508e9"),
		common.HexToAddress("0x01f1D7a77A8C869Ce8eF34b29a3D2FA4278A3C794b4bfE25f98A78c75F6c5cE3"),
		common.HexToAddress("0x01f24845A8683E1EC8238ddD8656Ddd8C7b08b48a1ea2850fE0F750a9dF61918"),
		common.HexToAddress("0x01F332dC039b5Af77357fcB30F778a53B295fcfe11AB9794FF88F445f3f7ceDA"),
		common.HexToAddress("0x01F462a47FD9ec48915020e1790C954B7e7834e170c343D9Dbf7d1114445d0C3"),
		common.HexToAddress("0x01F5c27854a7cb38BAAf661CAF03F9c20dA1668af43AABadb3E43A51AE078D0B"),
		common.HexToAddress("0x01F63ba5AC6BfF2Da58EDEAA78d840810CEf7BADAb9c84051D64C8c7B387d46c"),
		common.HexToAddress("0x01F7A206c1b0ba3bf108c1D6F2721e9646edfaCF81Ff91b0a37ed30fee4446c9"),
		common.HexToAddress("0x01F85E05ffbfDa12307a33796e9d65A956db76832201D10589B7a0aF62d935dC"),
		common.HexToAddress("0x01f9440Ee82103690fb948ce29749C3ef66cA8536aC0b4b89809224a2ecAa453"),
		common.HexToAddress("0x01fA4D689b5B8EfEA8058daCD95c0535987fD68A43DD547B9ad3A9A5e78E6896"),
		common.HexToAddress("0x01FB20972163b301dEf812eb513C9557EC934b6FD7dB0AbB9d24AaB8414147B8"),
		common.HexToAddress("0x01Fc5F3613e263b8523070F3F22f2109bE5d2DEaF6CE8835Ce886A04f35290Bf"),
		common.HexToAddress("0x01FD235531C928c4E2677737f5A645f7b6B9fbcbDB43E47A58C5746636953cF7"),
	}
	return addresses
}

func NewTopGenesisBlock(db DatabaseWriter) *types.Block {

	// tx1 := types.NewGenesisTransaction(common.BytesToAddress([]byte{0x1, 0x11}), big.NewInt(111), 1, big.NewInt(1))
	// tx2 := types.NewGenesisTransaction(common.BytesToAddress([]byte{0x2, 0x11}), big.NewInt(222), 1, big.NewInt(1))
	// tx3 := types.NewGenesisTransaction(common.BytesToAddress([]byte{0x3, 0x11}), big.NewInt(333), 1, big.NewInt(1))

	// Create a test block to move around the database and make sure it's really new
	tx1 := types.NewTransaction(common.BytesToAddress([]byte{0x1, 0x11}), common.Hash{0x0}, types.NORMAL_TX, common.Hash{0x0}, big.NewInt(111), 1111, big.NewInt(11111), nil, []byte{0x11, 0x11, 0x11})
	tx2 := types.NewTransaction(common.BytesToAddress([]byte{0x2, 0x11}), common.Hash{0x0}, types.NORMAL_TX, common.Hash{0x0}, big.NewInt(222), 2222, big.NewInt(22222), nil, []byte{0x22, 0x22, 0x22})
	tx3 := types.NewTransaction(common.BytesToAddress([]byte{0x3, 0x11}), common.Hash{0x0}, types.NORMAL_TX, common.Hash{0x0}, big.NewInt(333), 3333, big.NewInt(33333), nil, []byte{0x33, 0x33, 0x33})
	//	txs := []*types.Transaction{tx1, tx2, tx3}

	txs := []*types.Transaction{tx1, tx2, tx3}

	header := &types.Header{
		Type:        0x0,
		ShardId:     0x0,
		Time:        new(big.Int).SetUint64(uint64(time.Now().Unix())),
		UncleHash:   types.EmptyUncleHash,
		TxHash:      types.EmptyRootHash,
		ReceiptHash: types.EmptyRootHash,
		Difficulty:  big.NewInt(1),
		GasLimit:    1000000000,
		Protocol:    &devotedb.DevoteProtocol{CycleHash: common.Hash{byte(0xff)}, StatsHash: common.Hash{byte(0xff)}},
	}
	signers := masternodeAddress()
	header.Extra = make([]byte, 32+len(signers)*common.AddressLength+65)
	for i, signer := range signers {
		copy(header.Extra[32+i*common.AddressLength:], signer[:])
	}
	topGenesisBlock := types.NewBlock(header, txs, nil, nil)
	return topGenesisBlock

}

/*
Test functions:
	-ReadTopHeader
	-ReadTopBlock
	-WriteTopHeader
	-WriteTopBlock
*/

func TestTopGenesisBlockStorage(t *testing.T) {
	db := ethdb.NewMemDatabase()

	topGenesisBlock := NewTopGenesisBlock(db)
	//t.Log("Reading Top Header", "size", topGenesisBlock.Size(), "ExtraSize:", len(topGenesisBlock.Header().Extra))
	topHeader := ReadTopHeader(db)
	if topHeader != nil {
		t.Fatalf("Error Top header is exist %v", topHeader)
	}

	var headJson []byte
	//	var bodyJson []byte
	//	var blockJson []byte
	//	headJson, _ = topGenesisBlock.Header().MarshalJSON()
	//	bodyJson, _ = json.Marshal(topGenesisBlock.Body())
	//	blockJson, _ = json.Marshal(topGenesisBlock)
	//	t.Logf("Write Top header %s\nBody:\n%s\nBlock:\n%s", string(headJson), string(bodyJson), string(blockJson))

	WriteTopBlock(db, topGenesisBlock)
	topHeader = ReadTopHeader(db)
	if topHeader == nil {
		//t.Fatalf("Error Top header is exist %v", topHeader)
		t.Log("Error Top header is not exist")
	} else {
		headJson, _ = topHeader.MarshalJSON()
		//t.Logf("Top header is exist %s", string(headJson))
	}
	topBlock := ReadTopBlock(db)
	if topBlock == nil {
		t.Fatalf("Error read top block")
	}
	headJson, _ = topBlock.Header().MarshalJSON()
	if len(headJson) > 0 {
		//t.Logf("Top Genesis header is %s", string(headJson))
	}
}

func NewGenesisBlock(genesis *types.Block, chainType, shard uint64) *types.Block {
	var (
		extraVanity = 32
		extraSeal   = 65
	)
	header := &types.Header{
		Type:        uint(chainType),
		ShardId:     shard,
		ParentHash:  genesis.Header().Hash(),
		UncleHash:   types.EmptyUncleHash,
		TxHash:      types.EmptyRootHash,
		ReceiptHash: types.EmptyRootHash,
		Difficulty:  big.NewInt(1),
		Time:        genesis.Time(),
		GasLimit:    genesis.GasLimit(),
		Protocol:    &devotedb.DevoteProtocol{CycleHash: common.Hash{byte(shard)}, StatsHash: common.Hash{byte(shard)}},
	}
	txGenesis := make([]*types.Transaction, 0)
	if chainType == types.BLOCK_FOREIGN {
		for _, tx := range genesis.Transactions() {
			txGenesis = append(txGenesis, tx)
		}
	}

	extra := genesis.Extra()
	genesisSigners := make([]common.Address, (len(extra)-extraVanity-extraSeal)/common.AddressLength)
	for i := 0; i < len(genesisSigners); i++ {
		copy(genesisSigners[i][:], extra[extraVanity+i*common.AddressLength:])
	}
	signers := make([]common.Address, 0)
	for i := 0; i < len(genesisSigners); i++ {
		if genesisSigners[i].Shard() == shard {
			signers = append(signers, genesisSigners[i])
		}
	}
	header.Extra = make([]byte, 32+len(signers)*common.AddressLength+65)
	for i, signer := range signers {
		copy(header.Extra[32+i*common.AddressLength:], signer[:])
	}
	//	signerCount := len(extra) - 32 - 65
	//	header.Extra = make
	//	for i := 0; i < signerCount/common.AddressLength; i++ {
	//	}
	block := types.NewBlock(header, txGenesis, nil, nil)
	block.ReceivedAt = time.Unix(block.Time().Int64(), 0)

	return block
}

// Tests block storage and retrieval operations.
func TestGenesisBlockStorage(t *testing.T) {
	var (
		headJson      []byte
		bodyJson      []byte
		bodyLightJson []byte
		blockJson     []byte
	)

	db := ethdb.NewMemDatabase()

	// Create top genesis block
	topBlock := NewTopGenesisBlock(db)
	WriteTopBlock(db, topBlock)

	headJson, _ = topBlock.Header().MarshalJSON()
	if len(headJson) > 0 {
		//t.Logf("Top Genesis header is %s\n\n", string(headJson))
	}

	maxShard := 1
	for i := 0; i < maxShard; i++ {
		homeBlock := NewGenesisBlock(topBlock, uint64(types.BLOCK_HOME), uint64(i))
		homeBlock.DevoteDB, _ = devotedb.NewDevoteByProtocol(devotedb.NewDatabase(db), homeBlock.Header().Protocol)

		foreignBlock := NewGenesisBlock(topBlock, uint64(types.BLOCK_FOREIGN), uint64(i))

		headJson, _ = homeBlock.Header().MarshalJSON()
		bodyJson, _ = json.Marshal(homeBlock.Body())
		bodyLightJson, _ = json.Marshal(homeBlock.BodyLight())
		blockJson, _ = json.Marshal(homeBlock)
		t.Logf("\n\n\nShard %x size: %d home header %s\nBody:\n%s\n\nLight:\n%s", homeBlock.Size(), i, string(headJson), string(bodyJson), string(bodyLightJson))

		headJson, _ = foreignBlock.Header().MarshalJSON()
		bodyJson, _ = json.Marshal(foreignBlock.Body())
		bodyLightJson, _ = json.Marshal(foreignBlock.BodyLight())
		blockJson, _ = json.Marshal(foreignBlock)

		//WriteBodyLight(db, foreignBlock.Hash(), foreignBlock.NumberU64(), foreignBlock.BodyLight())
		//bodyLight := ReadBodyLight(db, foreignBlock.Hash(), 0)
		//bodyLightJson2, _ := json.Marshal(bodyLight)
		t.Logf("\n\nShard %x size:%v foreign header %s\nBody:\n%s\n\nBodyLight:\n%s", foreignBlock.Size(), i, string(headJson), string(bodyJson), string(bodyLightJson))

		t.Log("===========Write genesis", "shard", i, "number", foreignBlock.NumberU64(), "hash", hexutil.Encode(foreignBlock.Header().Hash().Bytes()), "data", string(bodyLightJson))

		WriteBlock(db, homeBlock)
		WriteBlock(db, foreignBlock)
		WriteCanonicalHash(db, uint64(homeBlock.Type()), uint64(homeBlock.ShardId()), homeBlock.NumberU64(), homeBlock.Header().Hash())
		WriteCanonicalHash(db, uint64(foreignBlock.Type()), uint64(foreignBlock.ShardId()), foreignBlock.NumberU64(), foreignBlock.Header().Hash())
		bodyLight := ReadBodyLight(db, foreignBlock.Hash(), 0)
		bodyLightJson2, _ := json.Marshal(bodyLight)
		t.Logf("\n\nbodyLightJson2 %d %s\n\n", foreignBlock.NumberU64(), string(bodyLightJson2))
	}
	t.Log("===========READ BACK=================================")
	for i := 0; i < maxShard; i++ {
		hash := ReadCanonicalHash(db, uint64(types.BLOCK_FOREIGN), uint64(i), 0)
		if hash != (common.Hash{}) {
			t.Log("Read genesis shard", i, "hash", hexutil.Encode(hash.Bytes()))
		}
		homeBlock := ReadBlock(db, hash, 0)

		headJson, _ = homeBlock.Header().MarshalJSON()
		bodyJson, _ = json.Marshal(homeBlock.Body())
		blockJson, _ = json.Marshal(homeBlock)
		bodyLight := ReadBodyLight(db, hash, 0)
		bodyLightJson, _ := json.Marshal(bodyLight)
		t.Logf("\n\nRead Shard %x header %s\nBody:\n%s\nBlock:\n%s\nTransactions:%d", i, string(headJson), string(bodyLightJson), string(blockJson), len(homeBlock.Transactions()))

	}
}

// Tests block storage and retrieval operations.
func TestAccountHeadStorage(t *testing.T) {
	var (
		headJson      []byte
		bodyJson      []byte
		bodyLightJson []byte
		blockJson     []byte
	)

	db := ethdb.NewMemDatabase()

	// Create top genesis block
	topBlock := NewTopGenesisBlock(db)
	WriteTopBlock(db, topBlock)

	headJson, _ = topBlock.Header().MarshalJSON()
	if len(headJson) > 0 {
		//t.Logf("Top Genesis header is %s\n\n", string(headJson))
	}

	maxShard := 1
	for i := 0; i < maxShard; i++ {
		homeBlock := NewGenesisBlock(topBlock, uint64(types.BLOCK_HOME), uint64(i))
		homeBlock.DevoteDB, _ = devotedb.NewDevoteByProtocol(devotedb.NewDatabase(db), homeBlock.Header().Protocol)

		foreignBlock := NewGenesisBlock(topBlock, uint64(types.BLOCK_FOREIGN), uint64(i))

		headJson, _ = homeBlock.Header().MarshalJSON()
		bodyJson, _ = json.Marshal(homeBlock.Body())
		bodyLightJson, _ = json.Marshal(homeBlock.BodyLight())
		blockJson, _ = json.Marshal(homeBlock)
		t.Logf("\n\n\nShard %x size: %d home header %s\nBody:\n%s\n\nLight:\n%s", homeBlock.Size(), i, string(headJson), string(bodyJson), string(bodyLightJson))

		headJson, _ = foreignBlock.Header().MarshalJSON()
		bodyJson, _ = json.Marshal(foreignBlock.Body())
		bodyLightJson, _ = json.Marshal(foreignBlock.BodyLight())
		blockJson, _ = json.Marshal(foreignBlock)

		//WriteBodyLight(db, foreignBlock.Hash(), foreignBlock.NumberU64(), foreignBlock.BodyLight())
		//bodyLight := ReadBodyLight(db, foreignBlock.Hash(), 0)
		//bodyLightJson2, _ := json.Marshal(bodyLight)
		t.Logf("\n\nShard %x size:%v foreign header %s\nBody:\n%s\n\nBodyLight:\n%s", foreignBlock.Size(), i, string(headJson), string(bodyJson), string(bodyLightJson))

		t.Log("===========Write genesis", "shard", i, "number", foreignBlock.NumberU64(), "hash", hexutil.Encode(foreignBlock.Header().Hash().Bytes()), "data", string(bodyLightJson))

		WriteBlock(db, homeBlock)
		WriteBlock(db, foreignBlock)
		WriteCanonicalHash(db, uint64(homeBlock.Type()), uint64(homeBlock.ShardId()), homeBlock.NumberU64(), homeBlock.Header().Hash())
		WriteCanonicalHash(db, uint64(foreignBlock.Type()), uint64(foreignBlock.ShardId()), foreignBlock.NumberU64(), foreignBlock.Header().Hash())
		bodyLight := ReadBodyLight(db, foreignBlock.Hash(), 0)
		bodyLightJson2, _ := json.Marshal(bodyLight)
		t.Logf("\n\nbodyLightJson2 %d %s\n\n", foreignBlock.NumberU64(), string(bodyLightJson2))
	}
	t.Log("===========READ BACK=================================")
	for i := 0; i < maxShard; i++ {
		hash := ReadCanonicalHash(db, uint64(types.BLOCK_FOREIGN), uint64(i), 0)
		if hash != (common.Hash{}) {
			t.Log("Read genesis shard", i, "hash", hexutil.Encode(hash.Bytes()))
		}
		homeBlock := ReadBlock(db, hash, 0)

		headJson, _ = homeBlock.Header().MarshalJSON()
		bodyJson, _ = json.Marshal(homeBlock.Body())
		blockJson, _ = json.Marshal(homeBlock)
		bodyLight := ReadBodyLight(db, hash, 0)
		bodyLightJson, _ := json.Marshal(bodyLight)
		t.Logf("\n\nRead Shard %x header %s\nBody:\n%s\nBlock:\n%s\nTransactions:%d", i, string(headJson), string(bodyLightJson), string(blockJson), len(homeBlock.Transactions()))

	}
}

func TestAccountStorage(t *testing.T) {
	db := ethdb.NewMemDatabase()

	//topBlock := NewTopGenesisBlock(db)
	//WriteTopBlock(db, topBlock)

	chainType := uint(types.BLOCK_FOREIGN)
	shard := uint64(0x1)

	tx1 := types.NewTransaction(common.BytesToAddress([]byte{0x11}), common.Hash{0x0}, types.NORMAL_RX, common.Hash{0x0}, big.NewInt(111), 1111, big.NewInt(11111), nil, []byte{0x11, 0x11, 0x11})
	tx2 := types.NewTransaction(common.BytesToAddress([]byte{0x22}), common.Hash{0x0}, types.NORMAL_RX, common.Hash{0x0}, big.NewInt(222), 2222, big.NewInt(22222), nil, []byte{0x22, 0x22, 0x22})
	tx3 := types.NewTransaction(common.BytesToAddress([]byte{0x33}), common.Hash{0x0}, types.NORMAL_RX, common.Hash{0x0}, big.NewInt(333), 3333, big.NewInt(33333), nil, []byte{0x33, 0x33, 0x33})
	//tx4 := types.NewTransaction(common.BytesToAddress([]byte{0x33}), common.Hash{0x0}, types.NORMAL_RX, common.Hash{0x0}, big.NewInt(333), 3333, big.NewInt(33333), nil, []byte{0x33, 0x33, 0x33})

	_, edPrivateKey1, _ := ed25519.GenerateKey(rand.Reader)
	newSubkey := edPrivateKey1.SubKey(tx3.Nonce() + 1)
	newSubPub := newSubkey.PublicKey()

	tx4 := types.NextTransaction(tx3, common.BytesToHash(newSubPub.Address()), types.NORMAL_TX, common.Hash{0x0}, big.NewInt(0x1024), big.NewInt(0x1024), 1, common.Big1, []byte("sendTx"), []byte("privateValueX"), []byte("abcdef"))
	txs := []*types.Transaction{tx1, tx2, tx3, tx4}

	block := types.NewBlock(&types.Header{Type: chainType, ShardId: shard, Number: big.NewInt(314)}, txs, nil, nil)

	// Check that no transactions entries are in a pristine database
	for i, tx := range txs {
		if txn, _, _, _ := ReadTransaction(db, uint64(chainType), uint64(shard), tx.Hash()); txn != nil {
			t.Fatalf("tx #%d [%x]: non existent transaction returned: %v", i, tx.Hash(), txn)
		}
	}
	///
	//rplData, _ := rlp.EncodeToBytes(header)
	data, err := json.Marshal(block.Body())
	if err != nil {
	}
	t.Log(string(data))

	//rplData, _ := rlp.EncodeToBytes(block)

	// Insert all the transactions into the database, and verify contents
	WriteBlock(db, block)
	WriteTxLookupEntries(db, block)

	for i, tx := range txs {
		if txn, hash, number, index := ReadTransaction(db, uint64(chainType), uint64(shard), tx.Hash()); txn == nil {
			t.Fatalf("tx #%d [%x]: transaction not found", i, tx.Hash())
		} else {
			if hash != block.Hash() || number != block.NumberU64() || index != uint64(i) {
				t.Fatalf("tx #%d [%x]: positional metadata mismatch: have %x/%d/%d, want %x/%v/%v", i, tx.Hash(), hash, number, index, block.Hash(), block.NumberU64(), i)
			}
			if tx.Hash() != txn.Hash() {
				t.Fatalf("tx #%d [%x]: transaction mismatch: have %v, want %v", i, tx.Hash(), txn, tx)
			}
		}
	}
	// Delete the transactions and check purge
	for i, tx := range txs {
		DeleteTxLookupEntry(db, uint64(chainType), uint64(shard), tx.Hash())
		if txn, _, _, _ := ReadTransaction(db, uint64(chainType), uint64(shard), tx.Hash()); txn != nil {
			t.Fatalf("tx #%d [%x]: deleted transaction returned: %v", i, tx.Hash(), txn)
		}
	}

	//// DAG Transaction Test
	for i, tx := range txs {
		if txn := ReadRawTransaction(db, tx.Hash()); txn != nil {
			t.Fatalf("tx #%d [%x]: non existent transaction returned: %v", i, tx.Hash(), txn)
		}
	}

	WriteTransactionEntries(db, block)
	WriteAccountEntries(db, block)
	for _, tx := range txs {
		//WriteTransaction(db, tx)
		WriteHeadAccount(db, tx.Account(), tx)
	}
	WriteHeadAccount(db, tx4.Account(), tx4)

	for i, tx := range txs {
		txn := ReadRawTransaction(db, tx.Hash())
		if txn == nil {
			t.Fatalf("tx #%d [%x]: non existent transaction returned: %v", i, tx.Hash(), txn)
		} else {
			txData, _ := json.Marshal(txn)
			t.Log("Trans", tx.Hash().Hex(), string(txData))
		}
	}
	for i, tx := range txs {
		txhash := ReadAccountNonce(db, tx.Account(), tx.Nonce())
		if txhash == (common.Hash{}) {
			t.Fatalf("tx #%d [%x]: non existent transaction returned: %v", i, tx.Account(), txhash)
		} else {
			t.Log("Account", tx.Account().Hex(), "at", tx.Nonce(), "tx hash", hexutil.Encode(txhash.Bytes()))
			txn := ReadRawTransaction(db, txhash)
			if txn == nil {
				t.Fatalf("tx #%d [%x]: non existent transaction returned: %v", i, txhash, txn)
			} else {
				txData, _ := json.Marshal(txn)
				t.Log("Account", tx.Account().Hex(), "at", tx.Nonce(), "tx hash", hexutil.Encode(txhash.Bytes()), "data:", string(txData))
			}
		}
	}

	for i, tx := range txs {
		txn := ReadHeadAccount(db, tx.Account())
		if txn == nil {
			t.Fatalf("tx #%d [%x]: non existent transaction returned: %v", i, tx.Account(), txn)
		} else {
			txData, _ := json.Marshal(txn)
			t.Log("Account", tx.Account().Hex(), " last tx: ", string(txData))
		}
	}
}
