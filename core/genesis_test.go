// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package core

import (
	"crypto/ecdsa"
	"crypto/rand"
	"encoding/json"
	"math/big"
	"reflect"
	"testing"

	"github.com/davecgh/go-spew/spew"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/consensus/ethash"
	"github.com/ethereum/go-ethereum/core/rawdb"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/core/types/devotedb"
	"github.com/ethereum/go-ethereum/core/vm"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethdb"
	"github.com/ethereum/go-ethereum/params"
)

/*
	signers := core.masternodeMainAddress()
	header.ExtraData = make([]byte, 32+len(signers)*common.AddressLength+65)
	for i, signer := range signers {
		copy(header.ExtraData[32+i*common.AddressLength:], signer[:])
	}
*/
func TestDefaultGenesisBlock(t *testing.T) {
	var (
		chainType = uint64(types.BLOCK_HOME)
		shard     = uint64(0x1)
	)
	topBlock := DefaultGenesisBlock().TopBlock(nil)
	block := DefaultGenesisBlock().ToBlock(nil, chainType, shard, topBlock)
	if block.Hash() != params.MainnetGenesisHash {
		t.Errorf("wrong mainnet genesis hash, got %v, want %v", block.Hash(), params.MainnetGenesisHash)
	}
	topBlock = DefaultTestnetGenesisBlock().TopBlock(nil)
	block = DefaultTestnetGenesisBlock().ToBlock(nil, chainType, shard, topBlock)
	if block.Hash() != params.TestnetGenesisHash {
		t.Errorf("wrong testnet genesis hash, got %v, want %v", block.Hash(), params.TestnetGenesisHash)
	}
}

func TestSetupGenesis(t *testing.T) {
	var (
		chainType   = uint64(0x1)
		shard       = uint64(0x1)
		customghash = common.HexToHash("0x89c99d90b79719238d2645c7642f2c9295246e80775b38cfd162b696817fbd50")
		customg     = Genesis{
			Config: &params.ChainConfig{ChainID: big.NewInt(1) /*HomesteadBlock: big.NewInt(3)*/},
			Alloc: GenesisAlloc{
				{1}: {Balance: big.NewInt(1), Storage: map[common.Hash]common.Hash{{1}: {1}}},
			},
		}
		oldcustomg = customg
	)
	oldcustomg.Config = &params.ChainConfig{ /*HomesteadBlock: big.NewInt(2)*/ }
	tests := []struct {
		name       string
		fn         func(ethdb.Database) (*params.ChainConfig, common.Hash, *types.Block, error)
		wantConfig *params.ChainConfig
		wantHash   common.Hash
		wantErr    error
	}{
		{
			name: "genesis without ChainConfig",
			fn: func(db ethdb.Database) (*params.ChainConfig, common.Hash, *types.Block, error) {
				return SetupGenesisBlock(db, new(Genesis))
			},
			wantErr:    errGenesisNoConfig,
			wantConfig: params.AllEthashProtocolChanges,
		},
		{
			name: "no block in DB, genesis == nil",
			fn: func(db ethdb.Database) (*params.ChainConfig, common.Hash, *types.Block, error) {
				return SetupGenesisBlock(db, nil)
			},
			wantHash:   params.MainnetGenesisHash,
			wantConfig: params.MainnetChainConfig,
		},
		{
			name: "mainnet block in DB, genesis == nil",
			fn: func(db ethdb.Database) (*params.ChainConfig, common.Hash, *types.Block, error) {
				DefaultGenesisBlock().MustCommit(db)
				return SetupGenesisBlock(db, nil)
			},
			wantHash:   params.MainnetGenesisHash,
			wantConfig: params.MainnetChainConfig,
		},
		{
			name: "custom block in DB, genesis == nil",
			fn: func(db ethdb.Database) (*params.ChainConfig, common.Hash, *types.Block, error) {
				customg.MustCommit(db)
				return SetupGenesisBlock(db, nil)
			},
			wantHash:   customghash,
			wantConfig: customg.Config,
		},
		{
			name: "custom block in DB, genesis == testnet",
			fn: func(db ethdb.Database) (*params.ChainConfig, common.Hash, *types.Block, error) {
				customg.MustCommit(db)
				return SetupGenesisBlock(db, DefaultTestnetGenesisBlock())
			},
			wantErr:    &GenesisMismatchError{Stored: customghash, New: params.TestnetGenesisHash},
			wantHash:   params.TestnetGenesisHash,
			wantConfig: params.TestnetChainConfig,
		},
		{
			name: "compatible config in DB",
			fn: func(db ethdb.Database) (*params.ChainConfig, common.Hash, *types.Block, error) {
				oldcustomg.MustCommit(db)
				return SetupGenesisBlock(db, &customg)
			},
			wantHash:   customghash,
			wantConfig: customg.Config,
		},
		{
			name: "incompatible config in DB",
			fn: func(db ethdb.Database) (*params.ChainConfig, common.Hash, *types.Block, error) {
				// Commit the 'old' genesis block with Homestead transition at #2.
				// Advance to block #4, past the homestead transition block of customg.
				genesis := oldcustomg.MustCommit(db)
				if genesis != nil {
					bc, _ := NewBlockChain(db, chainType, shard, nil, oldcustomg.Config, ethash.NewFullFaker(), vm.Config{})
					defer bc.Stop()

					blocks, _ := GenerateChain(chainType, shard, oldcustomg.Config, genesis, ethash.NewFaker(), db, 4, nil)
					bc.InsertChain(blocks)
					bc.CurrentBlock()
				}
				// This should return a compatibility error.
				return SetupGenesisBlock(db, &customg)

			},
			wantHash:   customghash,
			wantConfig: customg.Config,
			wantErr: &params.ConfigCompatError{
				What:         "Homestead fork block",
				StoredConfig: big.NewInt(2),
				NewConfig:    big.NewInt(3),
				RewindTo:     1,
			},
		},
	}

	for _, test := range tests {
		db := ethdb.NewMemDatabase()
		config, hash, block, err := test.fn(db)
		if block == nil {

		}
		// Check the return values.
		if !reflect.DeepEqual(err, test.wantErr) {
			spew := spew.ConfigState{DisablePointerAddresses: true, DisableCapacities: true}
			t.Errorf("%s: returned error %#v, want %#v", test.name, spew.NewFormatter(err), spew.NewFormatter(test.wantErr))
		}
		if !reflect.DeepEqual(config, test.wantConfig) {
			t.Errorf("%s:\nreturned %v\nwant     %v", test.name, config, test.wantConfig)
		}
		if hash != test.wantHash {
			t.Errorf("%s: returned hash %s, want %s", test.name, hash.Hex(), test.wantHash.Hex())
		} else if err == nil {
			// Check database content.
			stored := rawdb.ReadBlock(db, test.wantHash, 0)
			if stored.Hash() != test.wantHash {
				t.Errorf("%s: block in DB has hash %s, want %s", test.name, stored.Hash(), test.wantHash)
			}
		}
	}
}

// Tests block storage and retrieval operations.
func TestGenesisBlockStorage(t *testing.T) {
	db := ethdb.NewMemDatabase()

	// Create a test block to move around the database and make sure it's really new
	tx1 := types.NewTransaction(common.BytesToAddress([]byte{0x1, 0x11}), common.Hash{0x0}, types.NORMAL_TX, common.Hash{0x0}, big.NewInt(111), 1111, big.NewInt(11111), nil, []byte{0x11, 0x11, 0x11})
	tx2 := types.NewTransaction(common.BytesToAddress([]byte{0x2, 0x11}), common.Hash{0x0}, types.NORMAL_TX, common.Hash{0x0}, big.NewInt(222), 2222, big.NewInt(22222), nil, []byte{0x22, 0x22, 0x22})
	tx3 := types.NewTransaction(common.BytesToAddress([]byte{0x3, 0x11}), common.Hash{0x0}, types.NORMAL_TX, common.Hash{0x0}, big.NewInt(333), 3333, big.NewInt(33333), nil, []byte{0x33, 0x33, 0x33})
	//	txs := []*types.Transaction{tx1, tx2, tx3}

	//	tx1 := types.NewGenesisTransaction(common.BytesToAddress([]byte{0x1, 0x11}), big.NewInt(111), 1, big.NewInt(1))
	//	tx2 := types.NewGenesisTransaction(common.BytesToAddress([]byte{0x2, 0x11}), big.NewInt(222), 1, big.NewInt(1))
	//	tx3 := types.NewGenesisTransaction(common.BytesToAddress([]byte{0x3, 0x11}), big.NewInt(333), 1, big.NewInt(1))
	txs := []*types.Transaction{tx1, tx2, tx3}

	header := &types.Header{
		Type:        0x0,
		ShardId:     0x0,
		Extra:       []byte("test block"),
		UncleHash:   types.EmptyUncleHash,
		TxHash:      types.EmptyRootHash,
		ReceiptHash: types.EmptyRootHash,
		Protocol:    &devotedb.DevoteProtocol{CycleHash: common.Hash{}, StatsHash: common.Hash{}},
	}
	t.Log("Creating topGenesisBlock")
	topGenesisBlock := types.NewBlock(header, txs, nil, nil)
	t.Log("Reading Top Header")
	topHeader := rawdb.ReadTopHeader(db)
	if topHeader != nil {
		t.Fatalf("Error Top header is exist %v", topHeader)
		//t.Log("Error Top header is exist")
	}
	var headJson []byte
	var bodyJson []byte
	var blockJson []byte
	t.Log("Creating json objects")
	headJson, _ = topGenesisBlock.Header().MarshalJSON()
	bodyJson, _ = json.Marshal(topGenesisBlock.Body())
	blockJson, _ = json.Marshal(topGenesisBlock)

	t.Logf("Write Top header %s\nBody:\n%s\nBlock:\n%s", string(headJson), string(bodyJson), string(blockJson))
	rawdb.WriteTopBlock(db, topGenesisBlock)
	topHeader = rawdb.ReadTopHeader(db)
	if topHeader == nil {
		//t.Fatalf("Error Top header is exist %v", topHeader)
		t.Log("Error Top header is not exist")
	} else {
		headJson, _ = topHeader.MarshalJSON()
		//t.Logf("Top header is exist %s", string(headJson))
	}
	topBlock := rawdb.ReadTopBlock(db)
	if topBlock == nil {
		t.Fatalf("Error read top block")
	}
	headJson, _ = topBlock.Header().MarshalJSON()
	t.Logf("Genesis header is %s", string(headJson))

	maxShard := 5
	for i := 0; i <= maxShard; i++ {
		t.Log("Create genesis shard", i)

		homeBlock := NewGenesisBlock(topBlock, uint64(types.BLOCK_HOME), uint64(i))
		foreignBlock := NewGenesisBlock(topBlock, uint64(types.BLOCK_FOREIGN), uint64(i))

		headJson, _ = homeBlock.Header().MarshalJSON()
		bodyJson, _ = json.Marshal(homeBlock.Body())
		blockJson, _ = json.Marshal(homeBlock)
		t.Logf("\n\n\nShard %x home header %s\nBody:\n%s\nBlock:\n%s", i, string(headJson), string(bodyJson), string(blockJson))

		headJson, _ = foreignBlock.Header().MarshalJSON()
		bodyJson, _ = json.Marshal(foreignBlock.Body())
		blockJson, _ = json.Marshal(foreignBlock)
		t.Logf("\n\nShard %x foreign header %s\nBody:\n%s\nBlock:\n%s", i, string(headJson), string(bodyJson), string(blockJson))
		rawdb.WriteBlock(db, homeBlock)
		rawdb.WriteBlock(db, foreignBlock)
		rawdb.WriteCanonicalHash(db, uint64(homeBlock.Type()), uint64(homeBlock.ShardId()), homeBlock.NumberU64(), homeBlock.Header().Hash())
		rawdb.WriteCanonicalHash(db, uint64(foreignBlock.Type()), uint64(foreignBlock.ShardId()), foreignBlock.NumberU64(), foreignBlock.Header().Hash())
	}
	for i := 0; i <= maxShard; i++ {
		hash := rawdb.ReadCanonicalHash(db, uint64(types.BLOCK_HOME), uint64(i), 0)
		t.Log("Read genesis shard", i, "hash", hexutil.Encode(hash.Bytes()))
		homeBlock := rawdb.ReadBlock(db, hash, 0)

		headJson, _ = homeBlock.Header().MarshalJSON()
		bodyJson, _ = json.Marshal(homeBlock.Body())
		blockJson, _ = json.Marshal(homeBlock)
		t.Logf("\n\n\nRead Shard %x home header %s\nBody:\n%s\nBlock:\n%s\nTransactions:%d", i, string(headJson), string(bodyJson), string(blockJson), len(homeBlock.Transactions()))

	}
	/*
		if entry := ReadBlock(db, block.Hash(), block.NumberU64()); entry != nil {
			t.Fatalf("Non existent block returned: %v", entry)
		}
		if entry := ReadHeader(db, block.Hash(), block.NumberU64()); entry != nil {
			t.Fatalf("Non existent header returned: %v", entry)
		}
		if entry := ReadBody(db, block.Hash(), block.NumberU64()); entry != nil {
			t.Fatalf("Non existent body returned: %v", entry)
		}
		// Write and verify the block in the database
		WriteBlock(db, block)
		if entry := ReadBlock(db, block.Hash(), block.NumberU64()); entry == nil {
			t.Fatalf("Stored block not found")
		} else if entry.Hash() != block.Hash() {
			t.Fatalf("Retrieved block mismatch: have %v, want %v", entry, block)
		}
		if entry := ReadHeader(db, block.Hash(), block.NumberU64()); entry == nil {
			t.Fatalf("Stored header not found")
		} else if entry.Hash() != block.Header().Hash() {
			t.Fatalf("Retrieved header mismatch: have %v, want %v", entry, block.Header())
		}
		if entry := ReadBody(db, block.Hash(), block.NumberU64()); entry == nil {
			t.Fatalf("Stored body not found")
		} else if types.DeriveSha(types.Transactions(entry.Transactions)) != types.DeriveSha(block.Transactions()) || types.CalcUncleHash(entry.Uncles) != types.CalcUncleHash(block.Uncles()) {
			t.Fatalf("Retrieved body mismatch: have %v, want %v", entry, block.Body())
		}
		// Delete the block and verify the execution
		DeleteBlock(db, block.Hash(), block.NumberU64())
		if entry := ReadBlock(db, block.Hash(), block.NumberU64()); entry != nil {
			t.Fatalf("Deleted block returned: %v", entry)
		}
		if entry := ReadHeader(db, block.Hash(), block.NumberU64()); entry != nil {
			t.Fatalf("Deleted header returned: %v", entry)
		}
		if entry := ReadBody(db, block.Hash(), block.NumberU64()); entry != nil {
			t.Fatalf("Deleted body returned: %v", entry)
		}
	*/
}

func TestSetupGenesisBlock(t *testing.T) {
	db := ethdb.NewMemDatabase()

	masterKey, address, err := crypto.GenerateMasterKey(254)
	//masterKey, err := crypto.HexToECDSA("455f249b823adeb2d7715a7608c8a34415767429c76fe98e17612f948c6b482b")
	if err != nil {
		t.Fatalf("could not generate key: %v", err)
	}
	signKey := crypto.GenerateSignKey(masterKey)
	signKey2 := crypto.GenerateViewKey(masterKey)

	strKey1 := hexutil.Encode(crypto.FromECDSA(signKey))
	strKey2 := hexutil.Encode(crypto.FromECDSA(signKey2))

	data := []byte("Test signed")
	r, s, err := ecdsa.Sign(rand.Reader, signKey2, data)
	if result := ecdsa.Verify(&signKey2.PublicKey, data, r, s); !result {
		t.Log("Verify error:", strKey1, strKey2)
	} else {
		t.Log("Verify OK:", strKey1, strKey2)
	}

	t.Log("MasterKey:", hexutil.Encode(crypto.FromECDSA(masterKey)))
	t.Log("SignKey:", address, strKey1, strKey2)
	//keyTmp, err := crypto.HexToECDSA(strKey1)
	//keyTmp, err := crypto.HexToECDSA("51b762ea0ecb266528e37099e1fbb5be9a52655c352127bdfd14bafe9eb6aa30")
	addr1 := crypto.PubkeyToAddress(signKey.PublicKey)
	t.Log("Address shard: ", hexutil.Encode(addr1.Bytes()), addr1.Shard(), crypto.VerifyAddress(addr1), addr1.Id())
	addr2 := common.Address{0x01, 0x01}
	t.Log("Address2 shard: ", hexutil.Encode(addr2.Bytes()), addr2.Shard(), addr2.Id())
	t.Logf("Address2 %x: %x", addr2.Id(), addr1.Id())
	bInt := new(big.Int).SetUint64(0x1a65)
	bIntHash := common.BigToHash(bInt)
	//bIntHash2 := common.BytesToHash(bInt.Bytes())
	bIntHash2 := common.Hash{}
	copy(bIntHash2[:8], bInt.Bytes())
	t.Log("HASH ", hexutil.Encode(bIntHash.Bytes()), hexutil.Encode(bIntHash2.Bytes()))
	contractAddr1 := crypto.CreateAddress(addr1, 1)
	contractAddr2 := crypto.CreateAddress(addr2, 1)
	t.Log("CONTRACT ", hexutil.Encode(contractAddr1.Bytes()), crypto.VerifyAddress(contractAddr1),
		hexutil.Encode(contractAddr2.Bytes()), crypto.VerifyAddress(contractAddr2))

	customGenesis := Genesis{
		Config: &params.ChainConfig{
			ChainID: big.NewInt(1), /*HomesteadBlock: big.NewInt(3)*/
			Devote: &params.DevoteConfig{
				Witnesses: []string{
					"81e4a6821a4d7117",
					"754ae5a877ff913e",
				},
			},
		},
		Alloc: GenesisAlloc{
			{1}: {Balance: big.NewInt(1), Storage: map[common.Hash]common.Hash{{1}: {1}}},
		},
	}
	var jsonConfig []byte
	config, hash, block, err := SetupGenesisBlock(db, &customGenesis)
	jsonConfig, _ = json.Marshal(customGenesis)
	t.Log("Old Config ", string(jsonConfig), "hash", hexutil.Encode(hash.Bytes()), "err", err)
	jsonConfig, _ = json.Marshal(config)
	t.Log("New Config ", string(jsonConfig), "hash", hexutil.Encode(hash.Bytes()), "err", err)
	t.Logf("Config json %s", string(jsonConfig))
	for i := 0; i < 5; i++ {
		t.Log("Create shard ", i)
		SetupGenesisShardBlock(db, types.BLOCK_HOME, uint64(i), block, nil, config)
	}
	//	topBlock := DefaultGenesisBlock().TopBlock(nil)
	//	block := DefaultGenesisBlock().ToBlock(nil, chainType, shard, topBlock)
	//	if block.Hash() != params.MainnetGenesisHash {
	//		t.Errorf("wrong mainnet genesis hash, got %v, want %v", block.Hash(), params.MainnetGenesisHash)
	//	}
	//	topBlock = DefaultTestnetGenesisBlock().TopBlock(nil)
	//	block = DefaultTestnetGenesisBlock().ToBlock(nil, chainType, shard, topBlock)
	//	if block.Hash() != params.TestnetGenesisHash {
	//		t.Errorf("wrong testnet genesis hash, got %v, want %v", block.Hash(), params.TestnetGenesisHash)
	//	}
}
