// Package eth implements the Ethereum protocol.
package eth

import (
	//	"errors"
	//	"fmt"
	//	"math/big"
	//	"runtime"
	"sync"
	//"sync/atomic"
	//	"time"

	//	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/common"
	//	"github.com/ethereum/go-ethereum/common/hexutil"
	//"github.com/ethereum/go-ethereum/consensus"
	//	"github.com/ethereum/go-ethereum/consensus/clique"
	//	"github.com/ethereum/go-ethereum/consensus/devote"
	//	"github.com/ethereum/go-ethereum/consensus/ethash"
	//	"github.com/ethereum/go-ethereum/contracts/masternode/contract"
	"github.com/ethereum/go-ethereum/core"
	//	"github.com/ethereum/go-ethereum/core/bloombits"
	//	"github.com/ethereum/go-ethereum/core/rawdb"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/core/types/devotedb"
	//	"github.com/ethereum/go-ethereum/core/vm"
	//	"github.com/ethereum/go-ethereum/eth/downloader"
	//	"github.com/ethereum/go-ethereum/eth/filters"
	//	"github.com/ethereum/go-ethereum/eth/gasprice"
		"github.com/ethereum/go-ethereum/ethdb"
	//	"github.com/ethereum/go-ethereum/event"
	//	"github.com/ethereum/go-ethereum/internal/ethapi"
	"github.com/ethereum/go-ethereum/log"
	//"github.com/ethereum/go-ethereum/miner"
	//	"github.com/ethereum/go-ethereum/node"
	//	"github.com/ethereum/go-ethereum/p2p"
	//"github.com/ethereum/go-ethereum/params"
	//"github.com/ethereum/go-ethereum/rlp"
	//"github.com/ethereum/go-ethereum/rpc"
)


// Shard is a part of Kim sharding
// Kim system have multi shard, every shard consist home-subchain and foreign-subchain
// Home-subchain store send/lock/unlock transactions of an account.
// Foreign-subchain store any transactions which send to the account. It also store state of contract account (data trie only)
// Core shard: 254 shard
type Shard struct {
	shard        uint64
	homeChain    *SubChain
	foreignChain *SubChain
	devoteDB     *devotedb.DevoteDB // Same for subchains?
	lock         sync.RWMutex       // Protects the variadic fields (e.g. gas price and etherbase)
}

// NewShard create new shard with id
func NewShard(db ethdb.Database, id uint64) *Shard {
	devoteDB, err := devotedb.NewDevote(devotedb.NewDatabase(db), common.Hash{byte(id)}, common.Hash{byte(id)})
	if err != nil {
		log.Info("Create new shard error", id, err)
		return nil
	}
	return &Shard{
		shard: id,
		devoteDB:devoteDB,
	}
}
// ShardID return id of the shard
func (shard *Shard) ShardID() uint64 {
	return shard.shard
}

// HomeChain return home sub chain of a shard
func (shard *Shard) HomeChain() *SubChain {
	return shard.homeChain
}

// HomeBlockChain return home chain of a shard
func (shard *Shard) HomeBlockChain() *core.BlockChain {
	if shard.homeChain != nil {
		return shard.homeChain.BlockChain()
	}
	return nil
}

// ForeignChain return foreign sub chain of a shard
func (shard *Shard) ForeignChain() *SubChain {
	return shard.foreignChain
}

// ForeignBlockChain return foreign chain of a shard
func (shard *Shard) ForeignBlockChain() *core.BlockChain {
	if shard.foreignChain != nil {
		return shard.foreignChain.BlockChain()
	}
	return nil
}

// DevoteDB return devoteDB of a shard
func (shard *Shard) DevoteDB() *devotedb.DevoteDB {
	shard.lock.Lock()
	defer shard.lock.Unlock()
	return shard.devoteDB
}

// AddRemote a transaction to shard queues
func (shard *Shard) AddRemote(tx *types.Transaction) {
	//return shard.shard
}

// Start a shard
func (shard *Shard) Start() {	
	if shard.homeChain != nil {
		shard.homeChain.Start()
	}
	if shard.foreignChain != nil {
		shard.foreignChain.Start()
	}
	log.Info("Stard", "shard", shard.shard)
}

// Stop shard
func (shard *Shard) Stop() {
	if shard.homeChain != nil {
		shard.homeChain.Stop()
	}
	if shard.foreignChain != nil {
		shard.foreignChain.Stop()
	}
	log.Info("Stop", " shard", shard.shard)
}
