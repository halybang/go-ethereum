package ethash

import (
	//	"bytes"
	//	"errors"
	//	"fmt"
	//	"math/big"
	//	"runtime"
	//	"time"

	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/log"
	"github.com/ethereum/go-ethereum/rlp"
	"golang.org/x/crypto/sha3"
	//"github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/consensus"
	//"github.com/ethereum/go-ethereum/consensus/misc"
	//"github.com/ethereum/go-ethereum/core/state"
	"github.com/ethereum/go-ethereum/core/types"
	//"github.com/ethereum/go-ethereum/params"
	//set "gopkg.in/fatih/set.v0"
)

// INFO: copied from consensus/clique/clique.go
type SignerFn func(accounts.Account, []byte) ([]byte, error)

// INFO: copied from consensus/clique/clique.go
func sigHash(header *types.Header) (hash common.Hash) {
	hasher := sha3.NewLegacyKeccak256()

	rlp.Encode(hasher, []interface{}{
		header.ParentHash,
		header.UncleHash,
		header.Coinbase,
		header.Root,
		header.TxHash,
		header.ReceiptHash,
		header.Bloom,
		header.Difficulty,
		header.Number,
		header.GasLimit,
		header.GasUsed,
		header.Time,
		header.Extra[:len(header.Extra)-extraSeal], // Yes, this will panic if extra is too short
		header.MixDigest,
		header.Nonce,
		header.Protocol.Root(),
	})
	hasher.Sum(hash[:0])
	return hash
}

// INFO: copied from consensus/clique/clique.go
// ecrecover extracts the Ethereum account address from a signed header.
func ecrecover(header *types.Header) (common.Address, error) {
	// Retrieve the signature from the header extra-data
	if len(header.Extra) < extraSeal {
		return common.Address{}, errMissingSignature
	}
	signature := header.Extra[len(header.Extra)-extraSeal:]

	// Recover the public key and the Ethereum address
	//log.Trace(fmt.Sprintf(header.String()))

	pubkey, err := crypto.Ecrecover(sigHash(header).Bytes(), signature)
	// log.Trace("ecrecover(): Seal hash", "Input hash", sigHash(header).String())
	if err != nil {
		return common.Address{}, err
	}
	var signer common.Address
	copy(signer[:], crypto.Keccak256(pubkey[1:])[12:])

	// The signer's address should match the coinbase address in Wanchain Release 1, shall we add a warning if it's not?
	// log.Trace("ecrecover()", "recovered signer", signer)

	return signer, nil
}

func (self *Ethash) Authorize(signer common.Address, signFn SignerFn) {
	self.lock.Lock()
	defer self.lock.Unlock()

	self.signFn = signFn
	self.signer = signer
}

func (ethash *Ethash) VerifyPPOWReorg(chain consensus.ChainReader, commonBlock *types.Block, oldChain []*types.Block, newChain []*types.Block) error {
	s, err := ethash.snapshot(chain, commonBlock.NumberU64(), commonBlock.Hash(), nil)
	if err != nil {
		return err
	}
	oldSignerSet := make(map[common.Address]struct{})
	newSignerSet := make(map[common.Address]struct{})
	for signer := range s.UsedSigners {
		oldSignerSet[signer] = struct{}{}
		newSignerSet[signer] = struct{}{}
	}

	for _, b := range oldChain {
		//using coinbase here, previously checked signature by node or verify headers
		oldSignerSet[b.Coinbase()] = struct{}{}
	}

	for _, nb := range newChain {
		newSignerSet[nb.Coinbase()] = struct{}{}
	}

	//TODO:PPOW how to make a reasonable choice
	if len(newSignerSet) < (len(oldSignerSet) - 1) {
		return errUsedSignerDescend
	}

	return nil
}

// verify does current coinbase valid for sign
func (self *Ethash) verifySignerIdentity(chain consensus.ChainReader, header *types.Header, parents []*types.Header) error {
	number := header.Number.Uint64()

	s, err := self.snapshot(chain, number-1, header.ParentHash, parents)
	if err != nil {
		return err
	}

	if err = s.isLegal4Sign(header.Coinbase); err != nil {
		return err
	}

	return nil
}

// INFO: copied from consensus/clique/clique.go , mostly
// snapshot retrieves the signer status at a given point
func (self *Ethash) snapshot(chain consensus.ChainReader, number uint64, hash common.Hash, parents []*types.Header) (*Snapshot, error) {
	var (
		headers []*types.Header
		snap    *Snapshot
	)

	for snap == nil {
		if s, ok := self.recents.Get(hash); ok {
			snap = s.(*Snapshot)
			break
		}

		if number%checkpointInterval == 0 {
			if s, err := loadSnapShot(self.db, hash); err == nil {
				snap = s
				break
			}
		}

		if number == 0 {
			genesis := chain.GetHeaderByNumber(0)
			// verify genesis hash match
			if err := self.VerifyHeader(chain, genesis, false); err != nil {
				return nil, err
			}

			signers := make([]common.Address, len(genesis.Extra)/common.AddressLength)
			for i := 0; i < len(signers); i++ {
				copy(signers[i][:], genesis.Extra[i*common.AddressLength:])
			}
			snap = newSnapshot(0, genesis.Hash(), signers)
			if err := snap.store(self.db); err != nil {
				return nil, err
			}
			break
		}

		var header *types.Header
		if len(parents) > 0 {
			header = parents[len(parents)-1]
			if header.Hash() != hash || header.Number.Uint64() != number {
				return nil, consensus.ErrUnknownAncestor
			}
			parents = parents[:len(parents)-1]
		} else {
			header = chain.GetHeader(hash, number)
			if header == nil {
				return nil, consensus.ErrUnknownAncestor
			}
		}
		headers = append(headers, header)
		number, hash = number-1, header.ParentHash
	}

	for i := 0; i < len(headers)/2; i++ {
		headers[i], headers[len(headers)-1-i] = headers[len(headers)-1-i], headers[i]
	}

	snap, err := snap.apply(headers)
	if err != nil {
		return nil, err
	}

	self.recents.Add(snap.Hash, snap)

	if snap.Number%checkpointInterval == 0 && len(headers) > 0 {
		if err = snap.store(self.db); err != nil {
			return nil, err
		}
		log.Trace("Stored ppow snapshot to disk", "number", snap.Number, "hash", snap.Hash)
	}

	return snap, err
}
