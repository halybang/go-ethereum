// Copyright 2015 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package core

import (
	"fmt"
	//"math/big"
	"crypto/ecdsa"

	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/consensus"
	"github.com/ethereum/go-ethereum/consensus/ethash"
	"github.com/ethereum/go-ethereum/crypto"
	//"github.com/ethereum/go-ethereum/consensus/misc"
	"github.com/ethereum/go-ethereum/core/state"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/core/vm"
	"github.com/ethereum/go-ethereum/ethdb"
	"github.com/ethereum/go-ethereum/params"
)

////////////////////////////////////
// TODO: WAN Changed

func fakeSignerFn(signer accounts.Account, hash []byte) ([]byte, error) {
	return crypto.Sign(hash, fakedAccountPrivateKey)
}

type ChainEnv struct {
	config       *params.ChainConfig
	genesis      *Genesis
	engine       *ethash.Ethash
	blockChain   *BlockChain
	db           ethdb.Database
	mapSigners   map[common.Address]struct{}
	arraySigners []common.Address
	signerKeys   []*ecdsa.PrivateKey
	//set signers here for testing convenience
	// validSigners
}

// add for testing permission proof of work
var (
	totalSigner = 20
	signerSet   = make(map[common.Address]*ecdsa.PrivateKey)
	addrSigners = make([]common.Address, 0)

	fakedAddr                 = common.HexToAddress("0xf9b32578b4420a36f132db32b56f3831a7cc1804")
	fakedAccountPrivateKey, _ = crypto.HexToECDSA("f1572f76b75b40a7da72d6f2ee7fda3d1189c2d28f0a2f096347055abe344d7f")
	extraVanity               = 32
	extraSeal                 = 65
)

func init() {
	for i := 0; i < totalSigner; i++ {
		private, _ := crypto.GenerateKey()
		addr := crypto.PubkeyToAddress(private.PublicKey)
		signerSet[addr] = private
		addrSigners = append(addrSigners, addr)
	}
}

func NewChainEnv(config *params.ChainConfig, g *Genesis, engine consensus.Engine, bc *BlockChain, db ethdb.Database) *ChainEnv {
	ce := &ChainEnv{
		config:       config,
		genesis:      g,
		engine:       engine.(*ethash.Ethash),
		blockChain:   bc,
		db:           db,
		mapSigners:   make(map[common.Address]struct{}),
		arraySigners: make([]common.Address, len(g.ExtraData)/common.AddressLength),
	}

	for i := 0; i < len(ce.arraySigners); i++ {
		copy(ce.arraySigners[i][:], g.ExtraData[i*common.AddressLength:])
	}
	for _, s := range ce.arraySigners {
		ce.mapSigners[s] = struct{}{}
	}

	return ce
}

func (b *BlockGen) AddTxAndCalcGasUsed(tx *types.Transaction) uint64 {
	if b.gasPool == nil {
		b.SetCoinbase(b.parent.Coinbase())
	}
	b.statedb.Prepare(tx.Hash(), common.Hash{}, len(b.txs))
	receipt, gasUsed, err := ApplyTransaction(b.config, nil, &b.header.Coinbase, b.gasPool, b.statedb, b.header, tx, &b.header.GasUsed, vm.Config{})
	if err != nil {
		panic(err)
	}
	b.txs = append(b.txs, tx)
	b.receipts = append(b.receipts, receipt)
	return gasUsed
}

func (self *ChainEnv) GenerateChain(parent *types.Block, n int, gen func(int, *BlockGen)) ([]*types.Block, []types.Receipts) {
	blocks, receipts := make(types.Blocks, n), make([]types.Receipts, n)
	genblock := func(i int, h *types.Header, statedb *state.StateDB) (*types.Block, types.Receipts) {
		b := &BlockGen{parent: parent, i: i, chain: blocks, header: h, statedb: statedb, config: self.config}

		// Execute any user modifications to the block and finalize it
		if gen != nil {
			gen(i, b)
		}

		//TODO: WAN Changed
		//ethash.AccumulateRewards(self.config, statedb, h, b.uncles)
		root, err := statedb.Commit(true)
		if err != nil {
			panic(fmt.Sprintf("state write error: %v", err))
		}
		h.Root = root

		self.engine.Authorize(fakedAddr, fakeSignerFn)
		h.Coinbase = fakedAddr
		rawBlock := types.NewBlock(h, b.txs, b.uncles, b.receipts)
		//sealBlock, _ := self.engine.Seal(self.blockChain, rawBlock, nil, nil)
		//return sealBlock, b.receipts
		self.engine.Seal(self.blockChain, rawBlock, nil, nil)
		return nil, nil
	}
	for i := 0; i < n; i++ {
		statedb, err := state.NewStateDB(parent.Root(), state.NewDatabase(self.db))
		if err != nil {
			panic(err)
		}
		header := makeHeader(self.Blockchain(), parent, statedb, self.engine)
		block, receipt := genblock(i, header, statedb)
		blocks[i] = block
		receipts[i] = receipt
		parent = block
	}
	return blocks, receipts
}

func (self *ChainEnv) GenerateChainMulti(parent *types.Block, n int, gen func(int, *BlockGen)) ([]*types.Block, []types.Receipts) {
	blocks, receipts := make(types.Blocks, n), make([]types.Receipts, n)
	genblock := func(i int, h *types.Header, statedb *state.StateDB) (*types.Block, types.Receipts) {
		b := &BlockGen{parent: parent, i: i, chain: blocks, header: h, statedb: statedb, config: self.config}

		// Execute any user modifications to the block and finalize it
		if gen != nil {
			gen(i, b)
		}

		//ethash.AccumulateRewards(self.config, statedb, h, b.uncles)
		root, err := statedb.Commit(true)
		if err != nil {
			panic(fmt.Sprintf("state write error: %v", err))
		}
		h.Root = root

		self.engine.Authorize(fakedAddr, fakeSignerFn)
		rawBlock := types.NewBlock(h, b.txs, b.uncles, b.receipts)
		//sealBlock, _ := self.engine.Seal(self.blockChain, rawBlock, nil, nil)
		//return sealBlock, b.receipts
		self.engine.Seal(self.blockChain, rawBlock, nil, nil)
		return nil, nil
	}
	for i := 0; i < n; i++ {
		statedb, err := state.NewStateDB(parent.Root(), state.NewDatabase(self.db))
		if err != nil {
			panic(err)
		}
		header := makeHeader(self.Blockchain(), parent, statedb, self.engine)
		block, receipt := genblock(i, header, statedb)
		blocks[i] = block
		receipts[i] = receipt
		parent = block
	}
	return blocks, receipts
}

func fakeSignerFnEx(signer accounts.Account, hash []byte) ([]byte, error) {
	return crypto.Sign(hash, signerSet[signer.Address])
}

func (self *ChainEnv) GenerateChainEx(parent *types.Block, signerSequence []int, gen func(int, *BlockGen)) ([]*types.Block, []types.Receipts) {
	blocks, receipts := make(types.Blocks, len(signerSequence)), make([]types.Receipts, len(signerSequence))
	genblock := func(i int, h *types.Header, statedb *state.StateDB) (*types.Block, types.Receipts) {
		b := &BlockGen{parent: parent, i: i, chain: blocks, header: h, statedb: statedb, config: self.config}

		// Execute any user modifications to the block and finalize it
		if gen != nil {
			gen(i, b)
		}

		//ethash.AccumulateRewards(self.config, statedb, h, b.uncles)
		root, err := statedb.Commit(true)
		if err != nil {
			panic(fmt.Sprintf("state write error: %v", err))
		}
		h.Root = root

		self.engine.Authorize(addrSigners[i], fakeSignerFnEx)
		//h.Coinbase.Set(addrSigners[i])
		h.Coinbase = addrSigners[i]
		rawBlock := types.NewBlock(h, b.txs, b.uncles, b.receipts)
		//sealBlock, _ := self.engine.Seal(self.blockChain, rawBlock, nil)
		//return sealBlock, b.receipts
		self.engine.Seal(self.blockChain, rawBlock, nil, nil)
		return nil, nil
	}
	for i := 0; i < len(signerSequence); i++ {
		statedb, err := state.NewStateDB(parent.Root(), state.NewDatabase(self.db))
		if err != nil {
			panic(err)
		}
		//header := makeHeader(self.config, parent, statedb)
		header := makeHeader(self.Blockchain(), parent, statedb, self.engine)
		block, receipt := genblock(signerSequence[i], header, statedb)
		blocks[i] = block
		receipts[i] = receipt
		parent = block
	}
	return blocks, receipts
}

// makeHeaderChain creates a deterministic chain of headers rooted at parent.
func (self *ChainEnv) makeHeaderChain(parent *types.Header, n int, seed int) []*types.Header {
	blocks := self.makeBlockChain(types.NewBlockWithHeader(parent), n, seed)
	headers := make([]*types.Header, len(blocks))
	for i, block := range blocks {
		headers[i] = block.Header()
	}
	return headers
}

// makeBlockChain creates a deterministic chain of blocks rooted at parent.
func (self *ChainEnv) makeBlockChain(parent *types.Block, n int, seed int) []*types.Block {
	// blocks, _ := self.GenerateChain(parent, n, func(i int, b *BlockGen) {
	// b.SetCoinbase(common.Address{0: byte(seed), 19: byte(i)})
	// })
	blocks, _ := self.GenerateChain(parent, n, nil)
	return blocks
}

func (self *ChainEnv) Blockchain() *BlockChain {
	return self.blockChain
}

func (self *ChainEnv) Database() ethdb.Database {
	return self.db
}

func (self *ChainEnv) Config() *params.ChainConfig {
	return self.config
}
