// Copyright 2014 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

// Package eth implements the Ethereum protocol.
package eth

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/big"
	"runtime"
	"sync"
	"sync/atomic"
	"time"

	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/consensus"
	"github.com/ethereum/go-ethereum/consensus/clique"
	"github.com/ethereum/go-ethereum/consensus/devote"
	"github.com/ethereum/go-ethereum/consensus/ethash"
	"github.com/ethereum/go-ethereum/core"
	//"github.com/ethereum/go-ethereum/contracts/masternode/contract"
	"github.com/ethereum/go-ethereum/core/bloombits"
	"github.com/ethereum/go-ethereum/core/rawdb"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/core/types/devotedb"
	//"github.com/ethereum/go-ethereum/core/vm"
	"github.com/ethereum/go-ethereum/eth/downloader"
	"github.com/ethereum/go-ethereum/eth/filters"
	"github.com/ethereum/go-ethereum/eth/gasprice"
	"github.com/ethereum/go-ethereum/ethdb"
	"github.com/ethereum/go-ethereum/event"
	"github.com/ethereum/go-ethereum/internal/ethapi"
	"github.com/ethereum/go-ethereum/log"
	"github.com/ethereum/go-ethereum/miner"
	"github.com/ethereum/go-ethereum/node"
	"github.com/ethereum/go-ethereum/p2p"
	"github.com/ethereum/go-ethereum/params"
	"github.com/ethereum/go-ethereum/rlp"
	"github.com/ethereum/go-ethereum/rpc"
	"github.com/hashicorp/golang-lru"
)

type LesServer interface {
	Start(srvr *p2p.Server)
	Stop()
	Protocols() []p2p.Protocol
	SetBloomBitsIndexer(bbIndexer *core.ChainIndexer)
}

// Ethereum implements the Ethereum full node service.
type Ethereum struct {
	ctx *node.ServiceContext
	config      *Config
	chainConfig *params.ChainConfig		// Config for core shards/subchains

	// Channel for shutting down the service
	shutdownChan chan bool // Channel for shutting down the Ethereum

	// Handlers
	txPool *core.TxPool

	protocolManager *ProtocolManager
	lesServer       LesServer

	// DB interfaces
	chainDb ethdb.Database // Block chain database

	eventMux *event.TypeMux	
	accountManager *accounts.Manager

	bloomRequests chan chan *bloombits.Retrieval // Channel receiving bloom data retrieval requests
	bloomIndexer  *core.ChainIndexer             // Bloom indexer operating during block imports

	APIBackend *EthAPIBackend

	gasPrice          *big.Int
	etherbase         common.Address
	witness           string
	masternodeManager *MasternodeManager

	networkID     uint64
	netRPCService *ethapi.PublicNetAPI

	shards       map[uint64]*Shard // Core shards
	customShards map[uint64]*Shard // Custome shards / private shard of contracts
	allShards map[uint64]*Shard		// Store all shards
	mineShard    *Shard

	genesisBlock *types.Block // Top block, mother of all child genesis block
	lock         sync.RWMutex // Protects the variadic fields (e.g. gas price and etherbase)

	// Cache
	headerCache *lru.Cache // Cache for the most recent block headers
}

func (s *Ethereum) AddLesServer(ls LesServer) {
	s.lesServer = ls
	ls.SetBloomBitsIndexer(s.bloomIndexer)
}

// New creates a new Ethereum object (including the
// initialisation of the common Ethereum object)
func NewEth(ctx *node.ServiceContext, ethConfig *Config) (*Ethereum, error) {
	// Ensure configuration values are compatible and sane
	if ethConfig.SyncMode == downloader.LightSync {
		return nil, errors.New("can't run eth.Ethereum in light sync mode, use les.LightEthereum")
	}
	if !ethConfig.SyncMode.IsValid() {
		return nil, fmt.Errorf("invalid sync mode %d", ethConfig.SyncMode)
	}
	if ethConfig.MinerGasPrice == nil || ethConfig.MinerGasPrice.Cmp(common.Big0) <= 0 {
		log.Warn("Sanitizing invalid miner gas price", "provided", ethConfig.MinerGasPrice, "updated", DefaultConfig.MinerGasPrice)
		ethConfig.MinerGasPrice = new(big.Int).Set(DefaultConfig.MinerGasPrice)
	}
	// Assemble the Ethereum object
	chainDb, err := CreateDB(ctx, ethConfig, "chaindata")
	if err != nil {
		return nil, err
	}
	chainConfig, genesisHash, topGenesisBlock, genesisErr := core.SetupGenesisBlock(chainDb, ethConfig.Genesis)
	if _, ok := genesisErr.(*params.ConfigCompatError); genesisErr != nil && !ok {
		return nil, genesisErr
	}

	//var headJson []byte
	var bodyJson []byte
	var blockJson []byte

	//headJson, _ = topGenesisBlock.Header().MarshalJSON()
	bodyJson, _ = json.Marshal(topGenesisBlock.Body())
	blockJson, _ = json.Marshal(topGenesisBlock)

	fmt.Printf("Top Body:\n%s\n\nBlock:\n%s",  string(bodyJson), string(blockJson))

	log.Info("Initialised chain configuration", "config", chainConfig, "genesisHash", genesisHash, "genesis", ethConfig.Genesis, "topBlock", topGenesisBlock)

	eth := &Ethereum{
		ctx: ctx,
		config:         ethConfig,
		chainDb:        chainDb,
		chainConfig:    chainConfig,
		eventMux:       ctx.EventMux,
		accountManager: ctx.AccountManager,
		shutdownChan:   make(chan bool),
		networkID:      ethConfig.NetworkId,
		gasPrice:       ethConfig.MinerGasPrice,
		etherbase:      ethConfig.Etherbase,
		witness:        ethConfig.Witness,
		bloomRequests:  make(chan chan *bloombits.Retrieval),
		bloomIndexer:   NewBloomIndexer(chainDb, params.BloomBitsBlocks, params.BloomConfirms),
	}
	eth.genesisBlock = topGenesisBlock

	log.Info("Initialising Kim protocol", "versions", ProtocolVersions, "network", ethConfig.NetworkId)

	if !ethConfig.SkipBcVersionCheck {
		bcVersion := rawdb.ReadDatabaseVersion(chainDb)
		if bcVersion != core.BlockChainVersion && bcVersion != 0 {
			return nil, fmt.Errorf("Blockchain DB version mismatch (%d / %d).\n", bcVersion, core.BlockChainVersion)
		}
		rawdb.WriteDatabaseVersion(chainDb, core.BlockChainVersion)
	}
	eth.shards = make(map[uint64]*Shard)
	eth.customShards = make(map[uint64]*Shard)
	eth.allShards = make(map[uint64]*Shard)
	
	eth.mineShard = NewShard(chainDb, 0xFF)
	homeChain, _ := NewSubChain(eth, eth.mineShard, types.BLOCK_HOME, ethConfig, chainConfig, topGenesisBlock)
	if homeChain == nil {
		log.Info("Create mine homechain error")
	}
	eth.txPool = homeChain.TxPool()
	//foreignChain, _ := NewSubChain(eth, eth.mineShard, types.BLOCK_FOREIGN, ethConfig, chainConfig, topGenesisBlock)
	// eth.mineShard.homeChain = homeChain
	// eth.mineShard.foreignChain = foreignChain

	eth.shards[0xff] = eth.mineShard
	eth.allShards[0xff] = eth.mineShard

	eth.masternodeManager = NewMasternodeManager(eth, eth.mineShard.DevoteDB())
	if eth.masternodeManager == nil {
		return nil, errors.New("Can not create masternode manager")
	}
	//	eth.protocolManager.mm = eth.masternodeManager
	//, eth.txPool, eth.engine, eth.blockchain
	if eth.protocolManager, err = NewProtocolManager(eth, eth.chainConfig, ethConfig.SyncMode, ethConfig.NetworkId, eth.eventMux, chainDb); err != nil {
		return nil, err
	}

	// Create default shards
	for i := 0; i < 3; i++ {
		shard := NewShard(eth.chainDb, uint64(i))
		// devoteDB, _ := devotedb.NewDevote(devotedb.NewDatabase(eth.chainDb), common.Hash{byte(i)}, common.Hash{byte(i)})
		// shard := &Shard{
		// 	shard:    uint64(i),
		// 	devoteDB: devoteDB,
		// }
		homeChain, _ := NewSubChain(eth, shard, types.BLOCK_HOME, ethConfig, chainConfig, topGenesisBlock)
		if homeChain == nil {

		}
		foreignChain, _ := NewSubChain(eth, shard, types.BLOCK_FOREIGN, ethConfig, chainConfig, topGenesisBlock)
		if foreignChain == nil {

		}

		eth.shards[uint64(i)] = shard
		eth.allShards[uint64(i)] = shard
	}

	eth.APIBackend = &EthAPIBackend{eth, nil}
	gpoParams := ethConfig.GPO
	if gpoParams.Default == nil {
		gpoParams.Default = ethConfig.MinerGasPrice
	}
	eth.APIBackend.gpo = gasprice.NewOracle(eth.APIBackend, gpoParams)

	return eth, nil
}

// Genesis return top Genesis block
func (s *Ethereum) Genesis() *types.Block {
	return s.genesisBlock
}


// Shards return core-shard
func (s *Ethereum) Shards() map[uint64]*Shard {
	return s.shards
}

// CustomShards return custom shard
func (s *Ethereum) CustomShards() map[uint64]*Shard {
	return s.customShards
}

func (s *Ethereum) AllShards() map[uint64]*Shard {
	return s.allShards
}


func makeExtraData(extra []byte) []byte {
	if len(extra) == 0 {
		// create default extradata
		extra, _ = rlp.EncodeToBytes([]interface{}{
			uint(params.VersionMajor<<16 | params.VersionMinor<<8 | params.VersionPatch),
			"geth",
			runtime.Version(),
			runtime.GOOS,
		})
	}
	if uint64(len(extra)) > params.MaximumExtraDataSize {
		log.Warn("Miner extra data exceed limit", "extra", hexutil.Bytes(extra), "limit", params.MaximumExtraDataSize)
		extra = nil
	}
	return extra
}

// CreateDB creates the chain database.
func CreateDB(ctx *node.ServiceContext, config *Config, name string) (ethdb.Database, error) {
	db, err := ctx.OpenDatabase(name, config.DatabaseCache, config.DatabaseHandles)
	if err != nil {
		return nil, err
	}
	if db, ok := db.(*ethdb.LDBDatabase); ok {
		db.Meter("eth/db/chaindata/")
	}
	return db, nil
}

// CreateConsensusEngine creates the required type of consensus engine instance for an Ethereum service
func CreateConsensusEngine(ctx *node.ServiceContext, config *ethash.Config, chainConfig *params.ChainConfig, notify []string, noverify bool, db ethdb.Database) consensus.Engine {

	// TODO: ETZ Change
	// If proof-of-stake is requested, set it up
	if chainConfig.Devote != nil {
		return devote.NewDevote(chainConfig.Devote, db)
	}

	// If proof-of-authority is requested, set it up
	if chainConfig.Clique != nil {
		return clique.New(chainConfig.Clique, db)
	}
	//TODO: WAN Changed
	if chainConfig.Pluto != nil {
		var cliqueCfg params.CliqueConfig
		chainConfig.Clique = &cliqueCfg
		chainConfig.Clique.Period = chainConfig.Pluto.Period
		chainConfig.Clique.Epoch = chainConfig.Pluto.Epoch
		return clique.New(chainConfig.Clique, db)
	}

	// Otherwise assume proof-of-work
	switch config.PowMode {
	case ethash.ModeFake:
		log.Warn("Ethash used in fake mode")
		return ethash.NewFaker()
	case ethash.ModeTest:
		log.Warn("Ethash used in test mode")
		return ethash.NewTester(nil, noverify)
	case ethash.ModeShared:
		log.Warn("Ethash used in shared mode")
		return ethash.NewShared()
	default:
		engine := ethash.New(ethash.Config{
			CacheDir:       ctx.ResolvePath(config.CacheDir),
			CachesInMem:    config.CachesInMem,
			CachesOnDisk:   config.CachesOnDisk,
			DatasetDir:     config.DatasetDir,
			DatasetsInMem:  config.DatasetsInMem,
			DatasetsOnDisk: config.DatasetsOnDisk,
		}, notify, noverify)
		engine.SetThreads(-1) // Disable CPU mining
		return engine
	}
}

// APIs return the collection of RPC services the ethereum package offers.
// NOTE, some of these services probably need to be moved to somewhere else.
func (s *Ethereum) APIs() []rpc.API {
	apis := ethapi.GetAPIs(s.APIBackend)

	// Append any APIs exposed explicitly by the consensus engine
	apis = append(apis, s.Engine().APIs(s.BlockChain())...)

	// Append all the local APIs and return
	return append(apis, []rpc.API{
		{
			Namespace: "eth",
			Version:   "1.0",
			Service:   NewPublicEthereumAPI(s),
			Public:    true,
		}, {
			Namespace: "eth",
			Version:   "1.0",
			Service:   NewPublicMinerAPI(s),
			Public:    true,
		}, {
			Namespace: "eth",
			Version:   "1.0",
			Service:   downloader.NewPublicDownloaderAPI(s.protocolManager.downloader, s.eventMux),
			Public:    true,
		}, {
			Namespace: "miner",
			Version:   "1.0",
			Service:   NewPrivateMinerAPI(s),
			Public:    false,
		}, {
			Namespace: "eth",
			Version:   "1.0",
			Service:   filters.NewPublicFilterAPI(s.APIBackend, false),
			Public:    true,
		}, {
			Namespace: "admin",
			Version:   "1.0",
			Service:   NewPrivateAdminAPI(s),
		}, {
			Namespace: "debug",
			Version:   "1.0",
			Service:   NewPublicDebugAPI(s),
			Public:    true,
		}, {
			Namespace: "debug",
			Version:   "1.0",
			Service:   NewPrivateDebugAPI(s.chainConfig, s),
		}, {
			Namespace: "net",
			Version:   "1.0",
			Service:   s.netRPCService,
			Public:    true,
		},
	}...)
}

func (s *Ethereum) ResetWithGenesisBlock(gb *types.Block) {
	s.BlockChain().ResetWithGenesisBlock(gb)
}

func (s *Ethereum) Etherbase() (eb common.Address, err error) {
	s.lock.RLock()
	etherbase := s.etherbase
	s.lock.RUnlock()

	if etherbase != (common.Address{}) {
		return etherbase, nil
	}
	if wallets := s.AccountManager().Wallets(); len(wallets) > 0 {
		if accounts := wallets[0].Accounts(); len(accounts) > 0 {
			etherbase := accounts[0].Address

			s.lock.Lock()
			s.etherbase = etherbase
			s.lock.Unlock()

			log.Info("Etherbase automatically configured", "address", etherbase)
			return etherbase, nil
		}
	}
	return common.Address{}, fmt.Errorf("etherbase must be explicitly specified")
}

// SetEtherbase sets the mining reward address.
func (s *Ethereum) SetEtherbase(etherbase common.Address) {
	s.lock.Lock()
	s.etherbase = etherbase
	s.lock.Unlock()

	s.Miner().SetEtherbase(etherbase)
}

func (s *Ethereum) Witness() (witness string, err error) {
	s.lock.RLock()
	witness = s.witness
	s.lock.RUnlock()

	if witness != "" {
		return witness, nil
	}
	//if wallets := s.AccountManager().Wallets(); len(wallets) > 0 {
	//	if accounts := wallets[0].Accounts(); len(accounts) > 0 {
	//		fmt.Printf("backend Witness accounts: %x \n", accounts[0].Address)
	//		return accounts[0].Address, nil
	//	}
	//}
	//	if s.masternodeManager.active != nil {
	//		fmt.Printf("backend Witness accounts: %x \n", s.masternodeManager.active.ID)
	//		return s.masternodeManager.active.ID, nil
	//	}
	return "", fmt.Errorf("Witness  must be explicitly specified")
}

// set in js console via admin interface or wrapper from cli flags
func (self *Ethereum) SetWitness(witness string) {
	self.lock.Lock()
	self.witness = witness
	self.lock.Unlock()
}

// StartMining starts the miner with the given number of CPU threads. If mining
// is already running, this method adjust the number of threads allowed to use
// and updates the minimum price required by the transaction pool.
func (s *Ethereum) StartMining(threads int) error {
	// Update the thread count within the consensus engine
	type threaded interface {
		SetThreads(threads int)
	}
	if th, ok := s.Engine().(threaded); ok {
		log.Info("Updated mining threads", "threads", threads)
		if threads == 0 {
			threads = -1 // Disable the miner from within
		}
		th.SetThreads(threads)
	}
	// If the miner was not running, initialize it
	if !s.IsMining() {
		// Propagate the initial price point to the transaction pool
		s.lock.RLock()
		price := s.gasPrice
		s.lock.RUnlock()
		s.txPool.SetGasPrice(price)

		// Configure the local mining addess
		eb, err := s.Etherbase()
		if err != nil {
			log.Error("Cannot start mining without etherbase", "err", err)
			return fmt.Errorf("etherbase missing: %v", err)
		}
		if clique, ok := s.Engine().(*clique.Clique); ok {
			wallet, err := s.accountManager.Find(accounts.Account{Address: eb})
			if wallet == nil || err != nil {
				log.Error("Etherbase account unavailable locally", "err", err)
				return fmt.Errorf("signer missing: %v", err)
			}
			clique.Authorize(eb, wallet.SignHash)
		}
		// If mining is started, we can disable the transaction rejection mechanism
		// introduced to speed sync times.
		atomic.StoreUint32(&s.protocolManager.acceptTxs, 1)

		go s.Miner().Start(eb)
	}
	return nil
}

/*
func (s *Ethereum) StartMining(local bool) error {
	witness, err := s.Witness()
	fmt.Printf("backend StartMining witness:%s\n", witness)
	if err != nil {
		log.Error("Cannot start mining without Witness", "err", err)
		return fmt.Errorf("Witness missing: %v", err)
	}
	//if clique, ok := s.engine.(*clique.Clique); ok {
	//	wallet, err := s.accountManager.Find(accounts.Account{Address: witness})
	//	if wallet == nil || err != nil {
	//		log.Error("Etherbase account unavailable locally", "err", err)
	//		return fmt.Errorf("signer missing: %v", err)
	//	}
	//	clique.Authorize(witness, wallet.SignHash)
	//}
	eb, err := s.Etherbase()
	if devote, ok := s.engine.(*devote.Devote); ok {
		//wallet, err := s.accountManager.Find(accounts.Account{Address: witness})
		//if wallet == nil || err != nil {
		//	log.Error("Coinbase account unavailable locally", "err", err)
		//	return fmt.Errorf("signer missing: %v", err)
		//}

		active := s.masternodeManager.active
		if active == nil {
			log.Error("Active Masternode is nil")
			return fmt.Errorf("signer missing: %v", errors.New("Active Masternode is nil"))
		}
		devote.Authorize(witness, active.SignHash)
	}
	if local {
		// If local (CPU) mining is started, we can disable the transaction rejection
		// mechanism introduced to speed sync times. CPU mining on mainnet is ludicrous
		// so none will ever hit this path, whereas marking sync done on CPU mining
		// will ensure that private networks work in single miner mode too.
		atomic.StoreUint32(&s.protocolManager.acceptTxs, 1)
	}
	go s.miner.Start(eb)
	return nil
}
*/
/*
func (s *Ethereum) StartMining(local bool) error {
	eb, err := s.Etherbase()
	if err != nil {
		log.Error("Cannot start mining without etherbase", "err", err)
		return fmt.Errorf("etherbase missing: %v", err)
	}
	if clique, ok := s.engine.(*clique.Clique); ok {
		wallet, err := s.accountManager.Find(accounts.Account{Address: eb})
		if wallet == nil || err != nil {
			log.Error("Etherbase account unavailable locally", "err", err)
			return fmt.Errorf("signer missing: %v", err)
		}
		clique.Authorize(eb, wallet.SignHash)
	}
	// TODO: WAN Changed
	if ethash, ok := s.engine.(*ethash.Ethash); ok {
		wallet, err := s.accountManager.Find(accounts.Account{Address: eb})
		if wallet == nil || err != nil {
			log.Error("Etherbase account unavailable locally", "err", err)
			return fmt.Errorf("singer missing: %v", err)
		}
		ethash.Authorize(eb, wallet.SignHash)
	}

	if local {
		// If local (CPU) mining is started, we can disable the transaction rejection
		// mechanism introduced to speed sync times. CPU mining on mainnet is ludicrous
		// so none will ever hit this path, whereas marking sync done on CPU mining
		// will ensure that private networks work in single miner mode too.
		atomic.StoreUint32(&s.protocolManager.acceptTxs, 1)
	}
	go s.miner.Start(eb)
	return nil
}
*/

func (s *Ethereum) StopMining()                  { s.mineShard.homeChain.Stop() }
func (s *Ethereum) IsMining() bool               { return s.mineShard.homeChain.miner.Mining() }
func (s *Ethereum) Miner() *miner.Miner          { return s.mineShard.homeChain.miner }
func (s *Ethereum) DevoteDB() *devotedb.DevoteDB { return s.masternodeManager.devoteDB }
func (s *Ethereum) BlockChain() *core.BlockChain { return s.mineShard.homeChain.blockchain }

func (s *Ethereum) TxPool() *core.TxPool     { return s.txPool }
func (s *Ethereum) Engine() consensus.Engine { return s.mineShard.homeChain.engine }

func (s *Ethereum) AccountManager() *accounts.Manager     { return s.accountManager }
func (s *Ethereum) EventMux() *event.TypeMux              { return s.eventMux }
func (s *Ethereum) ChainDb() ethdb.Database               { return s.chainDb }
func (s *Ethereum) IsListening() bool                     { return true } // Always listening
func (s *Ethereum) EthVersion() int                       { return int(s.protocolManager.SubProtocols[0].Version) }
func (s *Ethereum) NetVersion() uint64                    { return s.networkID }
func (s *Ethereum) Downloader() *downloader.Downloader    { return s.protocolManager.downloader }
func (s *Ethereum) MasternodeManager() *MasternodeManager { return s.masternodeManager }

func (s *Ethereum) Shard(shard uint64) *Shard {
	return s.shards[shard]
}

func (s *Ethereum) SubChain(chainType, shardId uint64) *SubChain {
	if shard := s.Shard(shardId); shard != nil {
		if chainType == types.BLOCK_HOME {
			return shard.homeChain
		} else {
			return shard.foreignChain
		}
	}
	return nil
}

func (s *Ethereum) SubBlockChain(chainType, shardId uint64) *core.BlockChain {
	if shard := s.Shard(shardId); shard != nil {
		if chainType == types.BLOCK_HOME {
			return shard.HomeBlockChain()
		} else {
			return shard.ForeignBlockChain()
		}
	}
	return nil
}

func (s *Ethereum) SubBlockChains() []*core.BlockChain {
	subchains := make([]*core.BlockChain, 0)
	for _, shard := range s.Shards() {
		if shard != nil {
			blockchain := shard.HomeBlockChain()
			if blockchain != nil {
				subchains = append(subchains, blockchain)
			}
			if blockchain = shard.ForeignBlockChain(); blockchain != nil {
				subchains = append(subchains, blockchain)
			}
		}
	}
	return subchains
}

// TODO: Return
func (s *Ethereum) SubscribeNewTxsEvent(ch chan<- core.NewTxsEvent) event.Subscription {
	//return pool.scope.Track(pool.txFeed.Subscribe(ch))
	//txPool
	return nil
}

// Protocols implements node.Service, returning all the currently configured
// network protocols to start.
func (s *Ethereum) Protocols() []p2p.Protocol {
	if s.lesServer == nil {
		return s.protocolManager.SubProtocols
	}
	return append(s.protocolManager.SubProtocols, s.lesServer.Protocols()...)
}

// Start implements node.Service, starting all internal goroutines needed by the
// Ethereum protocol implementation.
func (s *Ethereum) Start(srvr *p2p.Server) error {
	// Start the bloom bits servicing goroutines
	s.startBloomHandlers(params.BloomBitsBlocks)

	// Start the RPC service
	s.netRPCService = ethapi.NewPublicNetAPI(srvr, s.NetVersion())

	// Figure out a max peers count based on the server limits
	maxPeers := srvr.MaxPeers
	if s.config.LightServ > 0 {
		if s.config.LightPeers >= srvr.MaxPeers {
			return fmt.Errorf("invalid peer config: light peer count (%d) >= total peer count (%d)", s.config.LightPeers, srvr.MaxPeers)
		}
		maxPeers -= s.config.LightPeers
	}
	// Start the networking layer and the light server if requested
	s.protocolManager.Start(maxPeers)
	//go s.startMasternode(srvr)
	if s.lesServer != nil {
		s.lesServer.Start(srvr)
	}
	return nil
}

// SubscribeVoteEvent registers a subscription of VoteEvent and
// starts sending event to the given channel.
//func (s *Ethereum) SubscribeVoteEvent(ch chan<- core.NewVoteEvent) event.Subscription {
//	return s.masternodeManager.SubscribeVoteEvent(ch)
//}

// SubscribePingEvent registers a subscription of PingEvent and
// starts sending event to the given channel.
//func (s *Ethereum) SubscribePingEvent(ch chan<- core.PingEvent) event.Subscription {
//	return s.masternodeManager.SubscribePingEvent(ch)
//}

func (s *Ethereum) startMasternode(srvr *p2p.Server) {
	t := time.NewTimer(5 * time.Second)
	for {
		select {
		case <-t.C:
			if s.Downloader().Synchronising() {
				t.Reset(5 * time.Second)
				break
			}
			s.MasternodeManager().Start(srvr, s.protocolManager.peers)
			break
		}
	}

}

// Stop implements node.Service, terminating all internal goroutines used by the
// Ethereum protocol.
func (s *Ethereum) Stop() error {
	s.bloomIndexer.Close()
	for _, shard := range s.allShards {
		shard.Stop()
	}

	s.protocolManager.Stop()
	if s.lesServer != nil {
		s.lesServer.Stop()
	}
	s.txPool.Stop()
	s.eventMux.Stop()

	s.chainDb.Close()
	close(s.shutdownChan)
	return nil
}
