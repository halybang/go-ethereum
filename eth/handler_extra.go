package eth

import (
	"encoding/json"
	"errors"
	//"fmt"
	//"math"
	"math/big"
	//"sync"
	"sync/atomic"
	"time"

	"github.com/ethereum/go-ethereum/common"
	//"github.com/ethereum/go-ethereum/consensus"
	//"github.com/ethereum/go-ethereum/consensus/misc"
	//"github.com/ethereum/go-ethereum/core"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/eth/downloader"
	//"github.com/ethereum/go-ethereum/eth/fetcher"
	//"github.com/ethereum/go-ethereum/ethdb"
	//"github.com/ethereum/go-ethereum/event"
	"github.com/ethereum/go-ethereum/log"
	"github.com/ethereum/go-ethereum/p2p"
	//"github.com/ethereum/go-ethereum/p2p/discover"
	//"github.com/ethereum/go-ethereum/params"
	"github.com/ethereum/go-ethereum/rlp"
)

var errUnknownMsg = errors.New("Unknown peer message")

// msg.Code == StatusMsg
func (pm *ProtocolManager) handleStatusMsg(p *peer, msg *p2p.Msg) error {
	return errResp(ErrExtraStatusMsg, "uncontrolled status message")
}

func (pm *ProtocolManager) handleGetBlockHeadersMsg(p *peer, msg *p2p.Msg) error {
	// Decode the complex header query
	var query getBlockHeadersData
	if err := msg.Decode(&query); err != nil {
		return errResp(ErrDecode, "%v: %v", msg, err)
	}
	hashMode := query.Origin.Hash != (common.Hash{})
	first := true
	maxNonCanonical := uint64(100)

	// Gather headers until the fetch or network limits is reached
	var (
		bytes   common.StorageSize
		headers []*types.Header
		unknown bool
	)
	for !unknown && len(headers) < int(query.Amount) && bytes < softResponseLimit && len(headers) < downloader.MaxHeaderFetch {
		// Retrieve the next header satisfying the query
		var origin *types.Header
		blockchain := pm.eth.SubChain(query.Type, query.Shard).BlockChain()
		if blockchain == nil {
			break
		}
		if hashMode {
			if first {
				first = false
				origin = blockchain.GetHeaderByHash(query.Origin.Hash)
				if origin != nil {
					query.Origin.Number = origin.Number.Uint64()
				}
			} else {
				origin = blockchain.GetHeader(query.Origin.Hash, query.Origin.Number)
			}
		} else {
			origin = blockchain.GetHeaderByNumber(query.Origin.Number)
		}
		if origin == nil {
			break
		}
		headers = append(headers, origin)
		bytes += estHeaderRlpSize

		// Advance to the next header of the query
		switch {
		case hashMode && query.Reverse:
			// Hash based traversal towards the genesis block
			ancestor := query.Skip + 1
			if ancestor == 0 {
				unknown = true
			} else {
				query.Origin.Hash, query.Origin.Number = blockchain.GetAncestor(query.Origin.Hash, query.Origin.Number, ancestor, &maxNonCanonical)
				unknown = (query.Origin.Hash == common.Hash{})
			}
		case hashMode && !query.Reverse:
			// Hash based traversal towards the leaf block
			var (
				current = origin.Number.Uint64()
				next    = current + query.Skip + 1
			)
			if next <= current {
				infos, _ := json.MarshalIndent(p.Peer.Info(), "", "  ")
				p.Log().Warn("GetBlockHeaders skip overflow attack", "current", current, "skip", query.Skip, "next", next, "attacker", infos)
				unknown = true
			} else {
				if header := blockchain.GetHeaderByNumber(next); header != nil {
					nextHash := header.Hash()
					expOldHash, _ := blockchain.GetAncestor(nextHash, next, query.Skip+1, &maxNonCanonical)
					if expOldHash == query.Origin.Hash {
						query.Origin.Hash, query.Origin.Number = nextHash, next
					} else {
						unknown = true
					}
				} else {
					unknown = true
				}
			}
		case query.Reverse:
			// Number based traversal towards the genesis block
			if query.Origin.Number >= query.Skip+1 {
				query.Origin.Number -= query.Skip + 1
			} else {
				unknown = true
			}

		case !query.Reverse:
			// Number based traversal towards the leaf block
			query.Origin.Number += query.Skip + 1
		}
	}
	return p.SendBlockHeaders(headers)
}

func (pm *ProtocolManager) handleBlockHeadersMsg(p *peer, msg *p2p.Msg) error {
	// A batch of headers arrived to one of our previous requests
	var headers []*types.Header
	if err := msg.Decode(&headers); err != nil {
		return errResp(ErrDecode, "msg %v: %v", msg, err)
	}
	// Filter out any explicitly requested headers, deliver the rest to the downloader
	filter := len(headers) == 1
	if filter {
		// Irrelevant of the fork checks, send the header to the fetcher just in case
		headers = pm.fetcher.FilterHeaders(p.id, headers, time.Now())
	}
	if len(headers) > 0 || !filter {
		err := pm.downloader.DeliverHeaders(p.id, headers)
		if err != nil {
			log.Debug("Failed to deliver headers", "err", err)
		}
	}
	return nil
}

func (pm *ProtocolManager) handleGetBlockBodiesMsg(p *peer, msg *p2p.Msg) error {
	// Decode the retrieval message
	msgStream := rlp.NewStream(msg.Payload, uint64(msg.Size))
	if _, err := msgStream.List(); err != nil {
		return err
	}
	// Gather blocks until the fetch or network limits is reached
	var (
		hash   common.Hash
		bytes  int
		bodies []rlp.RawValue
	)
	for bytes < softResponseLimit && len(bodies) < downloader.MaxBlockFetch {
		// Retrieve the hash of the next block
		if err := msgStream.Decode(&hash); err == rlp.EOL {
			break
		} else if err != nil {
			return errResp(ErrDecode, "msg %v: %v", msg, err)
		}
		// Retrieve the requested block body, stopping if enough was found
		for _, shard := range pm.eth.Shards() {
			if shard != nil {
				blockchain := shard.HomeBlockChain()
				if blockchain != nil {
					if data := blockchain.GetBodyRLP(hash); len(data) != 0 {
						bodies = append(bodies, data)
						bytes += len(data)
						break
					}
				}
				blockchain = shard.ForeignBlockChain()
				if blockchain != nil {
					if data := blockchain.GetBodyRLP(hash); len(data) != 0 {
						bodies = append(bodies, data)
						bytes += len(data)
						break
					}
				}
			}
		}
	}
	return p.SendBlockBodiesRLP(bodies)
}

func (pm *ProtocolManager) handleBlockBodiesMsg(p *peer, msg *p2p.Msg) error {
	// A batch of block bodies arrived to one of our previous requests
	var request blockBodiesData
	if err := msg.Decode(&request); err != nil {
		return errResp(ErrDecode, "msg %v: %v", msg, err)
	}
	// Deliver them all to the downloader for queuing
	transactions := make([][]*types.Transaction, len(request))
	uncles := make([][]*types.Header, len(request))

	for i, body := range request {
		transactions[i] = body.Transactions
		uncles[i] = body.Uncles
	}
	// Filter out any explicitly requested bodies, deliver the rest to the downloader
	filter := len(transactions) > 0 || len(uncles) > 0
	if filter {
		transactions, uncles = pm.fetcher.FilterBodies(p.id, transactions, uncles, time.Now())
	}
	if len(transactions) > 0 || len(uncles) > 0 || !filter {
		err := pm.downloader.DeliverBodies(p.id, transactions, uncles)
		if err != nil {
			log.Debug("Failed to deliver bodies", "err", err)
		}
	}
	return nil
}

func (pm *ProtocolManager) handleGetNodeDataMsg(p *peer, msg *p2p.Msg) error {
	// Decode the retrieval message
	msgStream := rlp.NewStream(msg.Payload, uint64(msg.Size))
	if _, err := msgStream.List(); err != nil {
		return err
	}
	// Gather state data until the fetch or network limits is reached
	var (
		hash  common.Hash
		bytes int
		data  [][]byte
	)
	for bytes < softResponseLimit && len(data) < downloader.MaxStateFetch {
		// Retrieve the hash of the next state entry
		if err := msgStream.Decode(&hash); err == rlp.EOL {
			break
		} else if err != nil {
			return errResp(ErrDecode, "msg %v: %v", msg, err)
		}
		for _, shard := range pm.eth.Shards() {
			if shard != nil {
				blockchain := shard.HomeBlockChain()
				if blockchain != nil {
					// Retrieve the requested state entry, stopping if enough was found
					if entry, err := blockchain.TrieNode(hash); err == nil {
						data = append(data, entry)
						bytes += len(entry)
						break
					}
				}
				blockchain = shard.ForeignBlockChain()
				if blockchain != nil {
					if entry, err := blockchain.TrieNode(hash); err == nil {
						data = append(data, entry)
						bytes += len(entry)
						break
					}
				}
			}
		}
	}
	return p.SendNodeData(data)
}

func (pm *ProtocolManager) handleNodeDataMsg(p *peer, msg *p2p.Msg) error {
	// A batch of node state data arrived to one of our previous requests
	var data [][]byte
	if err := msg.Decode(&data); err != nil {
		return errResp(ErrDecode, "msg %v: %v", msg, err)
	}
	// Deliver all to the downloader
	if err := pm.downloader.DeliverNodeData(p.id, data); err != nil {
		log.Debug("Failed to deliver node state data", "err", err)
	}
	return nil
}

func (pm *ProtocolManager) handleGetReceiptsMsg(p *peer, msg *p2p.Msg) error {
	// Decode the retrieval message
	msgStream := rlp.NewStream(msg.Payload, uint64(msg.Size))
	if _, err := msgStream.List(); err != nil {
		return err
	}
	// Gather state data until the fetch or network limits is reached
	var (
		hash     common.Hash
		bytes    int
		receipts []rlp.RawValue
	)
	for bytes < softResponseLimit && len(receipts) < downloader.MaxReceiptFetch {
		// Retrieve the hash of the next block
		if err := msgStream.Decode(&hash); err == rlp.EOL {
			break
		} else if err != nil {
			return errResp(ErrDecode, "msg %v: %v", msg, err)
		}
		for _, shard := range pm.eth.Shards() {
			if shard != nil {
				blockchain := shard.HomeBlockChain()
				if blockchain != nil {
					encode := true
					// Retrieve the requested block's receipts, skipping if unknown to us
					results := blockchain.GetReceiptsByHash(hash)
					if results == nil {
						if header := blockchain.GetHeaderByHash(hash); header == nil || header.ReceiptHash != types.EmptyRootHash {
							encode = false
						}
					}
					if encode {
						// If known, encode and queue for response packet
						if encoded, err := rlp.EncodeToBytes(results); err != nil {
							log.Error("Failed to encode receipt", "err", err)
						} else {
							receipts = append(receipts, encoded)
							bytes += len(encoded)
						}
					}
				}
				blockchain = shard.ForeignBlockChain()
				if blockchain != nil {

				}
			}
		}
	}
	return p.SendReceiptsRLP(receipts)
}

func (pm *ProtocolManager) handleReceiptsMsg(p *peer, msg *p2p.Msg) error {
	// A batch of receipts arrived to one of our previous requests
	var receipts [][]*types.Receipt
	if err := msg.Decode(&receipts); err != nil {
		return errResp(ErrDecode, "msg %v: %v", msg, err)
	}
	// Deliver all to the downloader
	if err := pm.downloader.DeliverReceipts(p.id, receipts); err != nil {
		log.Debug("Failed to deliver receipts", "err", err)
	}

	return nil
}

func (pm *ProtocolManager) handleNewBlockHashesMsg(p *peer, msg *p2p.Msg) error {
	var announces newBlockHashesData
	if err := msg.Decode(&announces); err != nil {
		return errResp(ErrDecode, "%v: %v", msg, err)
	}
	// Mark the hashes as present at the remote node
	for _, block := range announces {
		p.MarkBlock(block.Hash)
	}
	// Schedule all the unknown hashes for retrieval
	unknown := make(newBlockHashesData, 0, len(announces))
	for _, block := range announces {
		if blockchain := pm.eth.SubBlockChain(block.Type, block.Shard); blockchain != nil {
			if !blockchain.HasBlock(block.Hash, block.Number) {
				unknown = append(unknown, block)
			}
		}
	}
	for _, block := range unknown {
		pm.fetcher.Notify(p.id, block.Hash, block.Number, time.Now(), p.RequestOneHeader, p.RequestBodies)
	}
	return nil
}

func (pm *ProtocolManager) handleNewBlockMsg(p *peer, msg *p2p.Msg) error {
	// Retrieve and decode the propagated block
	var request newBlockData
	if err := msg.Decode(&request); err != nil {
		return errResp(ErrDecode, "%v: %v", msg, err)
	}
	request.Block.ReceivedAt = msg.ReceivedAt
	request.Block.ReceivedFrom = p

	// Mark the peer as owning the block and schedule it for import
	p.MarkBlock(request.Block.Hash())
	pm.fetcher.Enqueue(p.id, request.Block)

	// Assuming the block is importable by the peer, but possibly not yet done so,
	// calculate the head hash and TD that the peer truly must have.
	var (
		trueHead  = request.Block.ParentHash()
		trueTD    = new(big.Int).Sub(request.TD, request.Block.Difficulty())
		trueType  = uint64(request.Block.Type())
		trueShard = request.Block.ShardId()
	)
	// Update the peers total difficulty if better than the previous
	if _, td := p.Head(); trueTD.Cmp(td) > 0 {
		p.SetHead(trueHead, trueTD)

		// Schedule a sync if above ours. Note, this will not fire a sync for a gap of
		// a singe block (as the true TD is below the propagated block), however this
		// scenario should easily be covered by the fetcher.
		if blockchain := pm.eth.SubBlockChain(trueType, trueShard); blockchain != nil {
			currentBlock := blockchain.CurrentBlock()
			if trueTD.Cmp(blockchain.GetTd(currentBlock.Hash(), currentBlock.NumberU64())) > 0 {
				go pm.synchronise(p)
			}
		}
	}
	return nil
}

func (pm *ProtocolManager) handleTxMsg(p *peer, msg *p2p.Msg) error {
	// Transactions arrived, make sure we have a valid and fresh chain to handle them
	if atomic.LoadUint32(&pm.acceptTxs) == 0 {
		return nil
	}
	// Transactions can be processed, parse all of them and deliver to the pool
	var txs []*types.Transaction
	if err := msg.Decode(&txs); err != nil {
		return errResp(ErrDecode, "msg %v: %v", msg, err)
	}
	for i, tx := range txs {
		// Validate and mark the remote transaction
		if tx == nil {
			return errResp(ErrDecode, "transaction %d is nil", i)
		}
		p.MarkTransaction(tx.Hash())
		addr := tx.Account()
		shardId := addr.Shard()
		if shard, ok := pm.eth.Shards()[shardId]; ok && shard != nil {
			shard.AddRemote(tx)
		}

		addr = common.BytesToAddress(tx.Link().Bytes())
		shardId = addr.Shard()
		if shard, ok := pm.eth.Shards()[shardId]; ok && shard != nil {
			shard.AddRemote(tx)
		}
		isContract := addr[0]
		if isContract == 0x3 {
			shardId := addr.Id()
			if shard, ok := pm.eth.CustomShards()[shardId]; ok && shard != nil {
				shard.AddRemote(tx)
			}
		}
	}
	// TODO: KIM REMOVED
	// pm.txpool.AddRemotes(txs)
	return nil
}

func (pm *ProtocolManager) handleMasternodePingMsg(p *peer, msg *p2p.Msg) error {
	//	var ping = &masternode.PingMsg{}
	//	if err := msg.Decode(ping); err != nil {
	//		err := pm.mm.ProcessPingMsg(ping)
	//		if err != nil {
	//			log.Error("ProcessPingMsg", "error", err)
	//			break
	//		}
	//	}
	return nil
}

func (pm *ProtocolManager) handleMsg2(p *peer) error {

	msgPeer, err := p.rw.ReadMsg()
	if err != nil {
		return err
	}
	if msgPeer.Size > ProtocolMaxMsgSize {
		return errResp(ErrMsgTooLarge, "%v > %v", msgPeer.Size, ProtocolMaxMsgSize)
	}
	defer msgPeer.Discard()
	msg := &msgPeer
	// Handle the message depending on its contents
	switch {
	case msg.Code == StatusMsg:
		// Status messages should never arrive after the handshake
		return pm.handleStatusMsg(p, msg)
	// Block header query, collect the requested headers and reply
	case msg.Code == GetBlockHeadersMsg:
		return pm.handleGetBlockHeadersMsg(p, msg)
	case msg.Code == BlockHeadersMsg:
		return pm.handleBlockHeadersMsg(p, msg)
	case msg.Code == GetBlockBodiesMsg:
		return pm.handleGetBlockBodiesMsg(p, msg)
	case msg.Code == BlockBodiesMsg:
		return pm.handleBlockBodiesMsg(p, msg)
	case p.version >= eth63 && msg.Code == GetNodeDataMsg:
		return pm.handleGetNodeDataMsg(p, msg)
	case p.version >= eth63 && msg.Code == NodeDataMsg:
		return pm.handleNodeDataMsg(p, msg)
	case p.version >= eth63 && msg.Code == GetReceiptsMsg:
		return pm.handleGetReceiptsMsg(p, msg)
	case p.version >= eth63 && msg.Code == ReceiptsMsg:
		return pm.handleReceiptsMsg(p, msg)
	case msg.Code == NewBlockHashesMsg:
		return pm.handleNewBlockHashesMsg(p, msg)
	case msg.Code == NewBlockMsg:
		return pm.handleNewBlockMsg(p, msg)
	case msg.Code == TxMsg:
		return pm.handleTxMsg(p, msg)
	case msg.Code == MasternodePingMsg:
		return pm.handleMasternodePingMsg(p, msg)
	default:
		return errResp(ErrInvalidMsgCode, "%v", msg.Code)
	}
	return nil
}
