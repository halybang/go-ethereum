// Copyright 2015 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package params

// MainnetBootnodes are the enode URLs of the P2P bootstrap nodes running on
// the main Ethereum network.
var MainnetBootnodes = []string{
	"enode://66dcd06b384a5b68e758d3e4fad39e7735a85dbcd0d21d2d9f678c7e6a1cba2ffa118aeb2d45d4c8b626605f8680ef1d89d9de30fa9f25826fbfeeae4b5e0e6a@104.223.34.199:26890",
}

var MainnetMasternodes = []string{
	"enode://66dcd06b384a5b68e758d3e4fad39e7735a85dbcd0d21d2d9f678c7e6a1cba2ffa118aeb2d45d4c8b626605f8680ef1d89d9de30fa9f25826fbfeeae4b5e0e6a@104.223.34.199:26890",
}

// TestnetBootnodes are the enode URLs of the P2P bootstrap nodes running on the
// Ropsten test network.
var TestnetBootnodes = []string{
	"enode://337f86a67dfc5df262b8c1619d67b3e07491a51e96c8f59f12fb2fa3b378559930c77fbc2de4cba8b101e340d72e4934e30c53785dd78bcc72d95d6324f4fdb3@104.223.34.199:27777",
}

var TestnetMasternodes = []string{
	"enode://337f86a67dfc5df262b8c1619d67b3e07491a51e96c8f59f12fb2fa3b378559930c77fbc2de4cba8b101e340d72e4934e30c53785dd78bcc72d95d6324f4fdb3@104.223.34.199:27777",
}

// RinkebyBootnodes are the enode URLs of the P2P bootstrap nodes running on the
// Rinkeby test network.
var RinkebyBootnodes = []string{
	"enode://a1c35e8e2b66486ec326763853824cff23228b93fb7b3a3eec05e52be73b4aeae79a30f73b282b1b27c3879fb8cd84ac9718dc107ba2baf1ed54ee8911aaa832@104.223.34.199:28888",
}

var PlutoBootnodes = []string{
	"enode://a1c35e8e2b66486ec326763853824cff23228b93fb7b3a3eec05e52be73b4aeae79a30f73b282b1b27c3879fb8cd84ac9718dc107ba2baf1ed54ee8911aaa832@104.223.34.199:28888",
}
var InternalBootnodes = []string{
	"enode://00a3a42cd59c13f45d303a6d221dcfb87b85c4c7263f6a49db32bdde176ac92964e1d911d8f32147050b9e272e9096b037f9a5658d8cc498790e7da29b7a457f@104.223.34.199:29999",
}

// DiscoveryV5Bootnodes are the enode URLs of the P2P bootstrap nodes for the
// experimental RLPx v5 topic-discovery network.
var DiscoveryV5Bootnodes = []string{}
