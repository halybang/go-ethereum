// Copyright 2018 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

// Package rawdb contains a collection of low level database accessors.
package rawdb

import (
	"encoding/binary"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/metrics"
)

// The fields below define the low level database schema prefixing.
var (
	// databaseVerisionKey tracks the current database version.
	databaseVersionKey = []byte("DatabaseVersion")

	genesisHeaderKey = []byte("GenesisHeader") // Top genesis header
	genesisBodyKey   = []byte("GenesisBody")   // Top genesis Body

	// headHeaderKey tracks the latest know header's hash.
	headHeaderPrefix = []byte("LastHeader")

	// headBlockKey tracks the latest know full block's hash.
	headBlockPrefix = []byte("LastBlock")

	// headFastBlockKey tracks the latest known incomplete block's hash duirng fast sync.
	headFastBlockPrefix = []byte("LastFast")

	// fastTrieProgressKey tracks the number of trie entries imported during fast sync.
	fastTrieProgressPrefix = []byte("TrieSync")

	accountPrefix            = []byte("Aa") // accountPrefix + Account Address  -> account lastest transaction
	accountWithNonce         = []byte("An") // accountWithNonce + Account Adddress + nonce ->  hash of transaction at nonce
	accountReceivedPrefix    = []byte("Ar") // accountReceivedPrefix + Account Address + tx hash -> received tx hash
	accountRxLookupPrefix    = []byte("Al") // accountRxLookupPrefix + Account Address  + blockHash -> Account Lookup Entry at foreign chain, using for received
	accountRxLookupTopPrefix = []byte("Ah") // accountRxLookupTopPrefix + Account Address -> Account Lastest Lookup Entry at foreign chain, using for received

	transactionPrefix         = []byte("xH") // transactionPrefix + tx hash  -> full transaction
	receivedTransactionPrefix = []byte("xD") // receivedTransactionPrefix + tx hash  -> received transaction's hash

	// Data item prefixes (use single byte to avoid mixing data types, avoid `i`, used for indexes).
	headerPrefix       = []byte("h") // headerPrefix + num (uint64 big endian) + hash -> header
	headerTDSuffix     = []byte("t") // headerPrefix + num (uint64 big endian) + hash + headerTDSuffix -> td
	headerHashSuffix   = []byte("n") // headerPrefix + num (uint64 big endian) + headerHashSuffix -> hash
	headerNumberPrefix = []byte("H") // headerNumberPrefix + hash -> num (uint64 big endian)

	blockBodyPrefix      = []byte("b")  // blockBodyPrefix + num (uint64 big endian) + hash -> block body
	blockBodyLightPrefix = []byte("bl") // blockBodyPrefix + num (uint64 big endian) + hash -> block body
	blockReceiptsPrefix  = []byte("r")  // blockReceiptsPrefix + num (uint64 big endian) + hash -> block receipts

	txLookupPrefix  = []byte("l") // txLookupPrefix + hash -> transaction/receipt lookup metadata
	bloomBitsPrefix = []byte("B") // bloomBitsPrefix + bit (uint16 big endian) + section (uint64 big endian) + hash -> bloom bits

	preimagePrefix    = []byte("secure-key-")      // preimagePrefix + hash -> preimage
	configPrefix      = []byte("ethereum-config-") // config prefix for the db
	shardConfigPrefix = []byte("shard-config-")    // config prefix for the db

	// Chain index prefixes (use `i` + single byte to avoid mixing data types).
	BloomBitsIndexPrefix = []byte("iB") // BloomBitsIndexPrefix is the data table of a chain indexer to track its progress

	preimageCounter    = metrics.NewRegisteredCounter("db/preimage/total", nil)
	preimageHitCounter = metrics.NewRegisteredCounter("db/preimage/hits", nil)
)

// TxLookupEntry is a positional metadata to help looking up the data content of
// a transaction or receipt given only its hash.
type TxLookupEntry struct {
	BlockHash  common.Hash
	BlockIndex uint64
	Index      uint64
}

// encodeBlockNumber encodes a block number as big endian uint64
func encodeBlockNumber(number uint64) []byte {
	enc := make([]byte, 8)
	binary.BigEndian.PutUint64(enc, number)
	return enc
}

func accountKey(account common.Address) []byte {
	return append(accountPrefix, account.Bytes()...)
}

func accountWithNonceKey(account common.Address, number uint64) []byte {
	return append(append(accountWithNonce, account.Bytes()...), encodeBlockNumber(number)...)
}

func transactionKey(hash common.Hash) []byte {
	return append(transactionPrefix, hash.Bytes()...)
}
func receivedTransactionKey(hash common.Hash) []byte {
	return append(receivedTransactionPrefix, hash.Bytes()...)
}

func headHeaderKey(chainType uint64, shard uint64) []byte {
	return append(append(headHeaderPrefix, encodeBlockNumber(chainType)...), encodeBlockNumber(shard)...)
}

func headBlockKey(chainType uint64, shard uint64) []byte {
	return append(append(headBlockPrefix, encodeBlockNumber(chainType)...), encodeBlockNumber(shard)...)
}

func headFastBlockKey(chainType uint64, shard uint64) []byte {
	return append(append(fastTrieProgressPrefix, encodeBlockNumber(chainType)...), encodeBlockNumber(shard)...)
}

func fastTrieProgressKey(chainType uint64, shard uint64) []byte {
	return append(append(headFastBlockPrefix, encodeBlockNumber(chainType)...), encodeBlockNumber(shard)...)
}

// headerKey = headerPrefix + shard + num (uint64 big endian) + hash
func headerKey(number uint64, hash common.Hash) []byte {
	return append(append(headerPrefix, encodeBlockNumber(number)...), hash.Bytes()...)
	//return append(append(append(headerPrefix, encodeBlockNumber(shard)...), encodeBlockNumber(number)...), hash.Bytes()...)
}

// headerTDKey = headerPrefix + num (uint64 big endian) + hash + headerTDSuffix
func headerTDKey(number uint64, hash common.Hash) []byte {
	return append(headerKey(number, hash), headerTDSuffix...)
}

// headerHashKey = headerPrefix + num (uint64 big endian) + headerHashSuffix
func headerHashKey(chainType, shard, number uint64) []byte {
	return append(append(append(append(headerPrefix, encodeBlockNumber(chainType)...), encodeBlockNumber(shard)...), encodeBlockNumber(number)...), headerHashSuffix...)
}

// headerNumberKey = headerNumberPrefix + hash
func headerNumberKey(hash common.Hash) []byte {
	return append(headerNumberPrefix, hash.Bytes()...)
}

// blockBodyKey = blockBodyPrefix + num (uint64 big endian) + hash
func blockBodyKey(number uint64, hash common.Hash) []byte {
	return append(append(blockBodyPrefix, encodeBlockNumber(number)...), hash.Bytes()...)
}
func blockBodyLightKey(number uint64, hash common.Hash) []byte {
	return append(append(blockBodyLightPrefix, encodeBlockNumber(number)...), hash.Bytes()...)
}

// blockReceiptsKey = blockReceiptsPrefix + num (uint64 big endian) + hash
func blockReceiptsKey(number uint64, hash common.Hash) []byte {
	return append(append(blockReceiptsPrefix, encodeBlockNumber(number)...), hash.Bytes()...)
}

// txLookupKey = txLookupPrefix + hash
func txLookupKey(chainType, shard uint64, hash common.Hash) []byte {
	return append(append(append(txLookupPrefix, encodeBlockNumber(chainType)...), encodeBlockNumber(shard)...), hash.Bytes()...)
}

// bloomBitsKey = bloomBitsPrefix + bit (uint16 big endian) + section (uint64 big endian) + hash
func bloomBitsKey(bit uint, section uint64, hash common.Hash) []byte {
	//key := append(append(append(append(bloomBitsPrefix, make([]byte, 10)...), encodeBlockNumber(chainType)...), encodeBlockNumber(shard)...), hash.Bytes()...)
	key := append(append(bloomBitsPrefix, make([]byte, 10)...), hash.Bytes()...)

	binary.BigEndian.PutUint16(key[1:], uint16(bit))
	binary.BigEndian.PutUint64(key[3:], section)

	return key
}

// preimageKey = preimagePrefix + hash
func preimageKey(hash common.Hash) []byte {
	return append(preimagePrefix, hash.Bytes()...)
}

// configKey = configPrefix + hash
func configKey(hash common.Hash) []byte {
	return append(configPrefix, hash.Bytes()...)
}

/////////////////////////
func accountRxLookupKey(address common.Address, blockHash common.Hash) []byte {
	return append(append(accountRxLookupPrefix, address.Bytes()...), blockHash.Bytes()...)
}

func accountRxLookupTopKey(address common.Address) []byte {
	return append(accountRxLookupTopPrefix, address.Bytes()...)
}

// txReceivedKey = accountReceivedPrefix + address + hash (other send to address) -> which tx
func accountReceivedKey(address common.Address, txHash common.Hash) []byte {
	return append(append(accountReceivedPrefix, address.Bytes()...), txHash.Bytes()...)
}
