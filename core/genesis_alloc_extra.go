// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package core

// Constants containing the genesis allocation of built-in genesis blocks.
// Their content is an RLP-encoded list of (address, balance) tuples.
// Use mkalloc.go to create/update them.

var PlutoAllocJson = `{
	"0x2d0e7c0813a51d3bd1d08246af2a8a7a57d8922e": {"balance": "100000000000000000000"},
	"0xe20bfe3c8777036ca0ab03f3bcbbfb438d97dd91": {"balance": "100000000000000000000"},
	"0x9da26fc2e1d6ad9fdd46138906b0104ae68a65d8": {"balance": "100000000000000000000"},
 	"0xb02737095f945768ca983a60e0ba92b758111111": {"balance": "10000000000000000000000000000000000000"},
	"0xb752027021340f2fec33abc91daf198915bbbbbb": {"balance": "10000000000000000000000000000000000000"},
	"0xe8ffc3d0c02c0bfc39b139fa49e2c5475f000000": {"balance": "10000000000000000000000000000000000000"}
 }`

const devAllocData = "\xf9\x01\x94\xc2\x01\x01\xc2\x02\x01\xc2\x03\x01\xc2\x04\x01\xf0\x94\x1a&3\x8f\r\x90^)_\u0337\x1f\xa9\ua11f\xfa\x12\xaa\xf4\x9a\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xf0\x94.\xf4q\x00\xe0x{\x91Q\x05\xfd^?O\xf6u y\xd5\u02da\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xf0\x94l8jK&\xf7<\x80/4g?rH\xbb\x11\x8f\x97BJ\x9a\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xf0\x94\xb9\xc0\x15\x91\x8b\u06ba$\xb4\xff\x05z\x92\xa3\x87=n\xb2\x01\xbe\x9a\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xf0\x94\xcd*=\x9f\x93\x8e\x13\u0354~\xc0Z\xbc\u007f\xe74\u07cd\xd8&\x9a\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xf0\x94\xdb\xdb\xdb,\xbd#\xb7\x83t\x1e\x8d\u007f\xcfQ\xe4Y\xb4\x97\u499a\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xf0\x94\xe4\x15{4\xea\x96\x15\u03fd\xe6\xb4\xfd\xa4\x19\x82\x81$\xb7\fx\x9a\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xf0\x94\xe6qo\x95D\xa5lS\r\x86\x8eK\xfb\xac\xb1r1[\u07ad\x9a\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

/*
* wanchain balance allocation

* wanchain allocation unit: 1 wan = 1000000000000000000 win

* wanchain wancoin total:210000000 wan


* wanchain foundattion balance total: 19%, 39900000 wan

* wanchain foundation address 1 balance:1０％,21000000 wan
* "0x4A2a82864c2Db7091142C2078543FaB0286251a9": {"balance": "21000000000000000000000000"},

* wanchain foundation address 2 balance:６％, 12600000 wan
* "0x0dA4512bB81fA50891533d9A956238dCC219aBCf": {"balance": "12600000000000000000000000"},

* wanchain foundation address 3 balance:３％, 6300000 wan
* "0xD209fe3073Ca6B61A1a8A7b895e1F4aD997103E1": {"balance": "6300000000000000000000000"},

* wanchain miner total: 10%, 21000000 wan
* "0x01d0689001F18637993948e487a15BF3064b16e4": {"balance": "21000000000000000000000000"},


* wanchain open sold balance:total 51%, 107100000 wan

* wanchain open sold address 1 balance:１０％,21000000 wan
* "0xb3a5c9789A4d882BceF63abBe9B7893aC505bf60": {"balance": "21000000000000000000000000"},

* wanchain open sold address 2 balance:10%,21000000 wan
* "0xc57FeeC601d5A473fE9d1D70Af26ac639e0c61a1": {"balance": "21000000000000000000000000"},

* wanchain open sold address 3 balance:１０％,21000000 wan
* "0xEeCABC0900998aFeE0B52438a6003F2388c78A62": {"balance": "21000000000000000000000000"},

* wanchain open sold address 4 balance:１０％,21000000 wan
* "0x2dC9A6A04Bc004a8f68f0e886a463AeF23D43030": {"balance": "21000000000000000000000000"},

* wanchain open sold address 5 balance:１０％,21000000 wan
* "0x5866dD6794B8996E5bC745D508AC6901FF3b0427": {"balance": "21000000000000000000000000"},

* wanchain open sold address 6 balance:1%,2100000 wan
* "0x89442477dC39A2503E30D1f8d7FFD4Ea5f87a2aF": {"balance":  "2100000000000000000000000"},


* wanchain develop team balance:20%,42000000 wan

* wanchain develop team address:10%,21000000 wan
* "0xae8d9B975eC8df8359eA79e50e89b18601816aC3": {"balance": "21000000000000000000000000"},

* wanchain develop team address:5%,10500000 wan
* "0x53D81A644a0d1081D6C6E8B25f807C2cFb6edE35": {"balance": "10500000000000000000000000"},

* wanchain develop team address:5%,10500000 wan
* "0x3B9289124f04194F0b3C4F8F862fE1Fbac59c978": {"balance": "10500000000000000000000000"}

 */

const wanchainAllocJson = `{
		"0x010182864c2Db7091142C2078543FaB0286251a9000000000000000000000000": {"balance": "21000000000000000000000000"}
}`

//miner reward
//public sale
//Team holding
//Foundation operation
const wanchainTestAllocJson = `{
	  "0x4cb79c7868cd88629df6d4fa8637dda83d13ef27": {"balance": "21000000000000000000000000"},
	  "0xeb71d33d5c7cf05d9177934200c51efa53057c27": {"balance": "107100000000000000000000000"},
      "0x6b4683cafa549d9f4c06815a2397cef5a540b919": {"balance": "42000000000000000000000000"},
	  "0xbb9003ca8226f411811dd16a3f1a2c1b3f71825d": {"balance": "39900000000000000000000000"}
}`

const wanchainPPOWTestAllocJson = `{
	  "0xbd100cf8286136659a7d63a38a154e28dbf3e0fd": {"balance": "3000000000000000000000000000"},
	  "0xF9b32578b4420a36F132DB32b56f3831A7CC1804": {"balance": "3000000000000000000000000000"},
	  "0x1631447d041f929595a9c7b0c9c0047de2e76186": {"balance": "1000"}
}`

const wanchainPPOWDevAllocJson = `{
	  "0x2d0e7c0813a51d3bd1d08246af2a8a7a57d8922e": {"balance": "1000000000000000000000"},
	  "0x8b179c2b542f47bb2fb2dc40a3cf648aaae1df16": {"balance": "1000000000000000000000"},
	  "0x7a22d4e2dc5c135c4e8a7554157446e206835b05": {"balance": "3000000000000000000000000000"}
}`

const kimAllocJson = `{
		"0x0100a3158C780c27dE0FB194711A40CB42A60195C61a61B9051C8f6451d21F37": {"balance": "21000000000000000000000000"},
		"0x01014FBf7e9b07c2Ff3e02452f31A6ab9c5A02E16206217CaaA1df46ce0F301E": {"balance": "21000000000000000000000000"},
		"0x010288ac9294d44dAf4D210F9985251111Fba388b080862fd83037bfE03fB94f": {"balance": "21000000000000000000000000"},
		"0x010302c233469E9e0780AEc1922cc8D9810A79CCfA25a936879d19C044a903cD": {"balance": "21000000000000000000000000"},
		"0x0104DaCAC69CbE72f12042714397f8da7Aa3Af5F5F0C13bacC81686C65CDa46C": {"balance": "21000000000000000000000000"}
}`

const kimTestnetAllocJson = `{
		"0x01006a9C2c447ceD84F05402d21E1E1cB9b27e1F2ec82ea1f6c124821a32235B": {"balance": "21000000000000000000000000"}
		"0x01013ed72bcb0d961d0057B4D6d6a6Cd245114969B14538C9c6c6b3C14646c84": {"balance": "21000000000000000000000000"}
		"0x01022c08a0f08702cd84d53CAAb292a50a694a007f90CE038Ea0215fb4dB0039": {"balance": "21000000000000000000000000"}
		"0x0103e679EE4C3a408C723659F426CaD649C5f3800776bd166FF5218fE8B4F814": {"balance": "21000000000000000000000000"}
		"0x0104415c5F43E80F4d5f33E8ad95539b6E14F9d5ca85FD944c9d8558A1Fe771f": {"balance": "21000000000000000000000000"}
}`

const kimRinkebyAllocJson = `{
		"0x0100d282824Fc91d5e17bED49D2D78867E2D1B0cc4Cf9529027026f0D615126a": {"balance": "21000000000000000000000000"}
		"0x010103746C39777D74AEf451DE592ce5b2fbE44b15F680Fde256d8d2f1d8A646": {"balance": "21000000000000000000000000"}
		"0x0102b776D45600978f0e100a371c3d0842d8709c8484652aa8Eee82fbB71E48a": {"balance": "21000000000000000000000000"}
		"0x01039d302461197Ae8a0cB420507a2154c91a3A8D96CbC7FFDB1fF5CA9EC7E06": {"balance": "21000000000000000000000000"}
		"0x01049CbC4ACD62f3e2dD67fD59aeB33ad7136693D74afE449E89019ffAA7CA0D": {"balance": "21000000000000000000000000"}
}`
