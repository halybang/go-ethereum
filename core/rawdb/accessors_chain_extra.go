// Copyright 2018 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package rawdb

import (
	"bytes"
	//"fmt"
	//"encoding/binary"
	//"math/big"

	"github.com/ethereum/go-ethereum/common"
	//"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/log"
	"github.com/ethereum/go-ethereum/rlp"
)

// ReadTopHeader retrieves the top genesis block header.
func ReadTopHeader(db DatabaseReader) *types.Header {
	data, _ := db.Get(genesisHeaderKey)
	if len(data) == 0 {
		return nil
	}
	header := new(types.Header)
	if err := rlp.Decode(bytes.NewReader(data), header); err != nil {
		log.Error("Invalid top genesis block header RLP", "err", err)
		return nil
	}
	return header
}

// ReadTopBlock retrieves the top genesis block (top of all sub chain).
func ReadTopBlock(db DatabaseReader) *types.Block {
	header := ReadTopHeader(db)
	if header == nil {
		return nil
	}
	data, _ := db.Get(genesisBodyKey)
	if len(data) == 0 {
		return nil
	}
	body := new(types.Body)
	if err := rlp.Decode(bytes.NewReader(data), body); err != nil {
		log.Error("Invalid top genesis block body RLP", "err", err)
		return nil
	}
	return types.NewBlockWithHeader(header).WithBody(body.Transactions, body.Uncles)
}

// WriteTopHeader stores top genesis block header into the database and also stores all transactions
func WriteTopHeader(db DatabaseWriter, header *types.Header) {
	// Write the encoded header
	data, err := rlp.EncodeToBytes(header)
	if err != nil {
		log.Crit("Failed to RLP encode header", "err", err)
	}
	key := genesisHeaderKey
	if err := db.Put(key, data); err != nil {
		log.Crit("Failed to store header", "err", err)
	}
}

func WriteTopBlock(db DatabaseWriter, block *types.Block) {
	WriteTopHeader(db, block.Header())
	data, err := rlp.EncodeToBytes(block.Body())
	if err != nil {
		log.Crit("Failed to RLP encode top genesis body", "err", err)
	}
	if err := db.Put(genesisBodyKey, data); err != nil {
		log.Crit("Failed to top genesis body", "err", err)
	}
}

// HasHeader verifies the existence of an account corresponding to the address.
func HasAccount(db DatabaseReader, address common.Address) bool {
	if has, err := db.Has(accountKey(address)); !has || err != nil {
		return false
	}
	return true
}

// ReadHeadAccount retrieves the lastest canonical transaction of an account.
func ReadHeadAccount(db DatabaseReader, address common.Address) *types.Transaction {
	data, _ := db.Get(accountKey(address))
	if len(data) == 0 {
		return nil
	}
	transaction := new(types.Transaction)
	if err := rlp.Decode(bytes.NewReader(data), transaction); err != nil {
		log.Error("Invalid transaction RLP", "account", address, "err", err)
		return nil
	}
	return transaction
}

// WriteHeadAccount stores the lastest canonical transaction of an account.
func WriteHeadAccount(db DatabaseWriter, address common.Address, transaction *types.Transaction) {
	data, err := rlp.EncodeToBytes(transaction)
	if err != nil {
		log.Crit("Failed to RLP encode transaction", "err", err)
	}
	if err := db.Put(accountKey(address), data); err != nil {
		log.Crit("Failed to store last account tx", "err", err)
	}
}

// Read transaction's hash of account at nonce
func ReadAccountNonce(db DatabaseReader, address common.Address, nonce uint64) common.Hash {
	data, _ := db.Get(accountWithNonceKey(address, nonce))
	if len(data) == 0 {
		return common.Hash{}
	}
	return common.BytesToHash(data)
}

// WriteAccountNonce stores the hash of the canonical transaction at nonce.
func WriteAccountNonce(db DatabaseWriter, address common.Address, nonce uint64, hash common.Hash) {
	if err := db.Put(accountWithNonceKey(address, nonce), hash.Bytes()); err != nil {
		log.Crit("Failed to store account at nonce", "err", err)
	}
}

// WriteAccountNonce stores the hash of the canonical transaction at nonce.
func DeleteAccountNonce(db DatabaseDeleter, address common.Address, nonce uint64) {
	if err := db.Delete(accountWithNonceKey(address, nonce)); err != nil {
		log.Crit("Failed to delete account at nonce", "err", err)
	}
}

// HasHeader verifies the existence of a block header corresponding to the hash.
func HasTransaction(db DatabaseReader, hash common.Hash) bool {
	if has, err := db.Has(transactionKey(hash)); !has || err != nil {
		return false
	}
	return true
}

// ReadTransactionRLP retrieves a transaction in its raw RLP database encoding.
func ReadTransactionRLP(db DatabaseReader, hash common.Hash) rlp.RawValue {
	data, _ := db.Get(transactionKey(hash))
	return data
}

// ReadRawTransaction retrieves the transaction corresponding to the hash.
func ReadRawTransaction(db DatabaseReader, hash common.Hash) *types.Transaction {
	data := ReadTransactionRLP(db, hash)
	if len(data) == 0 {
		return nil
	}
	transaction := new(types.Transaction)
	if err := rlp.Decode(bytes.NewReader(data), transaction); err != nil {
		log.Error("Invalid transaction RLP", "hash", hash, "err", err)
		return nil
	}
	return transaction
}

// WriteTransaction stores a transaction into the database and also stores the nonce-to-hash
// mapping.
func WriteTransaction(db DatabaseWriter, transaction *types.Transaction) {
	// Write the hash -> number mapping
	var (
		hash = transaction.Hash()
		//account      = transaction.Account()
		//accountNonce = transaction.Nonce()
	)

	//WriteAccountNonce(db, account, accountNonce, hash)

	// Write the encoded transaction
	data, err := rlp.EncodeToBytes(transaction)
	if err != nil {
		log.Crit("Failed to RLP encode transaction", "err", err)
	}
	key := transactionKey(hash)
	if err := db.Put(key, data); err != nil {
		log.Crit("Failed to store transaction", "err", err)
	}
}

// DeleteTransaction removes all transaction data associated with a hash.
func DeleteTransaction(db DatabaseDeleter, hash common.Hash) {
	if err := db.Delete(transactionKey(hash)); err != nil {
		log.Crit("Failed to delete transaction hash", "err", err)
	}
}

func ReadReceivedTransaction(db DatabaseReader, hash common.Hash) common.Hash {
	data, _ := db.Get(receivedTransactionKey(hash))
	if len(data) == 0 {
		return common.Hash{}
	}
	return common.BytesToHash(data)
}

// WriteReceivedTransaction mark a send transaction was poke.
func WriteReceivedTransaction(db DatabaseWriter, hash common.Hash, received common.Hash) {
	if err := db.Put(receivedTransactionKey(hash), received.Bytes()); err != nil {
		log.Crit("Failed to store received transaction", "err", err)
	}
}

// Return transaction hash of received
func ReadAccountReceivedEntry(db DatabaseReader, address common.Address, txHash common.Hash) common.Hash {
	key := accountReceivedKey(address, txHash)
	data, _ := db.Get(key)
	if len(data) == 0 {
		return common.Hash{}
	}
	return common.BytesToHash(data)
}

func WriteAccountReceivedEntry(db DatabaseWriter, address common.Address, txHash, receivedHash common.Hash) {
	key := accountReceivedKey(address, txHash)
	if err := db.Put(key, receivedHash.Bytes()); err != nil {
		log.Crit("Failed to store WriteAccountReceivedEntry", "err", err)
	}
}

/////////////////////////////////
// AccountRxLookupEntry is a positional metadata to help looking up the data content of
// a transaction in foreign chain
type AccountRxLookupEntry struct {
	Account       common.Address
	PrevBlockHash common.Hash
	BlockHash     common.Hash
	Txs           []common.Hash // All transaction in that block
}

func ReadAccountRxLookupEntry(db DatabaseReader, address common.Address, blockHash common.Hash) *AccountRxLookupEntry {
	key := accountRxLookupKey(address, blockHash)
	data, _ := db.Get(key)
	if len(data) == 0 {
		return nil
	}
	entry := new(AccountRxLookupEntry)
	if err := rlp.Decode(bytes.NewReader(data), entry); err != nil {
		log.Error("Invalid RxLookupEntry RLP", "account", address.Hex(), "hash", blockHash.Hex(), "err", err)
		return nil
	}
	return entry
}

// Each block has multi lookup entry for every account
func WriteAccountRxLookupEntry(db DatabaseWriter, entry *AccountRxLookupEntry) {
	data, err := rlp.EncodeToBytes(entry)
	if err != nil {
		log.Crit("Failed to RLP encode AccountRxLookupEntry", "err", err)
		return
	}
	key := accountRxLookupKey(entry.Account, entry.BlockHash)
	if err := db.Put(key, data); err != nil {
		log.Crit("Failed to store AccountRxLookupEntry", "err", err)
	}
}

// Step:
// 1. Looking for top entry hash
// 2. Call ReadAccountRxLookupEntry for full lookup entry
func ReadAccountRxLookupTopEntry(db DatabaseReader, address common.Address) common.Hash {
	key := accountRxLookupTopKey(address)
	data, _ := db.Get(key)
	if len(data) == 0 {
		return common.Hash{}
	}
	return common.BytesToHash(data)
}

func WriteAccountRxLookupTopEntry(db DatabaseWriter, entry *AccountRxLookupEntry) {
	key := accountRxLookupTopKey(entry.Account)
	if err := db.Put(key, entry.BlockHash.Bytes()); err != nil {
		log.Crit("Failed to store WriteAccountRxLookupEntryTop", "err", err)
	}
}

// FindCommonAncestor returns the last common ancestor of two block headers
func FindAccountRxEntries(db DatabaseReader, address common.Address, blockHash common.Hash, max uint64) ([]common.Hash, common.Hash) {
	txs := make([]common.Hash, 0)
	topHash := blockHash
	if topHash == (common.Hash{}) {
		topHash = ReadAccountRxLookupTopEntry(db, address)
	}
	if topHash == (common.Hash{}) {
		return txs, common.Hash{}
	}
	for {
		entry := ReadAccountRxLookupEntry(db, address, topHash)
		if entry == nil {
			break
		}
		if len(entry.Txs) > 0 {
			txs = append(txs, entry.Txs...)
		}
		topHash = entry.PrevBlockHash
		if topHash == (common.Hash{}) {
			break
		}
		if uint64(len(txs)) > max {
			break
		}
	}
	return txs, topHash
}

// Write
// - transaction's hash 			-> transaction's body
// - account + account's nonce 	-> transaction's hash
func WriteTransactionEntries(db DatabaseReaderWriter, block *types.Block) {
	for _, tx := range block.Transactions() {
		old := ReadRawTransaction(db, tx.Hash())
		if old == nil {
			WriteTransaction(db, tx)
		}
	}
}

func WriteAccountEntries(db DatabaseReaderWriter, block *types.Block) {
	for _, tx := range block.Transactions() {
		old := ReadAccountNonce(db, tx.Account(), tx.Nonce())
		if old != tx.Hash() {
			WriteAccountNonce(db, tx.Account(), tx.Nonce(), tx.Hash())
		}
	}
}

func WriteAccountRxLookupEntries(db DatabaseWriter, block *types.Block) {
	entries := make(map[common.Address]*AccountRxLookupEntry)
	for _, tx := range block.Transactions() {
		addr := tx.Account()
		//var entry *AccountRxLookupEntry
		entry, ok := entries[addr]
		if !ok {
			entry = &AccountRxLookupEntry{}
			entries[addr] = entry
		}

		//entry.Txs
		//WriteAccountNonce(db, tx.Account(), tx.Nonce(), tx.Hash())
		//		entry := TxLookupEntry{
		//			BlockHash:  block.Hash(),
		//			BlockIndex: block.NumberU64(),
		//			Index:      uint64(i),
		//		}
		//		data, err := rlp.EncodeToBytes(entry)
		//		if err != nil {
		//			log.Crit("Failed to encode transaction lookup entry", "err", err)
		//		}
		//		if err := db.Put(txLookupKey(chainType, shard, tx.Hash()), data); err != nil {
		//			log.Crit("Failed to store transaction lookup entry", "err", err)
		//		}
	}
	for _, e := range entries {
		WriteAccountRxLookupEntry(db, e)
	}
}
