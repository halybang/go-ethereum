// Copyright 2014 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package types

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/rand"
	"encoding/json"
	"math/big"
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/crypto/ed25519"
	"github.com/ethereum/go-ethereum/rlp"
)

// The values in those tests are from the Transaction Tests
// at github.com/ethereum/tests.
var (
	emptyNext = common.Hash{} // common.HexToHash("0x0000000000000000000000000000000000000000000000000000000000000000")
	emptyLink = common.Hash{} // common.HexToHash("0x0000000000000000000000000000000000000000000000000000000000000000")
	emptyTx   = NewTransaction(
		common.HexToAddress("095e7baea6a6c7c4c2dfeb977efac326af552d87"),
		emptyNext,
		NORMAL_RX,
		emptyLink,
		big.NewInt(0), 0, big.NewInt(0),
		nil, nil,
	)

	rightvrsTx, _ = NewTransaction(
		common.HexToAddress("b94f5374fce5edbc8e2a8697c15331677e6ebf0b"),
		emptyNext,
		NORMAL_RX,
		emptyLink,
		big.NewInt(10),
		2000,
		big.NewInt(1),
		nil,
		common.FromHex("5544"),
	).WithSignature(
		HomesteadSigner{},
		common.Hex2Bytes("98ff921201554726367d2be8c804a7ff89ccf285ebc57dff8ae4c44b9c19ac4a8887321be575c8095f789dd4c743dfe42c1820f9231f98a962b210e3ac2452a301"),
	)
)

func TestTransactionSigHash(t *testing.T) {
	var homestead HomesteadSigner
	if homestead.Hash(emptyTx) != common.HexToHash("c775b99e7ad12f50d819fcd602390467e28141316969f4b57f0626f74fe3b386") {
		t.Errorf("empty transaction hash mismatch, got %x", emptyTx.Hash())
	}
	if homestead.Hash(rightvrsTx) != common.HexToHash("fe7a79529ed5f7c3375d06b26b186a8644e0e16c373d7a12be41c62d6042b77a") {
		t.Errorf("RightVRS transaction hash mismatch, got %x", rightvrsTx.Hash())
	}
}

func TestTransactionEncode(t *testing.T) {
	txb, err := rlp.EncodeToBytes(rightvrsTx)
	if err != nil {
		t.Fatalf("encode error: %v", err)
	}
	should := common.FromHex("f86103018207d094b94f5374fce5edbc8e2a8697c15331677e6ebf0b0a8255441ca098ff921201554726367d2be8c804a7ff89ccf285ebc57dff8ae4c44b9c19ac4aa08887321be575c8095f789dd4c743dfe42c1820f9231f98a962b210e3ac2452a3")
	if !bytes.Equal(txb, should) {
		t.Errorf("encoded RLP mismatch, got %x", txb)
	}
}

func decodeTx(data []byte) (*Transaction, error) {
	var tx Transaction
	t, err := &tx, rlp.Decode(bytes.NewReader(data), &tx)

	return t, err
}

func defaultTestKey() (*ecdsa.PrivateKey, common.Address) {
	key, _ := crypto.HexToECDSA("45a915e4d060149eb4365960e6a7a45f334393093061116b197e3240065ff2d8")
	addr := crypto.PubkeyToAddress(key.PublicKey)
	return key, addr
}

func TestRecipientEmpty(t *testing.T) {
	_, addr := defaultTestKey()
	tx, err := decodeTx(common.Hex2Bytes("f8498080808080011ca09b16de9d5bdee2cf56c28d16275a4da68cd30273e2525f3959f5d62557489921a0372ebd8fb3345f7db7b5a86d42e24d36e983e259b0664ceb8c227ec9af572f3d"))
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	from, err := Sender(HomesteadSigner{}, tx)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if addr != from {
		t.Error("derived address doesn't match")
	}
}

func TestRecipientNormal(t *testing.T) {
	_, addr := defaultTestKey()

	tx, err := decodeTx(common.Hex2Bytes("f85d80808094000000000000000000000000000000000000000080011ca0527c0d8f5c63f7b9f41324a7c8a563ee1190bcbf0dac8ab446291bdbf32f5c79a0552c4ef0a09a04395074dab9ed34d3fbfb843c2f2546cc30fe89ec143ca94ca6"))
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	from, err := Sender(HomesteadSigner{}, tx)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	if addr != from {
		t.Error("derived address doesn't match")
	}
}

// Tests that transactions can be correctly sorted according to their price in
// decreasing order, but at the same time with increasing nonces when issued by
// the same account.
func TestTransactionPriceNonceSort(t *testing.T) {
	// Generate a batch of accounts to start with
	keys := make([]*ecdsa.PrivateKey, 25)
	for i := 0; i < len(keys); i++ {
		keys[i], _ = crypto.GenerateKey()
	}

	signer := HomesteadSigner{}
	// Generate a batch of transactions with overlapping values, but shifted nonces
	groups := map[common.Address]Transactions{}
	for start, key := range keys {
		addr := crypto.PubkeyToAddress(key.PublicKey)
		for i := 0; i < 25; i++ {
			tx, _ := SignTx(NewTransaction(common.Address{}, emptyNext, NORMAL_RX, emptyLink, big.NewInt(100), 100, big.NewInt(int64(start+i)), nil, nil), signer, key)
			groups[addr] = append(groups[addr], tx)
		}
	}
	// Sort the transactions and cross check the nonce ordering
	txset := NewTransactionsByPriceAndNonce(signer, groups)

	txs := Transactions{}
	for tx := txset.Peek(); tx != nil; tx = txset.Peek() {
		txs = append(txs, tx)
		txset.Shift()
	}
	if len(txs) != 25*25 {
		t.Errorf("expected %d transactions, found %d", 25*25, len(txs))
	}
	for i, txi := range txs {
		fromi, _ := Sender(signer, txi)

		// Make sure the nonce order is valid
		for j, txj := range txs[i+1:] {
			fromj, _ := Sender(signer, txj)

			if fromi == fromj && txi.Nonce() > txj.Nonce() {
				t.Errorf("invalid nonce ordering: tx #%d (A=%x N=%v) < tx #%d (A=%x N=%v)", i, fromi[:4], txi.Nonce(), i+j, fromj[:4], txj.Nonce())
			}
		}

		// If the next tx has different from account, the price must be lower than the current one
		if i+1 < len(txs) {
			next := txs[i+1]
			fromNext, _ := Sender(signer, next)
			if fromi != fromNext && txi.GasPrice().Cmp(next.GasPrice()) < 0 {
				t.Errorf("invalid gasprice ordering: tx #%d (A=%x P=%v) < tx #%d (A=%x P=%v)", i, fromi[:4], txi.GasPrice(), i+1, fromNext[:4], next.GasPrice())
			}
		}
	}
}

// TestTransactionJSON tests serializing/de-serializing to/from JSON.
func TestTransactionJSON(t *testing.T) {
	key1, err := crypto.GenerateKey()
	if err != nil {
		t.Fatalf("could not generate key: %v", err)
	}
	strKey1 := hexutil.Encode(crypto.FromECDSA(key1))
	t.Log("Key1:", strKey1)
	//keyTmp, err := crypto.HexToECDSA(strKey1)
	keyTmp, err := crypto.HexToECDSA("51b762ea0ecb266528e37099e1fbb5be9a52655c352127bdfd14bafe9eb6aa30")
	if err != nil {
		t.Log("Key1 not match", strKey1, err)
	} else {
		t.Log("Key 1 Decode:", hexutil.Encode(crypto.FromECDSA(keyTmp)))
	}
	addr1 := crypto.PubkeyToAddress(key1.PublicKey)

	_, edPrivateKey1, _ := ed25519.GenerateKey(rand.Reader)
	edPublicKey2, edPrivateKey2, _ := ed25519.GenerateKey(rand.Reader)

	subKey1 := *edPrivateKey1.SubKey(1)
	subPub1 := subKey1.PublicKey()

	t.Log("EDKey1", hexutil.Encode(edPrivateKey1.Seed()))
	t.Log("SubEDKey1", hexutil.Encode(subKey1.Seed()))
	t.Log("SubPub1", hexutil.Encode(subKey1.Seed()))

	key2, err := crypto.GenerateKey()
	if err != nil {
		t.Fatalf("could not generate key: %v", err)
	}
	addr2 := crypto.PubkeyToAddress(key2.PublicKey)

	signer := NewEIP155Signer(common.Big1)
	//signer2 := NewEIP155Signer(common.Big1)

	var txGenesis *Transaction // Genesis
	var txLastAcc1 *Transaction
	var txLastAcc2 *Transaction
	var txHeader TxHeader
	txHeader.Type = NORMAL_TX
	t.Log("txHeader Size", txHeader.Size())

	for i := uint64(0); i < 25; i++ {
		//isGenesis := false
		t.Log("BEGIN ====================: ", i)
		var tx *Transaction
		next := common.Hash{byte(i + 100)}
		switch i % 5 {
		case 0:
			// Create Genesis Alloc for add 1
			tx = newGenesisTransaction(&addr1, big.NewInt(0x2048), 1, common.Big1)
			txGenesis = tx
			//isGenesis = true
		case 1:
			// Account 1 receive
			hash := txGenesis.Hash()
			//tx = NewTransaction(addr1, common.BytesToHash(edPublicKey1.Address()), NORMAL_RX, hash, txGenesis.Value(), 1, common.Big1, []byte("privateData"), []byte("abcdef"))
			tx = NewTransaction(addr1, common.BytesToHash(subPub1.Address()), NORMAL_RX, hash, txGenesis.Value(), 1, common.Big1, []byte("privateData"), []byte("abcdef"))
			tx, err = SignTx(tx, signer, key1)
			if err != nil {
				t.Fatalf("could not sign transaction: %v", err)
			}
			if err = tx.ValidateSignatureValues(); err != nil {
				t.Errorf("Invalid Signature: %v", err)
			}
			txLastAcc1 = tx
		case 2:
			// Account 1 send to Account 2
			newSubkey := edPrivateKey1.SubKey(txLastAcc1.data.AccountNonce + 1)
			newSubPub := newSubkey.PublicKey()
			tx = NextTransaction(txLastAcc1, common.BytesToHash(newSubPub.Address()), NORMAL_TX, addr2.Hash(), big.NewInt(0x1024), big.NewInt(0x1024), 1, common.Big1, []byte("sendTx"), []byte("privateValueX"), []byte("abcdef"))
			tx, _ = PreSignTx(tx, subKey1)
			tx, err = SignTx(tx, signer, key1)
			if err != nil {
				t.Fatalf("could not sign transaction: %v", err)
			}

			if edAddr, err := PreVerifyTx(tx, tx.data.QR, tx.data.QS); err != nil {
				t.Errorf("Invalid PrevSignature: %v %s", err, edAddr.Hex())
			}
			if err = tx.ValidateSignatureValues(); err != nil {
				t.Errorf("Invalid Signature: %v", err)
			}
			txLastAcc1 = tx
		case 3:
			// Account 2 receive
			hash := txLastAcc1.Hash()
			tx = NewTransaction(addr2, common.BytesToHash(edPublicKey2.Address()) /**/, NORMAL_RX, hash, txLastAcc1.Value(), 1, common.Big1, []byte("privateData"), []byte("abcdef"))
			tx, err = SignTx(tx, signer, key2)
			if err != nil {
				t.Fatalf("could not sign transaction: %v", err)
			}
			if err = tx.ValidateSignatureValues(); err != nil {
				t.Errorf("Invalid Signature: %v", err)
			}
			txLastAcc2 = tx
		case 4:
			// Account 2 send back to Account
			tx = NextTransaction(txLastAcc2, next, NORMAL_TX, addr1.Hash(), big.NewInt(0x512), big.NewInt(0x512), 1, common.Big1, []byte("sendTx"), []byte("privateValueX"), []byte("abcdef"))
			tx, _ = PreSignTx(tx, edPrivateKey2)
			tx, err = SignTx(tx, signer, key2)
			if err != nil {
				t.Fatalf("could not sign transaction: %v", err)
			}
			if addr, err := PreVerifyTx(tx, tx.data.QR, tx.data.QS); err != nil {
				t.Errorf("Invalid PrevSignature: %v %v", err, addr)
			}
			if err = tx.ValidateSignatureValues(); err != nil {
				t.Errorf("Invalid Signature: %v", err)
			}
			txLastAcc2 = tx
		}
		if tx == nil {
			continue
		}

		data, err := json.Marshal(tx)
		if err != nil {
			t.Errorf("json.Marshal failed: %v", err)
		} else {
			t.Log(i, tx.Size())
			t.Log(string(data))
			t.Log(tx.Hash().Hex())
		}

		var parsedTx *Transaction
		if err := json.Unmarshal(data, &parsedTx); err != nil {
			t.Errorf("json.Unmarshal failed: %v", err)
		}

		// compare nonce, price, gaslimit, recipient, amount, payload, V, R, S
		if tx.Hash() != parsedTx.Hash() {
			t.Errorf("parsed tx differs from original tx, want %v, got %v", tx, parsedTx)
		}
		if tx.ChainId().Cmp(parsedTx.ChainId()) != 0 {
			t.Errorf("invalid chain id, want %d, got %d", tx.ChainId(), parsedTx.ChainId())
		}
		t.Log("END =====================: ", i)
	}
}
