package vm

import (
	"crypto/ecdsa"
	//"crypto/sha256"
	"errors"
	"fmt"
	"math/big"
	"strings"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/keystore"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	//"github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	//"github.com/ethereum/go-ethereum/crypto/bn256"
	"github.com/ethereum/go-ethereum/log"
	"github.com/ethereum/go-ethereum/params"
	//"golang.org/x/crypto/ripemd160"
)

var (
	// invalid ring signed info
	ErrInvalidRingSigned = errors.New("invalid ring signed info")
)

func (c *ecrecover) ValidTx(stateDB StateDB, signer types.Signer, tx *types.Transaction) error {
	return nil
}

func (c *sha256hash) ValidTx(stateDB StateDB, signer types.Signer, tx *types.Transaction) error {
	return nil
}

func (c *ripemd160hash) ValidTx(stateDB StateDB, signer types.Signer, tx *types.Transaction) error {
	return nil
}

func (c *dataCopy) ValidTx(stateDB StateDB, signer types.Signer, tx *types.Transaction) error {
	return nil
}

func (c *bigModExp) ValidTx(stateDB StateDB, signer types.Signer, tx *types.Transaction) error {
	return nil
}

func (c *bn256Add) ValidTx(stateDB StateDB, signer types.Signer, tx *types.Transaction) error {
	return nil
}

func (c *bn256ScalarMul) ValidTx(stateDB StateDB, signer types.Signer, tx *types.Transaction) error {
	return nil
}

func (c *bn256Pairing) ValidTx(stateDB StateDB, signer types.Signer, tx *types.Transaction) error {
	return nil
}

func (c *peeridrecover) ValidTx(stateDB StateDB, signer types.Signer, tx *types.Transaction) error {
	return nil
}

func (c *ecrecoverByPublicKey) ValidTx(stateDB StateDB, signer types.Signer, tx *types.Transaction) error {
	return nil
}

///////////////////////for wan privacy tx /////////////////////////////////////////////////////////

var (
	coinSCDefinition = `
	[{"constant": false,"type": "function","stateMutability": "nonpayable","inputs": [{"name": "OtaAddr","type":"string"},{"name": "Value","type": "uint256"}],"name": "buyCoinNote","outputs": [{"name": "OtaAddr","type":"string"},{"name": "Value","type": "uint256"}]},{"constant": false,"type": "function","inputs": [{"name":"RingSignedData","type": "string"},{"name": "Value","type": "uint256"}],"name": "refundCoin","outputs": [{"name": "RingSignedData","type": "string"},{"name": "Value","type": "uint256"}]},{"constant": false,"type": "function","stateMutability": "nonpayable","inputs": [],"name": "getCoins","outputs": [{"name":"Value","type": "uint256"}]}]`

	stampSCDefinition = `[{"constant": false,"type": "function","stateMutability": "nonpayable","inputs": [{"name":"OtaAddr","type": "string"},{"name": "Value","type": "uint256"}],"name": "buyStamp","outputs": [{"name": "OtaAddr","type": "string"},{"name": "Value","type": "uint256"}]},{"constant": false,"type": "function","inputs": [{"name": "RingSignedData","type": "string"},{"name": "Value","type": "uint256"}],"name": "refundCoin","outputs": [{"name": "RingSignedData","type": "string"},{"name": "Value","type": "uint256"}]},{"constant": false,"type": "function","stateMutability": "nonpayable","inputs": [],"name": "getCoins","outputs": [{"name": "Value","type": "uint256"}]}]`

	coinAbi, errCoinSCInit               = abi.JSON(strings.NewReader(coinSCDefinition))
	buyIdArr, refundIdArr, getCoinsIdArr [4]byte

	stampAbi, errStampSCInit = abi.JSON(strings.NewReader(stampSCDefinition))
	stBuyId                  [4]byte

	errBuyCoin    = errors.New("error in buy coin")
	errRefundCoin = errors.New("error in refund coin")

	errBuyStamp = errors.New("error in buy stamp")

	errParameters = errors.New("error parameters")
	errMethodId   = errors.New("error method id")

	errBalance = errors.New("balance is insufficient")

	errStampValue = errors.New("stamp value is not support")

	errCoinValue = errors.New("wancoin value is not support")

	ErrMismatchedValue = errors.New("mismatched wancoin value")

	ErrInvalidOTASet = errors.New("invalid OTA mix set")

	ErrOTAReused = errors.New("OTA is reused")

	StampValueSet   = make(map[string]string, 5)
	WanCoinValueSet = make(map[string]string, 10)
)

const (
	Wancoin10  = "10000000000000000000"  //10
	Wancoin20  = "20000000000000000000"  //20
	Wancoin50  = "50000000000000000000"  //50
	Wancoin100 = "100000000000000000000" //100

	Wancoin200   = "200000000000000000000"   //200
	Wancoin500   = "500000000000000000000"   //500
	Wancoin1000  = "1000000000000000000000"  //1000
	Wancoin5000  = "5000000000000000000000"  //5000
	Wancoin50000 = "50000000000000000000000" //50000

	WanStampdot001 = "1000000000000000" //0.001
	WanStampdot002 = "2000000000000000" //0.002
	WanStampdot005 = "5000000000000000" //0.005

	WanStampdot003 = "3000000000000000" //0.003
	WanStampdot006 = "6000000000000000" //0.006
	WanStampdot009 = "9000000000000000" //0.009

	WanStampdot03 = "30000000000000000"  //0.03
	WanStampdot06 = "60000000000000000"  //0.06
	WanStampdot09 = "90000000000000000"  //0.09
	WanStampdot2  = "200000000000000000" //0.2
	WanStampdot3  = "300000000000000000" //0.3
	WanStampdot5  = "500000000000000000" //0.5

)

var (
	ecrecoverPrecompileAddr      = common.BytesToAddress([]byte{1})
	sha256hashPrecompileAddr     = common.BytesToAddress([]byte{2})
	ripemd160hashPrecompileAddr  = common.BytesToAddress([]byte{3})
	dataCopyPrecompileAddr       = common.BytesToAddress([]byte{4})
	bigModExpPrecompileAddr      = common.BytesToAddress([]byte{5})
	bn256AddPrecompileAddr       = common.BytesToAddress([]byte{6})
	bn256ScalarMulPrecompileAddr = common.BytesToAddress([]byte{7})
	bn256PairingPrecompileAddr   = common.BytesToAddress([]byte{8})

	wanCoinPrecompileAddr  = common.BytesToAddress([]byte{100})
	wanStampPrecompileAddr = common.BytesToAddress([]byte{200})

	//otaBalanceStorageAddr = common.BytesToAddress(big.NewInt(300).Bytes())
	//otaImageStorageAddr   = common.BytesToAddress(big.NewInt(301).Bytes())

	//MasterndeContractAddress
	//GovernanceContractAddress

	// 0.01wan --> "0x0000000000000000000000010000000000000000"
	otaBalancePercentdot001WStorageAddr = common.HexToAddress(WanStampdot001)
	otaBalancePercentdot002WStorageAddr = common.HexToAddress(WanStampdot002)
	otaBalancePercentdot005WStorageAddr = common.HexToAddress(WanStampdot005)

	otaBalancePercentdot003WStorageAddr = common.HexToAddress(WanStampdot003)
	otaBalancePercentdot006WStorageAddr = common.HexToAddress(WanStampdot006)
	otaBalancePercentdot009WStorageAddr = common.HexToAddress(WanStampdot009)

	otaBalancePercentdot03WStorageAddr = common.HexToAddress(WanStampdot03)
	otaBalancePercentdot06WStorageAddr = common.HexToAddress(WanStampdot06)
	otaBalancePercentdot09WStorageAddr = common.HexToAddress(WanStampdot09)
	otaBalancePercentdot2WStorageAddr  = common.HexToAddress(WanStampdot2)
	otaBalancePercentdot5WStorageAddr  = common.HexToAddress(WanStampdot5)

	otaBalance10WStorageAddr  = common.HexToAddress(Wancoin10)
	otaBalance20WStorageAddr  = common.HexToAddress(Wancoin20)
	otaBalance50WStorageAddr  = common.HexToAddress(Wancoin50)
	otaBalance100WStorageAddr = common.HexToAddress(Wancoin100)

	otaBalance200WStorageAddr   = common.HexToAddress(Wancoin200)
	otaBalance500WStorageAddr   = common.HexToAddress(Wancoin500)
	otaBalance1000WStorageAddr  = common.HexToAddress(Wancoin1000)
	otaBalance5000WStorageAddr  = common.HexToAddress(Wancoin5000)
	otaBalance50000WStorageAddr = common.HexToAddress(Wancoin50000)
)

func init() {
	if errCoinSCInit != nil || errStampSCInit != nil {
		panic("err in coin sc initialize or stamp error initialize ")
	}

	copy(buyIdArr[:], coinAbi.Methods["buyCoinNote"].Id())
	copy(refundIdArr[:], coinAbi.Methods["refundCoin"].Id())
	copy(getCoinsIdArr[:], coinAbi.Methods["getCoins"].Id())

	copy(stBuyId[:], stampAbi.Methods["buyStamp"].Id())

	svaldot001, _ := new(big.Int).SetString(WanStampdot001, 10)
	StampValueSet[svaldot001.Text(16)] = WanStampdot001

	svaldot002, _ := new(big.Int).SetString(WanStampdot002, 10)
	StampValueSet[svaldot002.Text(16)] = WanStampdot002

	svaldot005, _ := new(big.Int).SetString(WanStampdot005, 10)
	StampValueSet[svaldot005.Text(16)] = WanStampdot005

	svaldot003, _ := new(big.Int).SetString(WanStampdot003, 10)
	StampValueSet[svaldot003.Text(16)] = WanStampdot003

	svaldot006, _ := new(big.Int).SetString(WanStampdot006, 10)
	StampValueSet[svaldot006.Text(16)] = WanStampdot006

	svaldot009, _ := new(big.Int).SetString(WanStampdot009, 10)
	StampValueSet[svaldot009.Text(16)] = WanStampdot009

	svaldot03, _ := new(big.Int).SetString(WanStampdot03, 10)
	StampValueSet[svaldot03.Text(16)] = WanStampdot03

	svaldot06, _ := new(big.Int).SetString(WanStampdot06, 10)
	StampValueSet[svaldot06.Text(16)] = WanStampdot06

	svaldot09, _ := new(big.Int).SetString(WanStampdot09, 10)
	StampValueSet[svaldot09.Text(16)] = WanStampdot09

	svaldot2, _ := new(big.Int).SetString(WanStampdot2, 10)
	StampValueSet[svaldot2.Text(16)] = WanStampdot2

	svaldot3, _ := new(big.Int).SetString(WanStampdot3, 10)
	StampValueSet[svaldot3.Text(16)] = WanStampdot3

	svaldot5, _ := new(big.Int).SetString(WanStampdot5, 10)
	StampValueSet[svaldot5.Text(16)] = WanStampdot5

	cval10, _ := new(big.Int).SetString(Wancoin10, 10)
	WanCoinValueSet[cval10.Text(16)] = Wancoin10

	cval20, _ := new(big.Int).SetString(Wancoin20, 10)
	WanCoinValueSet[cval20.Text(16)] = Wancoin20

	cval50, _ := new(big.Int).SetString(Wancoin50, 10)
	WanCoinValueSet[cval50.Text(16)] = Wancoin50

	cval100, _ := new(big.Int).SetString(Wancoin100, 10)
	WanCoinValueSet[cval100.Text(16)] = Wancoin100

	cval200, _ := new(big.Int).SetString(Wancoin200, 10)
	WanCoinValueSet[cval200.Text(16)] = Wancoin200

	cval500, _ := new(big.Int).SetString(Wancoin500, 10)
	WanCoinValueSet[cval500.Text(16)] = Wancoin500

	cval1000, _ := new(big.Int).SetString(Wancoin1000, 10)
	WanCoinValueSet[cval1000.Text(16)] = Wancoin1000

	cval5000, _ := new(big.Int).SetString(Wancoin5000, 10)
	WanCoinValueSet[cval5000.Text(16)] = Wancoin5000

	cval50000, _ := new(big.Int).SetString(Wancoin50000, 10)
	WanCoinValueSet[cval50000.Text(16)] = Wancoin50000

}

type wanchainStampSC struct{}

func (c *wanchainStampSC) RequiredGas(input []byte) uint64 {
	// ota balance store gas + ota wanaddr store gas
	return params.SstoreSetGas * 2
}

func (c *wanchainStampSC) Run(in []byte, contract *Contract, env *EVM) ([]byte, error) {
	if len(in) < 4 {
		return nil, errParameters
	}

	var methodId [4]byte
	copy(methodId[:], in[:4])

	if methodId == stBuyId {
		return c.buyStamp(in[4:], contract, env)
	}

	return nil, errMethodId
}

func (c *wanchainStampSC) ValidTx(stateDB StateDB, signer types.Signer, tx *types.Transaction) error {
	if stateDB == nil || signer == nil || tx == nil {
		return errParameters
	}

	payload := tx.Payload()
	if len(payload) < 4 {
		return errParameters
	}

	var methodId [4]byte
	copy(methodId[:], payload[:4])
	if methodId == stBuyId {
		_, err := c.ValidBuyStampReq(stateDB, payload[4:], tx.Value())
		return err
	}

	return errParameters
}

func (c *wanchainStampSC) ValidBuyStampReq(stateDB StateDB, payload []byte, value *big.Int) (otaAddr []byte, err error) {
	if stateDB == nil || len(payload) == 0 || value == nil {
		return nil, errors.New("unknown error")
	}

	var StampInput struct {
		OtaAddr string
		Value   *big.Int
	}

	err = stampAbi.Unpack(&StampInput, "buyStamp", payload)
	if err != nil || StampInput.Value == nil {
		return nil, errBuyStamp
	}

	if StampInput.Value.Cmp(value) != 0 {
		return nil, ErrMismatchedValue
	}

	_, ok := StampValueSet[StampInput.Value.Text(16)]
	if !ok {
		return nil, errStampValue
	}

	wanAddr, err := hexutil.Decode(StampInput.OtaAddr)
	if err != nil {
		return nil, err
	}

	ax, err := GetAXFromWanAddr(wanAddr)
	exist, _, err := CheckOTAAXExist(stateDB, ax)
	if err != nil {
		return nil, err
	}

	if exist {
		return nil, ErrOTAReused
	}

	return wanAddr, nil
}

func (c *wanchainStampSC) buyStamp(in []byte, contract *Contract, evm *EVM) ([]byte, error) {
	wanAddr, err := c.ValidBuyStampReq(evm.StateDB, in, contract.value)
	if err != nil {
		return nil, err
	}

	add, err := AddOTAIfNotExist(evm.StateDB, contract.value, wanAddr)
	if err != nil || !add {
		return nil, errBuyStamp
	}

	addrSrc := contract.CallerAddress
	balance := evm.StateDB.GetBalance(addrSrc)

	if balance.Cmp(contract.value) >= 0 {
		// Need check contract value in  build in value sets
		evm.StateDB.SubBalance(addrSrc, contract.value)
		return []byte{1}, nil
	} else {
		return nil, errBalance
	}
}

type wanCoinSC struct {
}

func (c *wanCoinSC) RequiredGas(input []byte) uint64 {
	if len(input) < 4 {
		return 0
	}

	var methodIdArr [4]byte
	copy(methodIdArr[:], input[:4])

	if methodIdArr == refundIdArr {

		var RefundStruct struct {
			RingSignedData string
			Value          *big.Int
		}

		err := coinAbi.Unpack(&RefundStruct, "refundCoin", input[4:])
		if err != nil {
			return params.RequiredGasPerMixPub
		}

		err, publickeys, _, _, _ := DecodeRingSignOut(RefundStruct.RingSignedData)
		if err != nil {
			return params.RequiredGasPerMixPub
		}

		mixLen := len(publickeys)
		ringSigDiffRequiredGas := params.RequiredGasPerMixPub * (uint64(mixLen))

		// ringsign compute gas + ota image key store setting gas
		return ringSigDiffRequiredGas + params.SstoreSetGas

	} else {
		// ota balance store gas + ota wanaddr store gas
		return params.SstoreSetGas * 2
	}

}

func (c *wanCoinSC) Run(in []byte, contract *Contract, evm *EVM) ([]byte, error) {
	if len(in) < 4 {
		return nil, errParameters
	}

	var methodIdArr [4]byte
	copy(methodIdArr[:], in[:4])

	if methodIdArr == buyIdArr {
		return c.buyCoin(in[4:], contract, evm)
	} else if methodIdArr == refundIdArr {
		return c.refund(in[4:], contract, evm)
	}

	return nil, errMethodId
}

func (c *wanCoinSC) ValidTx(stateDB StateDB, signer types.Signer, tx *types.Transaction) error {
	if stateDB == nil || signer == nil || tx == nil {
		return errParameters
	}

	payload := tx.Payload()
	if len(payload) < 4 {
		return errParameters
	}

	var methodIdArr [4]byte
	copy(methodIdArr[:], payload[:4])

	if methodIdArr == buyIdArr {
		_, err := c.ValidBuyCoinReq(stateDB, payload[4:], tx.Value())
		return err

	} else if methodIdArr == refundIdArr {
		from, err := types.Sender(signer, tx)
		if err != nil {
			return err
		}

		_, _, err = c.ValidRefundReq(stateDB, payload[4:], from.Bytes())
		return err
	}

	return errParameters
}

var (
	ether = new(big.Int).Exp(big.NewInt(10), big.NewInt(18), nil)
)

func (c *wanCoinSC) ValidBuyCoinReq(stateDB StateDB, payload []byte, txValue *big.Int) (otaAddr []byte, err error) {
	if stateDB == nil || len(payload) == 0 || txValue == nil {
		return nil, errors.New("unknown error")
	}

	var outStruct struct {
		OtaAddr string
		Value   *big.Int
	}

	err = coinAbi.Unpack(&outStruct, "buyCoinNote", payload)
	if err != nil || outStruct.Value == nil {
		return nil, errBuyCoin
	}

	if outStruct.Value.Cmp(txValue) != 0 {
		return nil, ErrMismatchedValue
	}

	_, ok := WanCoinValueSet[outStruct.Value.Text(16)]
	if !ok {
		return nil, errCoinValue
	}

	wanAddr, err := hexutil.Decode(outStruct.OtaAddr)
	if err != nil {
		return nil, err
	}

	ax, err := GetAXFromWanAddr(wanAddr)
	if err != nil {
		return nil, err
	}

	exist, _, err := CheckOTAAXExist(stateDB, ax)
	if err != nil {
		return nil, err
	}

	if exist {
		return nil, ErrOTAReused
	}

	return wanAddr, nil
}

func (c *wanCoinSC) buyCoin(in []byte, contract *Contract, evm *EVM) ([]byte, error) {
	otaAddr, err := c.ValidBuyCoinReq(evm.StateDB, in, contract.value)
	if err != nil {
		return nil, err
	}

	add, err := AddOTAIfNotExist(evm.StateDB, contract.value, otaAddr)
	if err != nil || !add {
		return nil, errBuyCoin
	}

	addrSrc := contract.CallerAddress
	balance := evm.StateDB.GetBalance(addrSrc)

	if balance.Cmp(contract.value) >= 0 {
		// Need check contract value in  build in value sets
		evm.StateDB.SubBalance(addrSrc, contract.value)
		return []byte{1}, nil
	} else {
		return nil, errBalance
	}
}

func (c *wanCoinSC) ValidRefundReq(stateDB StateDB, payload []byte, from []byte) (image []byte, value *big.Int, err error) {
	if stateDB == nil || len(payload) == 0 || len(from) == 0 {
		return nil, nil, errors.New("unknown error")
	}

	var RefundStruct struct {
		RingSignedData string
		Value          *big.Int
	}

	err = coinAbi.Unpack(&RefundStruct, "refundCoin", payload)
	if err != nil || RefundStruct.Value == nil {
		return nil, nil, errRefundCoin
	}

	ringSignInfo, err := FetchRingSignInfo(stateDB, from, RefundStruct.RingSignedData)
	if err != nil {
		return nil, nil, err
	}

	if ringSignInfo.OTABalance.Cmp(RefundStruct.Value) != 0 {
		return nil, nil, ErrMismatchedValue
	}

	kix := crypto.FromECDSAPub(ringSignInfo.KeyImage)
	exist, _, err := CheckOTAImageExist(stateDB, kix)
	if err != nil {
		return nil, nil, err
	}

	if exist {
		return nil, nil, ErrOTAReused
	}

	return kix, RefundStruct.Value, nil

}

func (c *wanCoinSC) refund(all []byte, contract *Contract, evm *EVM) ([]byte, error) {
	kix, value, err := c.ValidRefundReq(evm.StateDB, all, contract.CallerAddress.Bytes())
	if err != nil {
		fmt.Println("failed refund")
		fmt.Println(evm.BlockNumber)
		return nil, err
	}

	err = AddOTAImage(evm.StateDB, kix, value.Bytes())
	if err != nil {
		return nil, err
	}

	addrSrc := contract.CallerAddress
	evm.StateDB.AddBalance(addrSrc, value)
	return []byte{1}, nil

}

func DecodeRingSignOut(s string) (error, []*ecdsa.PublicKey, *ecdsa.PublicKey, []*big.Int, []*big.Int) {
	ss := strings.Split(s, "+")
	if len(ss) < 4 {
		return ErrInvalidRingSigned, nil, nil, nil, nil
	}

	ps := ss[0]
	k := ss[1]
	ws := ss[2]
	qs := ss[3]

	pa := strings.Split(ps, "&")
	publickeys := make([]*ecdsa.PublicKey, 0)
	for _, pi := range pa {

		publickey, _ := crypto.UnmarshalPubkey(common.FromHex(pi))
		if publickey == nil || publickey.X == nil || publickey.Y == nil {
			return ErrInvalidRingSigned, nil, nil, nil, nil
		}

		publickeys = append(publickeys, publickey)
	}

	keyimgae, _ := crypto.UnmarshalPubkey(common.FromHex(k))
	if keyimgae == nil || keyimgae.X == nil || keyimgae.Y == nil {
		return ErrInvalidRingSigned, nil, nil, nil, nil
	}

	wa := strings.Split(ws, "&")
	w := make([]*big.Int, 0)
	for _, wi := range wa {
		bi, err := hexutil.DecodeBig(wi)
		if bi == nil || err != nil {
			return ErrInvalidRingSigned, nil, nil, nil, nil
		}

		w = append(w, bi)
	}

	qa := strings.Split(qs, "&")
	q := make([]*big.Int, 0)
	for _, qi := range qa {
		bi, err := hexutil.DecodeBig(qi)
		if bi == nil || err != nil {
			return ErrInvalidRingSigned, nil, nil, nil, nil
		}

		q = append(q, bi)
	}

	if len(publickeys) != len(w) || len(publickeys) != len(q) {
		return ErrInvalidRingSigned, nil, nil, nil, nil
	}

	return nil, publickeys, keyimgae, w, q
}

type RingSignInfo struct {
	PublicKeys []*ecdsa.PublicKey
	KeyImage   *ecdsa.PublicKey
	W_Random   []*big.Int
	Q_Random   []*big.Int
	OTABalance *big.Int
}

func FetchRingSignInfo(stateDB StateDB, hashInput []byte, ringSignedStr string) (info *RingSignInfo, err error) {
	if stateDB == nil || hashInput == nil {
		return nil, errParameters
	}

	infoTmp := new(RingSignInfo)

	err, infoTmp.PublicKeys, infoTmp.KeyImage, infoTmp.W_Random, infoTmp.Q_Random = DecodeRingSignOut(ringSignedStr)
	if err != nil {
		return nil, err
	}

	otaLongs := make([][]byte, 0, len(infoTmp.PublicKeys))
	for i := 0; i < len(infoTmp.PublicKeys); i++ {
		otaLongs = append(otaLongs, keystore.ECDSAPKCompression(infoTmp.PublicKeys[i]))
	}

	exist, balanceGet, _, err := BatCheckOTAExist(stateDB, otaLongs)
	if err != nil {

		log.Error("verify mix ota fail", "err", err.Error())
		return nil, err
	}

	if !exist {
		return nil, ErrInvalidOTASet
	}

	infoTmp.OTABalance = balanceGet

	valid := crypto.VerifyRingSign(hashInput, infoTmp.PublicKeys, infoTmp.KeyImage, infoTmp.W_Random, infoTmp.Q_Random)
	if !valid {
		return nil, ErrInvalidRingSigned
	}

	return infoTmp, nil
}

func GetSupportWanCoinOTABalances() []*big.Int {
	cval10, _ := new(big.Int).SetString(Wancoin10, 10)
	cval20, _ := new(big.Int).SetString(Wancoin20, 10)
	cval50, _ := new(big.Int).SetString(Wancoin50, 10)
	cval100, _ := new(big.Int).SetString(Wancoin100, 10)

	cval200, _ := new(big.Int).SetString(Wancoin200, 10)
	cval500, _ := new(big.Int).SetString(Wancoin500, 10)
	cval1000, _ := new(big.Int).SetString(Wancoin1000, 10)
	cval5000, _ := new(big.Int).SetString(Wancoin5000, 10)
	cval50000, _ := new(big.Int).SetString(Wancoin50000, 10)

	wancoinBalances := []*big.Int{
		cval10,
		cval20,
		cval50,
		cval100,

		cval200,
		cval500,
		cval1000,
		cval5000,
		cval50000,
	}

	return wancoinBalances
}

func GetSupportStampOTABalances() []*big.Int {

	svaldot09, _ := new(big.Int).SetString(WanStampdot09, 10)
	svaldot2, _ := new(big.Int).SetString(WanStampdot2, 10)
	svaldot5, _ := new(big.Int).SetString(WanStampdot5, 10)

	stampBalances := []*big.Int{
		//svaldot03,
		//svaldot06,
		svaldot09,
		svaldot2,
		svaldot5,
	}

	return stampBalances
}
