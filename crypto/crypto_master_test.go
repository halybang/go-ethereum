package crypto

import (
	"crypto/ecdsa"
	"crypto/rand"
	"math/big"
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto/ed25519"
)

//var testAddrHex = "970e8128ab834e8eac17ab8e3812f010678cf791"
//var testPrivHex = "289c2857d4598e37fb9647507e47a309d6133539bf21a8b9cb6df88fd5232032"

func TestMasterSign(t *testing.T) {
	masterKey, _, err := GenerateMasterKey()
	//masterKey, err := crypto.HexToECDSA("455f249b823adeb2d7715a7608c8a34415767429c76fe98e17612f948c6b482b")
	if err != nil {
		t.Fatalf("could not generate key: %v", err)
	}
	signKey1 := GenerateEdDSAKey(masterKey)
	signKey2 := GenerateViewKey(masterKey)
	signKey3 := GenerateSignKey(masterKey)

	strKey1 := hexutil.Encode(signKey1.Seed())
	strKey2 := hexutil.Encode(FromECDSA(signKey2))
	strKey3 := hexutil.Encode(FromECDSA(signKey3))

	t.Log("MasterKey:", hexutil.Encode(FromECDSA(masterKey)))
	t.Log("EDDSAKey:", strKey1)
	t.Log("ViewKey:", strKey2)
	t.Log("SignKey:", strKey3)

	t.Log("NextAddress:", hexutil.Encode(signKey1.SubKey(0).Seed()))

	data := []byte("Test signed")
	r, s, err := ecdsa.Sign(rand.Reader, signKey2, data)
	if result := ecdsa.Verify(&signKey2.PublicKey, data, r, s); !result {
		t.Fatal("Verify ecdsa error:", strKey1, strKey2)
	}
	edData := ed25519.Sign(*signKey1.SubKey(0), data)
	if !ed25519.Verify((*signKey1.SubKey(0)).PublicKey(), data, edData) {
		t.Fatal("Verify EDDSA failured")
	}

	addr1 := PubkeyToAddress(signKey3.PublicKey)
	t.Log("Address1: ", hexutil.Encode(addr1.Bytes()), addr1.Shard(), VerifyAddress(addr1), addr1.Id())

	addr2 := common.Address{0x01, 0x01}
	t.Log("Address2: ", hexutil.Encode(addr2.Bytes()), addr2.Shard(), VerifyAddress(addr2), addr2.Id())

	bInt := new(big.Int).SetUint64(0x1a65)
	bIntHash := common.BigToHash(bInt)

	contractAddr1 := CreateAddress(addr1, 1)
	contractAddr2 := CreateAddress(addr2, 1)
	t.Logf("CONTRACT 1 %s %v %x %v", hexutil.Encode(contractAddr1.Bytes()), VerifyAddress(contractAddr1), contractAddr1.Id(), contractAddr1.Shard())
	t.Logf("CONTRACT 2 %s %v %x %v", hexutil.Encode(contractAddr2.Bytes()), VerifyAddress(contractAddr2), contractAddr2.Id(), contractAddr2.Shard())

	shardContract := common.IntToHash(contractAddr1.Id())

	t.Log("HASH ", hexutil.Encode(bIntHash.Bytes()), hexutil.Encode(shardContract.Bytes()))
}
