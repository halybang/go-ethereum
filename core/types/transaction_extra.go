package types

import (
	//"container/heap"
	//"errors"
	//"io"
	"math/big"
	//"sync/atomic"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	//"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/rlp"
)

//go:generate gencodec -type TxInfo -field-override TxInfoMarshaling -out gen_txinfo_json.go

// Support multi send/received
type TxInfo struct {
	Link         common.Hash `json:"link"        gencodec:"required"`    // Send: Destination Address, Received: Tx, CreateContract: ContractAddress
	Value        *big.Int    `json:"value"     gencodec:"required"`      // Amount to send
	PrivateValue []byte      `json:"privateValue"   gencodec:"required"` // Bulletproofs amount to send
}

type TxInfoMarshaling struct {
	Value        *hexutil.Big
	PrivateValue hexutil.Bytes
}

// Fast block
type TxHeader struct {
	Type         uint           `json:"type"    	gencodec:"required"`
	AccountNonce uint64         `json:"nonce"     	gencodec:"required"`
	Account      common.Address `json:"account"	gencodec:"required"`
	Link         common.Hash    `json:"link"   	gencodec:"required"` // Send: Destination Address, Received: Tx, CreateContract: ContractAddress
	Hash         common.Hash    `json:"hash"		gencodec:"required"`
}

// Size returns the true RLP encoded storage size of the transaction, either by
// encoding and returning it, or returning a previsouly cached value.
func (txHeader *TxHeader) Size() common.StorageSize {
	c := writeCounter(0)
	rlp.Encode(&c, txHeader)
	return common.StorageSize(c)
}

// Transactions is a Transaction slice type for basic sorting.
type TxHeaders []*TxHeader

// Len returns the length of s.
func (s TxHeaders) Len() int { return len(s) }

// Swap swaps the i'th and the j'th element in s.
func (s TxHeaders) Swap(i, j int) { s[i], s[j] = s[j], s[i] }

// GetRlp implements Rlpable and returns the i'th element of s in rlp.
func (s TxHeaders) GetRlp(i int) []byte {
	enc, _ := rlp.EncodeToBytes(s[i])
	return enc
}

// New Genesis transaction for genesis block
func newGenesisTransaction(account *common.Address, value *big.Int, gasLimit uint64, gasPrice *big.Int) *Transaction {
	d := TxData{
		Version:    TX_VERSION,
		Type:       GENESIS_TX,
		ParentHash: common.Hash{},    //("0x0000000000000000000000000000000000000000000000000000000000000000"),
		Account:    common.Address{}, // HexToAddress("0x0000000000000000000000000000000000000000"),
		Next:       common.Hash{},    // HexToHash("0x0000000000000000000000000000000000000000000000000000000000000000"),

		AccountNonce: 0,
		Price:        new(big.Int),
		GasLimit:     gasLimit,
		LockAmount:   new(big.Int),
		LockTime:     0,
		Balance:      new(big.Int),
		Link:         account.Hash(), //common.HexToHash("0x0000000000000000000000000000000000000000000000000000000000000000"),

		Value: new(big.Int),
		//		QR:    new(big.Int),
		//		QS:    new(big.Int),
		V: new(big.Int),
		R: new(big.Int),
		S: new(big.Int),
	}
	if value != nil {
		d.Value.Set(value)
	}
	if gasPrice != nil {
		d.Price.Set(gasPrice)
	}
	return &Transaction{data: d}
}

// Open: First receive payment
// link: Tx which send amount to account
// balance: link's Tx->Value
func newOpenTransaction(account *common.Address, next *common.Hash, txType uint, link *common.Hash, balance *big.Int, gasLimit uint64, gasPrice *big.Int, privateData []byte, payload []byte) *Transaction {
	//	if (txType != NORMAL_RX) && (txType != PRIVACY_RX) {
	//		return nil
	//	}
	if len(payload) > 0 {
		payload = common.CopyBytes(payload)
	}
	if len(privateData) > 0 {
		privateData = common.CopyBytes(privateData)
	}
	d := TxData{
		Version:    TX_VERSION,
		Type:       txType,
		ParentHash: common.Hash{}, //HexToHash("0x0000000000000000000000000000000000000000000000000000000000000000"),
		Account:    *account,      // common.HexToAddress("0x0000000000000000000000000000000000000000"),
		Next:       *next,         //common.HexToHash("0x0000000000000000000000000000000000000000000000000000000000000000"),

		AccountNonce: 0,
		Price:        new(big.Int),
		GasLimit:     gasLimit,
		LockAmount:   new(big.Int),
		LockTime:     0,
		Balance:      new(big.Int),
		PrivateData:  privateData,
		Link:         *link,

		Value: new(big.Int),

		Payload: payload,

		//		QR: new(big.Int),
		//		QS: new(big.Int),
		V: new(big.Int),
		R: new(big.Int),
		S: new(big.Int),
	}
	if balance != nil {
		d.Balance.Set(balance)
	}
	if gasPrice != nil {
		d.Price.Set(gasPrice)
	}
	return &Transaction{data: d}
}

// Create transaction for exist account
// Type send/receive/lock/unlock/secret send/secret receive/create contract
// When create contract, link is contract address.
// Contract address was choosen by owner
func nextTransaction(parent *Transaction, next *common.Hash, txType uint, link *common.Hash, balance *big.Int, value *big.Int, gasLimit uint64, gasPrice *big.Int, privateData []byte, privateValue []byte, payload []byte) *Transaction {
	if len(payload) > 0 {
		payload = common.CopyBytes(payload)
	}
	if len(privateData) > 0 {
		privateData = common.CopyBytes(privateData)
	}
	if len(privateValue) > 0 {
		privateValue = common.CopyBytes(privateValue)
	}
	d := TxData{
		Version:    TX_VERSION,
		Type:       txType,
		ParentHash: parent.Hash(),
		Account:    parent.data.Account, // common.HexToAddress("0x0000000000000000000000000000000000000000"),
		Next:       *next,

		AccountNonce: parent.data.AccountNonce + 1,
		Price:        new(big.Int),
		GasLimit:     gasLimit,
		LockAmount:   new(big.Int),
		LockTime:     0,
		Balance:      new(big.Int),
		PrivateData:  privateData,
		Link:         *link,
		Value:        new(big.Int),
		PrivateValue: privateValue,

		Payload: payload,

		//		QR: new(big.Int),
		//		QS: new(big.Int),
		V: new(big.Int),
		R: new(big.Int),
		S: new(big.Int),
	}
	if balance != nil {
		d.Balance.Set(balance)
	}
	if value != nil {
		d.Value.Set(value)
	}
	if gasPrice != nil {
		d.Price.Set(gasPrice)
	}
	return &Transaction{data: d}
}

////////////////////////////////////for privacy tx ///////////////////////
func NewOTATransaction(nonce uint64, to common.Address, amount *big.Int, gasLimit uint64, gasPrice *big.Int, data []byte) *Transaction {
	return newOTATransaction(nonce, &to, amount, gasLimit, gasPrice, data)
}

func newOTATransaction(nonce uint64, to *common.Address, amount *big.Int, gasLimit uint64, gasPrice *big.Int, data []byte) *Transaction {
	if to == nil {
		return nil
	}

	//var addressDst common.Address
	//addressDst = *to

	d := TxData{
		Type:         PRIVACY_TX,
		AccountNonce: nonce,
		//Recipient:    &addressDst,
		Payload:  data,
		Value:    new(big.Int),
		GasLimit: gasLimit,
		Price:    new(big.Int),
		V:        new(big.Int),
		R:        new(big.Int),
		S:        new(big.Int),
	}
	if amount != nil {
		d.Value.Set(amount)
	}
	//	if gasLimit != nil {
	//		d.GasLimit.Set(gasLimit)
	//	}
	if gasPrice != nil {
		d.Price.Set(gasPrice)
	}

	return &Transaction{data: d}
}
