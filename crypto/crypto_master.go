package crypto

import (
	"crypto/ecdsa"
	"encoding/binary"
	//	"crypto/elliptic"
	"crypto/rand"
	//"encoding/hex"
	"errors"
	//"fmt"
	//	"io"
	//	"io/ioutil"
	"math/big"
	//	"os"

	"github.com/ethereum/go-ethereum/common"
	//	"github.com/ethereum/go-ethereum/common/math"
	//	"github.com/ethereum/go-ethereum/crypto/sha3"
	//	"github.com/ethereum/go-ethereum/rlp"

	"github.com/ethereum/go-ethereum/crypto/ed25519"
	//"github.com/golang/crypto/blake2b"
	"golang.org/x/crypto/blake2b"
)

const (
	MAX_SHARD = 10
)

func Blake2b(data []byte, size int) []byte {
	hash, err := blake2b.New(size, nil)
	if err != nil {
		panic(err)
	}

	hash.Write(data[:])
	return hash.Sum(nil)
}

// HD Seed = Hash(Hash(private)+number)
func HDSeedKey(priv *ecdsa.PrivateKey, nonce uint64) []byte {
	bytes := FromECDSA(priv)

	enc := make([]byte, 8)
	binary.BigEndian.PutUint64(enc, nonce)

	hash, err := blake2b.New(32, nil)
	if err != nil {
		return nil
	}
	hash.Write(bytes[:])
	privHash := hash.Sum(nil)

	hash2, err := blake2b.New(32, nil)
	if err != nil {
		return nil
	}
	hash2.Write(append(privHash, enc...))
	seed := hash2.Sum(nil)
	return seed
}

func GenerateMasterKey(max uint64) (*ecdsa.PrivateKey, common.Address, error) {
	for {
		privateKey, _ := ecdsa.GenerateKey(S256(), rand.Reader)
		signKey := GenerateKeyFromSeed(HDSeedKey(privateKey, 2))
		addr := PubkeyToAddress(signKey.PublicKey)
		if addr.Shard() < max {
			return privateKey, addr, nil
		}
	}
	return nil, common.Address{}, errors.New("Can not generate master key")
}

func GenerateEdDSAKey(privateKey *ecdsa.PrivateKey) ed25519.PrivateKey {
	seed := HDSeedKey(privateKey, 0)
	return ed25519.NewKeyFromSeed(seed)
}

func GenerateViewKey(privateKey *ecdsa.PrivateKey) *ecdsa.PrivateKey {
	return GenerateKeyFromSeed(HDSeedKey(privateKey, 1))
}

func GenerateSignKey(privateKey *ecdsa.PrivateKey) *ecdsa.PrivateKey {
	return GenerateKeyFromSeed(HDSeedKey(privateKey, 2))
}

func GenerateKeyFromSeed(seed []byte) *ecdsa.PrivateKey {
	var priv *ecdsa.PrivateKey
	curve := S256()
	params := curve.Params()

	k := new(big.Int)
	b := make([]byte, params.BitSize/8+8)
	copy(b[:], seed[:])
	k.SetBytes(b)

	one := new(big.Int).SetInt64(1)
	n := new(big.Int).Sub(params.N, one)
	k.Mod(k, n)
	k.Add(k, one)

	priv = new(ecdsa.PrivateKey)

	priv.PublicKey.Curve = curve
	priv.D = k
	priv.PublicKey.X, priv.PublicKey.Y = curve.ScalarBaseMult(k.Bytes())
	return priv
}

/*
const KeyLength = 32

// Key can be a Scalar or a Point
type EdDSAKey [KeyLength]byte

type MasterKey struct {
	RootKey   ecdsa.PrivateKey
	EdKey     EdDSAKey
	EcViewKey ecdsa.PrivateKey
	EcSignKey ecdsa.PrivateKey
}

func GenerateMasterKey() (*ecdsa.PrivateKey, error) {
	//for i:=0, i < 255 {
	key := ecdsa.GenerateKey(S256(), rand.Reader)
	//}
}

// this uses random number generator from the OS
func RandomScalar() (result *EdDSAKey) {
	result = new(EdDSAKey)
	var reduceFrom [KeyLength * 2]byte
	tmp := make([]byte, KeyLength*2)
	rand.Read(tmp)
	copy(reduceFrom[:], tmp)
	edwards25519.ScReduce(result, &reduceFrom)
	return
}

// generate a new private-public key pair
func NewKeyPair() (privKey *EdDSAKey, pubKey *EdDSAKey) {
	privKey = RandomScalar()
	pubKey = privKey.PublicKey()
	return
}
*/
