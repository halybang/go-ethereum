// Package eth implements the Ethereum protocol.
package eth

import (
	"os"
	"errors"
	"fmt"
	//	"math/big"
	//	"runtime"
	"path/filepath"
	"sync"
	//"sync/atomic"
	//	"time"

	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/common"
	//	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/consensus"
	//	"github.com/ethereum/go-ethereum/consensus/clique"
	"github.com/ethereum/go-ethereum/consensus/devote"
	//	"github.com/ethereum/go-ethereum/consensus/ethash"
	//	"github.com/ethereum/go-ethereum/contracts/masternode/contract"
	"github.com/ethereum/go-ethereum/core"
	//	"github.com/ethereum/go-ethereum/core/bloombits"
	//	"github.com/ethereum/go-ethereum/core/rawdb"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/core/types/devotedb"
	"github.com/ethereum/go-ethereum/core/vm"
	//	"github.com/ethereum/go-ethereum/eth/downloader"
	//	"github.com/ethereum/go-ethereum/eth/filters"
	//	"github.com/ethereum/go-ethereum/eth/gasprice"
	"github.com/ethereum/go-ethereum/ethdb"
	//	"github.com/ethereum/go-ethereum/event"
	//	"github.com/ethereum/go-ethereum/internal/ethapi"
	"github.com/ethereum/go-ethereum/log"
	"github.com/ethereum/go-ethereum/miner"
	//	"github.com/ethereum/go-ethereum/node"
	//	"github.com/ethereum/go-ethereum/p2p"
	"github.com/ethereum/go-ethereum/params"
	//"github.com/ethereum/go-ethereum/rlp"
	//"github.com/ethereum/go-ethereum/rpc"
)

// SubChain is a part of Kim system
type SubChain struct {
	eth               *Ethereum // Shared
	shard             *Shard    // Parent
	chainType         uint64
	chainConfig       *params.ChainConfig
	txPool            *core.TxPool
	blockchain        *core.BlockChain
	engine            consensus.Engine
	miner             *miner.Miner
	masternodeManager *MasternodeManager
	//devoteDB          *devotedb.DevoteDB
	lock sync.RWMutex // Protects the variadic fields (e.g. gas price and etherbase)
}

// Begin Implement ChainReader interface

// Config retrieves the blockchain's chain configuration.
func (subChain *SubChain) Config() *params.ChainConfig {
	return subChain.chainConfig
}

// CurrentHeader retrieves the current header from the local chain.
func (subChain *SubChain) CurrentHeader() *types.Header {
	return subChain.blockchain.CurrentHeader()
}

// GetHeader retrieves a block header from the database by hash and number.
func (subChain *SubChain) GetHeader(hash common.Hash, number uint64) *types.Header {
	return subChain.blockchain.GetHeader(hash, number)
}

// GetHeaderByNumber retrieves a block header from the database by number.
func (subChain *SubChain) GetHeaderByNumber(number uint64) *types.Header {
	return subChain.blockchain.GetHeaderByNumber(number)
}

// GetHeaderByHash retrieves a block header from the database by its hash.
func (subChain *SubChain) GetHeaderByHash(hash common.Hash) *types.Header {
	return subChain.blockchain.GetHeaderByHash(hash)
}

// GetBlock retrieves a block from the database by hash and number.
func (subChain *SubChain) GetBlock(hash common.Hash, number uint64) *types.Block {
	return subChain.blockchain.GetBlock(hash, number)
}

// End Implement ChainReader interface

// Shard retrun shard object of sub-chain
func (subChain *SubChain) Shard() *Shard {
	subChain.lock.Lock()
	defer subChain.lock.Unlock()

	return subChain.shard
}

// BlockChain return blockchain of sub-chain
func (subChain *SubChain) BlockChain() *core.BlockChain {
	subChain.lock.Lock()
	defer subChain.lock.Unlock()

	return subChain.blockchain
}

// TxPool return txPool of the sub-chain
func (subChain *SubChain) TxPool() *core.TxPool {
	subChain.lock.Lock()
	defer subChain.lock.Unlock()

	return subChain.txPool
}

// Engine return consensus of the sub-chain
func (subChain *SubChain) Engine() consensus.Engine {
	return subChain.engine
}

// Miner return miner of the chain
func (subChain *SubChain) Miner() *miner.Miner {
	subChain.lock.Lock()
	defer subChain.lock.Unlock()
	return subChain.miner
}

// DevoteDB return devoteDB
func (subChain *SubChain) DevoteDB() *devotedb.DevoteDB {
	return subChain.shard.DevoteDB()
}

// Start start sub-chain
func (subChain *SubChain) Start() {
	log.Info("Start chain")
}

// AccountManager return AccountManager
func (subChain *SubChain) AccountManager() *accounts.Manager {
	return subChain.eth.AccountManager()
}

// ChainDb return ChainDb
func (subChain *SubChain) ChainDb() ethdb.Database {
	return subChain.eth.ChainDb()
}

// Stop stops subchain
func (subChain *SubChain) Stop() {
	subChain.blockchain.Stop()
	subChain.engine.Close()
	subChain.txPool.Stop()
	subChain.miner.Stop()
}

// NewSubChain creates a subchain, depend on type and id
func NewSubChain(eth *Ethereum, shard *Shard, chainType uint64, ethConfig *Config, chainConfig *params.ChainConfig, topBlock *types.Block) (*SubChain, error) {
	var (
		vmConfig    = vm.Config{EnablePreimageRecording: ethConfig.EnablePreimageRecording}
		cacheConfig = &core.CacheConfig{Disabled: ethConfig.NoPruning, TrieNodeLimit: ethConfig.TrieCache, TrieTimeLimit: ethConfig.TrieTimeout}
	)
	if shard.ShardID() <= 0xFF {

	} else {
		if uint64(types.BLOCK_FOREIGN) != chainType {
			return nil, errors.New("Custome chain must be foreign chain")
		}
	}
	cConfig, _, block, err := core.SetupGenesisShardBlock(eth.chainDb, chainType, shard.shard, topBlock, ethConfig.Genesis, chainConfig)
	log.Info("ShardChainConfig", "cConfig", cConfig, "chainConfig", chainConfig)
	if cConfig != chainConfig {
		log.Info("ShardChainConfig diff", "cConfig", cConfig, "chainConfig", chainConfig)
	}
	if block == nil {

	}
	//engine: CreateConsensusEngine(ctx, chainConfig, &config.Ethash, config.MinerNotify, config.MinerNoverify, chainDb),
	engine := CreateConsensusEngine(eth.ctx, &eth.config.Ethash, chainConfig, ethConfig.MinerNotify, ethConfig.MinerNoverify, eth.chainDb)
	blockchain, err := core.NewBlockChain(eth.chainDb, chainType, shard.shard, cacheConfig, chainConfig, engine, vmConfig)
	if err != nil {
		return nil, err
	}
	// Rewind the chain in case of an incompatible config upgrade.
	//	if compat, ok := genesisErr.(*params.ConfigCompatError); ok {
	//		log.Warn("Rewinding chain to upgrade configuration", "err", compat)
	//		blockhain.SetHead(compat.RewindTo)
	//		//rawdb.WriteChainConfig(chainDb, genesisHash, chainConfig)
	//	}
	//eth.bloomIndexer.Start(eth.blockchain)
	newTxPoolConfig := ethConfig.TxPool	
	tmp := fmt.Sprintf("pools/%016x-%02x-%s",  shard.shard, chainType, newTxPoolConfig.Journal)	
	if newTxPoolConfig.Journal != "" {
		newTxPoolConfig.Journal = eth.ctx.ResolvePath(tmp)
		os.MkdirAll(filepath.Dir(newTxPoolConfig.Journal), 0777)
	}
	
	log.Info("Create pool with config ", "path",  newTxPoolConfig.Journal)
	txPool := core.NewTxPool(newTxPoolConfig, chainConfig, blockchain)

	//	contractBackend := NewContractBackend(eth)
	//	contract, err := contract.NewContract(params.MasterndeContractAddress, contractBackend)

	if devote, ok := engine.(*devote.Devote); ok {
		devote.Masternodes(eth.masternodeManager.MasternodeList)
		//devote.GetGovernanceContractAddress(eth.masternodeManager.GetGovernanceContractAddress)
	}
	subChain := &SubChain{
		chainType:         chainType,
		shard:             shard,
		chainConfig:       chainConfig,
		eth:               eth,
		blockchain:        blockchain,
		engine:            engine,
		txPool:            txPool,
		masternodeManager: eth.masternodeManager,
	}

	//miner := miner.NewMiner(subChain, blockchain, chainConfig, eth.EventMux(), engine, ethConfig.MinerRecommit, ethConfig.MinerGasFloor, ethConfig.MinerGasCeil)
	//	miner.SetExtra(makeExtraData(ethConfig.MinerExtraData))
	//subChain.miner = miner

	if uint64(types.BLOCK_FOREIGN) == chainType {
		shard.foreignChain = subChain
	} else {
		shard.homeChain = subChain
	}
	return subChain, nil
}
