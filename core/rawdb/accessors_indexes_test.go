// Copyright 2018 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package rawdb

import (
	"crypto/rand"
	"encoding/json"
	"math/big"
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto/ed25519"
	"github.com/ethereum/go-ethereum/ethdb"
)

// Tests that positional lookup metadata can be stored and retrieved.
func TestLookupStorage(t *testing.T) {
	db := ethdb.NewMemDatabase()
	chainType := uint(types.BLOCK_FOREIGN)
	shard := uint64(0x1)

	tx1 := types.NewTransaction(common.BytesToAddress([]byte{0x11}), common.Hash{0x0}, types.NORMAL_RX, common.Hash{0x0}, big.NewInt(111), 1111, big.NewInt(11111), nil, []byte{0x11, 0x11, 0x11})
	tx2 := types.NewTransaction(common.BytesToAddress([]byte{0x22}), common.Hash{0x0}, types.NORMAL_RX, common.Hash{0x0}, big.NewInt(222), 2222, big.NewInt(22222), nil, []byte{0x22, 0x22, 0x22})
	tx3 := types.NewTransaction(common.BytesToAddress([]byte{0x33}), common.Hash{0x0}, types.NORMAL_RX, common.Hash{0x0}, big.NewInt(333), 3333, big.NewInt(33333), nil, []byte{0x33, 0x33, 0x33})
	//tx4 := types.NewTransaction(common.BytesToAddress([]byte{0x33}), common.Hash{0x0}, types.NORMAL_RX, common.Hash{0x0}, big.NewInt(333), 3333, big.NewInt(33333), nil, []byte{0x33, 0x33, 0x33})

	_, edPrivateKey1, _ := ed25519.GenerateKey(rand.Reader)
	newSubkey := edPrivateKey1.SubKey(tx3.Nonce() + 1)
	newSubPub := newSubkey.PublicKey()
	tx4 := types.NextTransaction(tx3, common.BytesToHash(newSubPub.Address()), types.NORMAL_TX, common.Hash{0x0}, big.NewInt(0x1024), big.NewInt(0x1024), 1, common.Big1, []byte("sendTx"), []byte("privateValueX"), []byte("abcdef"))
	txs := []*types.Transaction{tx1, tx2, tx3}

	block := types.NewBlock(&types.Header{Type: chainType, ShardId: shard, Number: big.NewInt(314)}, txs, nil, nil)

	// Check that no transactions entries are in a pristine database
	for i, tx := range txs {
		if txn, _, _, _ := ReadTransaction(db, uint64(chainType), uint64(shard), tx.Hash()); txn != nil {
			t.Fatalf("tx #%d [%x]: non existent transaction returned: %v", i, tx.Hash(), txn)
		}
	}
	///
	//rplData, _ := rlp.EncodeToBytes(header)
	data, err := json.Marshal(block.Body())
	if err != nil {
	}
	t.Log(string(data))

	//rplData, _ := rlp.EncodeToBytes(block)

	// Insert all the transactions into the database, and verify contents
	WriteBlock(db, block)
	WriteTxLookupEntries(db, block)

	for i, tx := range txs {
		if txn, hash, number, index := ReadTransaction(db, uint64(chainType), uint64(shard), tx.Hash()); txn == nil {
			t.Fatalf("tx #%d [%x]: transaction not found", i, tx.Hash())
		} else {
			if hash != block.Hash() || number != block.NumberU64() || index != uint64(i) {
				t.Fatalf("tx #%d [%x]: positional metadata mismatch: have %x/%d/%d, want %x/%v/%v", i, tx.Hash(), hash, number, index, block.Hash(), block.NumberU64(), i)
			}
			if tx.Hash() != txn.Hash() {
				t.Fatalf("tx #%d [%x]: transaction mismatch: have %v, want %v", i, tx.Hash(), txn, tx)
			}
		}
	}
	// Delete the transactions and check purge
	for i, tx := range txs {
		DeleteTxLookupEntry(db, uint64(chainType), uint64(shard), tx.Hash())
		if txn, _, _, _ := ReadTransaction(db, uint64(chainType), uint64(shard), tx.Hash()); txn != nil {
			t.Fatalf("tx #%d [%x]: deleted transaction returned: %v", i, tx.Hash(), txn)
		}
	}

	//// DAG Transaction Test
	for i, tx := range txs {
		if txn := ReadRawTransaction(db, tx.Hash()); txn != nil {
			t.Fatalf("tx #%d [%x]: non existent transaction returned: %v", i, tx.Hash(), txn)
		}
	}
	for _, tx := range txs {
		WriteTransaction(db, tx)
		WriteHeadAccount(db, tx.Account(), tx)
	}
	WriteHeadAccount(db, tx4.Account(), tx4)

	for i, tx := range txs {
		txn := ReadRawTransaction(db, tx.Hash())
		if txn == nil {
			t.Fatalf("tx #%d [%x]: non existent transaction returned: %v", i, tx.Hash(), txn)
		} else {
			txData, _ := json.Marshal(txn)
			t.Log(string(txData))
		}
	}
	for i, tx := range txs {
		txh := ReadAccountNonce(db, tx.Account(), tx.Nonce())
		if txh == (common.Hash{}) {
			t.Fatalf("tx #%d [%x]: non existent transaction returned: %v", i, tx.Account(), txh)
		} else {
			t.Log("Account", tx.Account().Hex(), "at", tx.Nonce(), "tx hash", hexutil.Encode(txh.Bytes()))
			txn := ReadRawTransaction(db, txh)
			if txn == nil {
				t.Fatalf("tx #%d [%x]: non existent transaction returned: %v", i, txh, txn)
			} else {
				txData, _ := json.Marshal(txn)
				t.Log("Account", tx.Account().Hex(), "at", tx.Nonce(), "tx hash", hexutil.Encode(txh.Bytes()), "data:", string(txData))
			}
		}
	}

	for i, tx := range txs {
		txn := ReadHeadAccount(db, tx.Account())
		if txn == nil {
			t.Fatalf("tx #%d [%x]: non existent transaction returned: %v", i, tx.Account(), txn)
		} else {
			txData, _ := json.Marshal(txn)
			t.Log("Account", tx.Account().Hex(), " last tx: ", string(txData))
		}
	}
}
