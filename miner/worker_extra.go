package miner

/*
import (
	//"bytes"
	//"errors"
	//"fmt"
	//"math/big"
	//"sync"
	"sync/atomic"
	"time"

	//mapset "github.com/deckarep/golang-set"
	"github.com/ethereum/go-ethereum/common"
	//"github.com/ethereum/go-ethereum/consensus"
	"github.com/ethereum/go-ethereum/consensus/devote"
	//"github.com/ethereum/go-ethereum/consensus/misc"
	"github.com/ethereum/go-ethereum/core"
	//"github.com/ethereum/go-ethereum/core/state"
	"github.com/ethereum/go-ethereum/core/types"
	//"github.com/ethereum/go-ethereum/core/types/devotedb"
	//"github.com/ethereum/go-ethereum/core/vm"
	//"github.com/ethereum/go-ethereum/ethdb"
	//"github.com/ethereum/go-ethereum/event"
	"github.com/ethereum/go-ethereum/log"
	//"github.com/ethereum/go-ethereum/params"
)
*/
/*
func (self *worker) mine(now int64) {
	engine, ok := self.engine.(*devote.Devote)
	if !ok {
		log.Error("Only the devote engine was allowed")
		return
	}

	err := engine.CheckWitness(self.chain.CurrentBlock(), now)
	if err != nil {
		switch err {
		case devote.ErrWaitForPrevBlock,
			devote.ErrMinerFutureBlock,
			devote.ErrInvalidBlockWitness,
			devote.ErrInvalidMinerBlockTime:
			log.Debug("Failed to miner the block, while ", "err", err)
			//fmt.Printf("Failed to miner the block, while error:%s\n", err)
		default:
			log.Error("Failed to miner the block", "err", err)
			//fmt.Printf("Failed to miner the block, while error:%s\n", err)
		}
		return
	}
	work, err := self.commitNewWork()
	if err != nil {
		log.Error("Failed to create the new work", "err", err)
		return
	}

	result, err := self.engine.Seal(self.chain, work.Block, self.quitCh)
	if err != nil {
		log.Error("Failed to seal the block", "err", err)
		return
	}
	self.recv <- &Result{work, result}
}

func (self *worker) mineLoop() {
	ticker := time.NewTicker(time.Second).C
	for {
		select {
		case now := <-ticker:
			self.mine(now.Unix())
		case <-self.stopper:
			close(self.quitCh)
			self.quitCh = make(chan struct{}, 1)
			self.stopper = make(chan struct{}, 1)
			return
		}
	}
}
*/

//func (self *worker) register(agent Agent) {
//	self.mu.Lock()
//	defer self.mu.Unlock()
//	self.agents[agent] = struct{}{}
//	agent.SetReturnCh(self.recv)
//}

//func (self *worker) unregister(agent Agent) {
//	self.mu.Lock()
//	defer self.mu.Unlock()
//	delete(self.agents, agent)
//	agent.Stop()
//}
/*
func (self *worker) update() {
	defer self.txsSub.Unsubscribe()
	defer self.chainHeadSub.Unsubscribe()
	defer self.chainSideSub.Unsubscribe()

	for {
		// A real event arrived, process interesting content
		select {
		// Handle ChainHeadEvent
		case <-self.chainHeadCh:
			self.commitNewWork()

		// Handle ChainSideEvent
		case ev := <-self.chainSideCh:
			self.uncleMu.Lock()
			self.possibleUncles[ev.Block.Hash()] = ev.Block
			self.uncleMu.Unlock()

		// Handle NewTxsEvent
		case ev := <-self.txsCh:
			// Apply transactions to the pending state if we're not mining.
			//
			// Note all transactions received may not be continuous with transactions
			// already included in the current mining block. These transactions will
			// be automatically eliminated.
			if atomic.LoadInt32(&self.mining) == 0 {
				self.currentMu.Lock()
				txs := make(map[common.Address]types.Transactions)
				for _, tx := range ev.Txs {
					acc, _ := types.Sender(self.current.signer, tx)
					txs[acc] = append(txs[acc], tx)
				}
				txset := types.NewTransactionsByPriceAndNonce(self.current.signer, txs)
				self.current.commitTransactions(self.mux, txset, self.chain, self.coinbase)
				self.updateSnapshot()
				self.currentMu.Unlock()
			} else {
				/// TODO: WAN Removed
				// If we're mining, but nothing is being processed, wake on new transactions
				//				if self.config.Clique != nil && self.config.Clique.Period == 0 {
				//					self.commitNewWork()
				//				}
			}

		// System stopped
		case <-self.txsSub.Err():
			return
		case <-self.chainHeadSub.Err():
			return
		case <-self.chainSideSub.Err():
			return
		}
	}
}

func (self *worker) wait() {
	for {
		for result := range self.recv {
			atomic.AddInt32(&self.atWork, -1)

			if result == nil {
				continue
			}
			block := result.Block
			work := result.Work

			// TODO: WAN Changed
			// waiting minimum sealing time
			//beginTime := block.Header().Time.Int64()
			//for time.Now().Unix()-beginTime < self.miniSealTime {
			//	log.Trace("need wait minimum sealing time", "now", time.Now().Unix(), "block begin", beginTime)
			//	time.Sleep(time.Millisecond * 100)
			//
			//	// if have synchronized new block from remote, stop waiting at once.
			//	if self.chain.CurrentHeader().Number.Cmp(block.Header().Number) >= 0 {
			//		log.Info("have synchronized new block from remote, should stop wait sealing minimum time at once",
			//			"chain last block", self.chain.CurrentHeader().Number.Int64(),
			//			"the waiting block", block.Header().Number.Int64())
			//		break
			//	}
			//}

			// Update the block hash in all logs since it is now available and not when the
			// receipt/log of individual transactions were created.
			for _, r := range work.receipts {
				for _, l := range r.Logs {
					l.BlockHash = block.Hash()
				}
			}
			for _, log := range work.state.Logs() {
				log.BlockHash = block.Hash()
			}
			self.currentMu.Lock()
			stat, err := self.chain.WriteBlockWithState(block, work.receipts, work.state)
			self.currentMu.Unlock()
			if err != nil {
				log.Error("Failed writing block to chain", "err", err)
				continue
			}
			// Broadcast the block and announce chain insertion event
			self.mux.Post(core.NewMinedBlockEvent{Block: block})
			var (
				events []interface{}
				logs   = work.state.Logs()
			)
			events = append(events, core.ChainEvent{Block: block, Hash: block.Hash(), Logs: logs})
			if stat == core.CanonStatTy {
				events = append(events, core.ChainHeadEvent{Block: block})
			}
			self.chain.PostChainEvents(events, logs)

			// Insert the block into the set of pending ones to wait for confirmations
			self.unconfirmed.Insert(block.NumberU64(), block.Hash())
		}
	}
}
*/

// push sends a new work task to currently live miner agents.
//func (self *worker) push(work *Work) {
//	if atomic.LoadInt32(&self.mining) != 1 {
//		return
//	}
//	for agent := range self.agents {
//		atomic.AddInt32(&self.atWork, 1)
//		if ch := agent.Work(); ch != nil {
//			ch <- work
//		}
//	}
//}

/*
// TODO: 1.8.12 function
//func (self *worker) start() {
//	self.mu.Lock()
//	defer self.mu.Unlock()

//	atomic.StoreInt32(&self.mining, 1)

//	// spin up agents
//	for agent := range self.agents {
//		agent.Start()
//	}
//}

//func (self *worker) stop() {
//	self.wg.Wait()

//	self.mu.Lock()
//	defer self.mu.Unlock()
//	if atomic.LoadInt32(&self.mining) == 1 {
//		for agent := range self.agents {
//			agent.Stop()
//		}
//	}
//	atomic.StoreInt32(&self.mining, 0)
//	atomic.StoreInt32(&self.atWork, 0)
//}

*/
